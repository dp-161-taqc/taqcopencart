﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Models;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages
{
    public class ReturnsAccountPage: CommonPageWithMenu
    {
        public ReturnsAccountPage(IWebDriver driver) : base(driver)
        {

        }
      
        private By returnProduct= By.XPath( "//*[@class='breadcrumb']//*[text()='Product Returns']");
        private By lastReturnsOderId=By.XPath("//*[@id='content']/div[1]/table/tbody/tr[1]/td[4]");

        private By lastReturnsReturnsId = By.XPath("//*[@id='content']/div[1]/table/tbody/tr[1]/td[1]");

        private By viewInformationButton=By.XPath("//*[@id='content']/div[1]/table/tbody/tr[1]/td[6]/a/i");

        protected string returnId;

        public void CheckReturnsAccountPage()
        {
            IWebElement expectedElement = driver.FindElement(returnProduct);
            Assert.NotNull(expectedElement);
        }

        public void CheckLastReturnsProductId()
        {
            FieldsInformationForReturnsForm correctData = FieldsInformationForReturnsGenerator.EnterCorrectDataInRequiredFields();
            string expectedOderId = correctData.OderId;
            string actualOderId = driver.FindElement(lastReturnsOderId).Text;
            Assert.AreEqual(expectedOderId,actualOderId);
        }

        public ReturnsProductInformationPage ClickViewButton()
        {
            string actualReturnId = GetReturnId();
            driver.FindElement(viewInformationButton).Click();
            return new ReturnsProductInformationPage(driver, actualReturnId);
        }

        public string GetReturnId() 
        {
            return driver.FindElement(lastReturnsReturnsId, elementWaitSeconds).GetAttribute("innerText");
        }

    }
}
