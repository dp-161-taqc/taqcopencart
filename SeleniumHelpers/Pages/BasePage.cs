﻿using OpenQA.Selenium;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages
{
    public abstract class BasePage
    {
        protected XmlParser Parser { get; set; } = new XmlParser();
        protected IWebDriver driver;
        protected double elementWaitSeconds = 10;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }
    }
}