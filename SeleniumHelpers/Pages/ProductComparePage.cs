﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class ProductComparePage : CommonPage
    {
        public ProductComparePage(IWebDriver driver) : base(driver)
        {
        }
        
        //-----------------List Selectors------------------//
        private readonly By productsToCompareImagesList = By.ClassName("img-thumbnail");
        private readonly By removeCompareProductButtonsList = By.ClassName("btn-danger");
        //-----------------Single Selectors------------------//
        public readonly By firstHeader = By.XPath("//h1[contains(text(),'Product Comparison')]");
        public readonly By buttonContinue = By.XPath("//a[@class='btn btn-default']");
        public readonly By captionOfEmptyComparePage = By.XPath("//p[contains(text(),'You have not chosen any products to compare.')]");

        public IList<IWebElement> RemoveCompareProductButtonsList()
        {
            return driver.FindElements(removeCompareProductButtonsList);
        }

        public IList<IWebElement> ProductsToCompareImagesList()
        {
            return driver.FindElements(productsToCompareImagesList);
        }

        public ProductComparePage RemoveСomparableProductByIndex(int indexOfProduct)
        {
            RemoveCompareProductButtonsList()[indexOfProduct].Click();
            return this;
        }

        public ProductComparePage CheckIfButtonContinueClickableThenPass()
        {
            try
            {
                driver.FindElement(buttonContinue).Click();
                Assert.Pass("Button was clicked");
            }
            catch (NoSuchElementException)
            {
                Assert.Fail("No such element on current page");
            }
            return this;
        }

        public ProductComparePage CheckIfButtonContinueClickableThenFail()
        {
            try
            {
                driver.FindElement(buttonContinue).Click();
                Assert.Fail("Button was clicked");
            }
            catch (NoSuchElementException)
            {
                Assert.Pass("No such element on current page");
            }
            return this;
        }

        public ProductComparePage CheckIsProductComparePageLoaded()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            bool element = wait.Until(condition =>
            {
                try
                {
                    IWebElement elementToBeDisplayed = driver.FindElement(firstHeader);
                    return elementToBeDisplayed.Displayed;
                }
                catch (StaleElementReferenceException)
                {
                    Assert.Fail("The page is not loaded, or loaded with an error!");
                    return false;
                }
                catch (NoSuchElementException)
                {
                    Assert.Fail("The page is not loaded, or loaded not fully!");
                    return false;
                }
            });
            return this;
        }

    }
}
