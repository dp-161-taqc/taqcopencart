﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class ProductPage : CommonPage
    {        
        private const string reviewRightReference = "Write a review";
        
        private By productNameLabel = By.CssSelector(".col-sm-4 h1");
        private By productPriceLabel = By.CssSelector("ul h2");
        private By productOldPriceLabel = By.CssSelector(".list-unstyled span");
                
        private By TagLink(string tagText) { return By.LinkText(tagText); }           

        private By productFullDescriptionTab = By.XPath("//a[@href='#tab-description']");

        private By reviewTab = By.XPath("//a[@href='#tab-review']");
        
        private By reviewWriteText = By.CssSelector("#review + h2");
        private By reviewContinueButton = By.CssSelector(".pull-right button");
        
        private By reviewNameTextBox = By.CssSelector(".control-label + input");
        private By reviewTextBox = By.CssSelector(".control-label + textarea");
        private By radioButtonOne = By.XPath("//input[@value='1']");
        private By radioButtonTwo = By.XPath("//input[@value='2']");
        private By radioButtonThree = By.XPath("//input[@value='3']");
        private By radioButtonFour = By.XPath("//input[@value='4']");
        private By radioButtonFive = By.XPath("//input[@value='5']");
        
        private By reviewBadWarning = By.CssSelector(".alert-danger");
        private By reviewGoodWarning = By.CssSelector(".alert-success");              

        private By productFullDescriptionText = By.CssSelector("#tab-description");
        private By relatedProductNamesList = By.CssSelector(".caption h4 a");
        private By smallProductImage = By.CssSelector(".thumbnail img");
        private By largeProductImage = By.CssSelector(".mfp-img");
        private By imageCloseButton = By.CssSelector(".mfp-close");
        private By addToShoppingCartButton = By.XPath("//button[@id='button-cart']");
        private By addToWishList = By.CssSelector(".col-sm-4 .btn-group button");
        private By alertAboutWishList = By.XPath("//div[@class='alert alert-success']");
        private By quantityField = By.Id("input-quantity");

        private By newReviewUserName = By.CssSelector("td strong");
        private By nameAddedReview = By.CssSelector("td strong");

        public ProductPage(IWebDriver driver) : base(driver)
        {
        }        

        public IList<IWebElement> RelatedProductNamesList()
        {
            return driver.FindElements(relatedProductNamesList);
        }

        public ProductPage OpenProductPageById(int Id)
        {
            driver
               .Navigate()
               .GoToUrl(ConfigurationHelper.BaseUrl 
                    + string.Format(URL.PathToProductPage, Id));
            return new ProductPage(driver);
        }             

        public CommonPage GotoBaseUrl()
        {
            driver
               .Navigate()
               .GoToUrl(ConfigurationHelper.BaseUrl);
            return new CommonPage(driver);
        }             

        public ProductPage ClickReviewRightReference()
        {            
            driver.FindElement(TagLink(reviewRightReference)).Click();            
            return new ProductPage(driver);
        }

        public ProductPage ClickRelatedProductInList(string relatedProductName)
        {
            RelatedProductNamesList().FirstOrDefault(el => el.Text == relatedProductName).Click();
            return new ProductPage(driver);
        }

        public SearchPage ClickOnTag(string tag)
        {
            driver.FindElement(TagLink(tag)).Click();
            return new SearchPage(driver);
        }

        public ProductPage OpenReviewTab()
        {
            driver.FindElement(reviewTab, elementWaitSeconds).Click();            
            return new ProductPage(driver);
        }

        public ProductPage FillName(string name)
        {
            driver.FindElement(reviewNameTextBox, elementWaitSeconds).SendKeys(name);            
            return new ProductPage(driver); ;
        }

        public ProductPage FillReview(string review)
        {           
            driver.FindElement(reviewTextBox, elementWaitSeconds).SendKeys(review);          
            return new ProductPage(driver); 
        }

        public ProductPage SetRatingRadioButton(int rating)
        {
            switch (rating)
            {
                case 1:
                    driver.FindElement(radioButtonOne).Click();
                    break;
                case 2:
                    driver.FindElement(radioButtonTwo).Click(); ;
                    break;
                case 3:
                    driver.FindElement(radioButtonThree).Click();
                    break;
                case 4:
                    driver.FindElement(radioButtonFour).Click();
                    break;
                case 5:
                    driver.FindElement(radioButtonFive).Click();
                    break;
            }            
            return new ProductPage(driver); 
        }

        public ProductPage ClickContinueButton()
        {
            driver.FindElement(reviewContinueButton, elementWaitSeconds).Click();
            return new ProductPage(driver);
        }

        public string GetReviewBadWarning()
        {            
            return driver.FindElement(reviewBadWarning).Text;
        }

        public string GetReviewGoodWarning()
        {
            return driver.FindElement(reviewGoodWarning, elementWaitSeconds).Text;
        }

        public ProductPage CheckGoodWarning()
        {
            StringAssert.Contains("Thank you", GetReviewGoodWarning());
            return this;
        }

        public ProductPage CheckWarningAboutRating()
        {
            StringAssert.Contains("Please select a review rating!", GetReviewBadWarning());            
            return this;
        }

        public ProductPage CheckWarningAboutName()
        {
            StringAssert.Contains("Review Name must", GetReviewBadWarning());
            return this;
        }

        public ProductPage CheckWarningAboutReview()
        {
            StringAssert.Contains("Review Text must", GetReviewBadWarning());
            return this;
        }

        public ProductPage CheckNameForProductReviewFormForLoggedUser(string name)
        {
            StringAssert.Contains(name, GetDataFromTextBox(reviewNameTextBox));
            return this;
        }            
       
        public ProductPage ClickOnImage()
        {
            driver.FindElement(smallProductImage, elementWaitSeconds).Click();
            return new ProductPage(driver);
        }

        public ProductPage ClickOnCloseButton()
        {
            driver.FindElement(imageCloseButton).Click();
            return new ProductPage(driver);
        }

        public ProductPage ClickOnAddToShoppingCartButton()
        {
            Thread.Sleep(3000);
            driver.FindElement(addToShoppingCartButton).Click();
            return new ProductPage(driver);
        }

        public ProductPage EnterValueShoppingCart(string quantity)
        {
            Thread.Sleep(3000);
            driver.FindElement(quantityField).SendKeys(Keys.Backspace + quantity );
            return new ProductPage(driver);
        }
        
        public ProductPage ClickOnAddToWishListButton()
        {
            driver.FindElement(addToWishList).Click();
            return new ProductPage(driver);
        }

        public string GetDataFromTextBox(By el)
        {
            return driver.FindElement(el).GetAttribute("value");
        }

        public string GetAllertAboutWishList()
        {
            return driver.FindElement(alertAboutWishList).Text;
        }
        
        public int GetDiscount()
        {
            string newPr = GetActualPrice().TrimStart('$');
            string oldPr = GetOldPrice().TrimStart('$');
            oldPr = oldPr.Substring(0, oldPr.Length - 3).Replace(",", "");
            newPr = newPr.Substring(0, newPr.Length - 3).Replace(",", "");
            int oldPrice = Convert.ToInt32(oldPr);
            int newPrice = Convert.ToInt32(newPr);
            return oldPrice - newPrice;
        }

        public IWebElement GetSmallImage()
        {
            return driver.FindElement(smallProductImage);
        }

        public IWebElement GetLargeImage()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            wait.Until((dr) => dr.FindElement(largeProductImage).Size != new System.Drawing.Size(0, 0));
            return driver.FindElement(largeProductImage);
        }

        public string GetProductName()
        {
            return driver.FindElement(productNameLabel).Text;
        }

        public string GetActualPrice()
        {
            return driver.FindElement(productPriceLabel, elementWaitSeconds).Text;
        }

        public string GetOldPrice()
        {
            return driver.FindElement(productOldPriceLabel, elementWaitSeconds).Text;
        }

        private string GetFullDescriptionLabel()
        {
            return driver.FindElement(productFullDescriptionTab, elementWaitSeconds).Text;
        }

        public string GetReviewsLabel()
        {
            return driver.FindElement(reviewTab).Text;
        }

        public string GetWriteReviewText()
        {
            return driver.FindElement(reviewWriteText).Text;
        }
               
        public string GetFullDescriptionText()
        {
            return driver.FindElement(productFullDescriptionText, elementWaitSeconds).Text;
        }

        public ProductPage CheckFullDescriptionTabAndText(string expectedFullDescriptionTab, string expectedFullDescriptionText)
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedFullDescriptionTab, GetFullDescriptionLabel());
                Assert.AreEqual(expectedFullDescriptionText, GetFullDescriptionText());
            });
            return this;
        }

        public ProductPage CheckReviewsLabelAndText()
        {
            Assert.Multiple(() =>
            {
                StringAssert.Contains("Reviews", GetReviewsLabel());
                Assert.AreEqual("Write a review", GetWriteReviewText());
            });            
            return this;
        }

        public ProductPage CheckActualPriceAndOldPrice(string expectedPrice, string expectedOldPrice)
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedPrice, GetActualPrice());
                Assert.AreEqual(expectedOldPrice, GetOldPrice());
            });
            return this;
        }

        public ProductPage CheckSmallProductImage(string expectedSmallImage)
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedSmallImage, GetSmallImage().GetAttribute("src"));
                Assert.AreEqual(new Size(228, 228), GetSmallImage().Size);
            });
            return this;
        }

        public ProductPage CheckLargeProductImage(string expectedLargeImage)
        {
            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedLargeImage, GetLargeImage().GetAttribute("src"));
                Assert.AreEqual(new Size(500, 580), GetLargeImage().Size);
            });
            return this;           
        }

        public ProductPage CheckProductName(string expectedProductName)
        {
            Assert.AreEqual(expectedProductName, GetProductName());
            return this;
        }
        public ProductPage CheckAllertAddToWishList(string expected)
        {
            StringAssert.Contains(expected, GetAllertAboutWishList());
            return this;
        }

        public ProductPage CheckGetDiscount(int expectedDiscount)
        {
            Assert.AreEqual(expectedDiscount, GetDiscount());
            return this;
        }

        public ProductPage CheckIsProductPageLoaded()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            bool element = wait.Until(condition =>
            {
                try
                {
                    IWebElement elementToBeDisplayed = driver.FindElement(productNameLabel);
                    return elementToBeDisplayed.Displayed;
                }
                catch (StaleElementReferenceException)
                {
                    Assert.Fail("The page is not loaded, or loaded with an error!");
                    return false;
                }
                catch (NoSuchElementException)
                {
                    Assert.Fail("The page is not loaded, or loaded not fully!");
                    return false;
                }
            });
            return this;
        }
    }
}
