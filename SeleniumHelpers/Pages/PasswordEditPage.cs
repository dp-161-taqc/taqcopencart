﻿using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class PasswordEditPage : CommonPageWithAccountMenu
    {
        private readonly By inputPassword = By.Id("input-password");
        private readonly By inputPasswordConfirm = By.Id("input-confirm");
        private readonly By continueButton = By.XPath("//input[@value='Continue']");
        private readonly By backButton = By.XPath("//*[@class='pull-left']//*[text()='Back']");
  
        public PasswordEditPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToPasswordEditPage));
        }

        public AccountPage ClickBackButton()
        {
            driver.FindElement(backButton).Click();
            return new AccountPage(driver);
        }

        public AccountPage ClickContinueButton()
        {
            driver.FindElement(continueButton).Click();
            return new AccountPage(driver);
        }

        public PasswordEditPage InputPassword(string input)
        {
            driver.FindElement(inputPassword).SendKeys(input);
            return this;
        }

        public PasswordEditPage InputPasswordConfirm(string input)
        {
            driver.FindElement(inputPasswordConfirm).SendKeys(input);
            return this;
        }
    }
}
