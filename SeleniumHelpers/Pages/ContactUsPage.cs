﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages
{
    public class ContactUsPage : CommonPage
    {  
        private By contactUsBreabcrumb = By.CssSelector(".breadcrumb");

        private By enquiryTextBox = By.CssSelector("#input-enquiry");
        private By mailTextbox = By.CssSelector("#input-email");
        private By nameTextBox = By.CssSelector("#input-name");

        private By submitButton = By.CssSelector(".btn-primary");
               
        private By contactUsBadMessage = By.CssSelector(".text-danger");

        public ContactUsPage(IWebDriver driver) : base(driver)
        {
        }

        public ContactUsPage CheckContactUsPageIsOpen()
        {
            Assert.AreEqual("Contact Us", GetBreadcrumb());
            return this;
        }
                
        public ContactUsPage CheckContactUsBadNameMessage()
        {
            Assert.AreEqual("Name must be between 3 and 32 characters!", GetContactUsBadMessage());
            return this;
        }

        public ContactUsPage CheckContactUsBadMailMessage()
        {
            Assert.AreEqual("E-Mail Address does not appear to be valid!", GetContactUsBadMessage());
            return this;
        }

        public ContactUsPage CheckContactUsBadEnquiryMessage()
        {
            Assert.AreEqual("Enquiry must be between 10 and 3000 characters!", GetContactUsBadMessage());
            return this;
        }

        public ContactUsPage CheckNameForContactUsFormForLoggedUser(string firstname)
        {
            Assert.AreEqual(firstname, GetDataFromTextBox(nameTextBox));
            return this;
        }

        public ContactUsPage CheckMailForContactUsFormForLoggedUser(string email)
        {
            Assert.AreEqual(email, GetDataFromTextBox(mailTextbox));
            return this;
        }       

        public ContactUsSuccessPage ClickSubmitButton()
        {
            driver
                .FindElement(submitButton).Click();
            return new ContactUsSuccessPage(driver);
        }

        public ContactUsPage ClickSubmitButtonWithBadData()
        {
            driver
               .FindElement(submitButton).Click();
            return new ContactUsPage(driver);
        }

        private string GetBreadcrumb()
        {
            return driver.FindElement(contactUsBreabcrumb).Text;
        }

        private string GetContactUsBadMessage()
        {
            return driver.FindElement(contactUsBadMessage).Text;
        }

        private string GetDataFromTextBox(By textBox)
        {
            return driver.FindElement(textBox).GetAttribute("value");
        }        

        public HomePage GoToHomePage()
        {
            driver
                .Navigate()
                .GoToUrl(ConfigurationHelper.BaseUrl);
            return new HomePage(driver);
        }

        public ContactUsPage OpenContactUsPageByUrl()
        {
            driver
               .Navigate()
               .GoToUrl(ConfigurationHelper.BaseUrl + URL.PathToContactUsPage);
            return new ContactUsPage(driver);
        }
        
        public ContactUsPage SetEnquiry(string enquiry)
        {
            driver
                .FindElement(enquiryTextBox).SendKeys(enquiry);
            return new ContactUsPage(driver);
        }

        public ContactUsPage SetMail(string mail)
        {
            driver
                .FindElement(mailTextbox).SendKeys(mail);
            return new ContactUsPage(driver);
        }

        public ContactUsPage SetName(string name)
        {
            driver
                .FindElement(nameTextBox, elementWaitSeconds).SendKeys(name);
            return new ContactUsPage(driver);
        }    
    }
}
