﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Models;
using SeleniumHelpers.Data;
using NUnit.Framework;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class OrderHistoryPage : CommonPage
    {
        public OrderHistoryPage(IWebDriver driver) : base(driver)
        {
        }
        private By orderHistory = By.XPath("//*[@id=\"content\"]/h1");
        private By mainPage = By.XPath("//*[@id=\"logo\"]/a/img");
        private By loginPageTitle = By.TagName("title");

        public string IsOrderHistoryPage()
        {
            Thread.Sleep(2000);
            return driver.FindElement(orderHistory).Text;
        }
        public void IsOrderHistory()
        {
            Thread.Sleep(2000);
            StringAssert.Contains("Order History", IsOrderHistoryPage());
        }

        public void CheckLoginPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl + string.Format(URL.PathToLoginPage);
            string expectedTitle = GetPageTitle(loginPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }
        public CommonPage GotoMain()
        {
            driver.FindElement(mainPage).Click();
            return new CommonPage(driver);
        }

    }
}

