﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Pages.CommonPages;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SeleniumHelpers.Pages
{
    public class WishListPage : CommonPageWithAccountMenu
    {
        public WishListPage(IWebDriver driver)
            : base(driver)
        {

        }
        private By pageTitle = By.TagName("title");
        private By allert = By.XPath("//div[@class='alert alert-success']");
        private By tableWithWishProduct = By.XPath("//table[@class='table table-bordered table-hover']//tbody");
        private By buttonContinue = By.XPath("//a[@class='btn btn-primary']");
        private By addToShoppingCartButton = By.TagName("button");
        private By removeProductButton = By.XPath("//a[@class='btn btn-danger']");
        private By isEmptyWishList = By.XPath("//*[@id='content']");

        private List<IWebElement> GetTableRows()
        {
            IWebElement table = driver.FindElement(tableWithWishProduct);
            return table.FindElements(By.TagName("tr")).ToList();
        }

        public string GetAllert()
        {
            return driver.FindElement(allert).Text;
        }

        public string GetPageTitle()
        {
            return driver.FindElement(pageTitle).GetAttribute("innerText");
        }

        public int GetAmountProducts()
        {
            return GetTableRows().Count();
        }

        public int GetAmountIdenticalProducts(string productName)
        {
            int amountIdenticalProducts = 0;
            foreach (IWebElement products in GetTableRows())
            {
                if (products.Text.Contains(productName))
                {
                    amountIdenticalProducts++;
                }
            }
            return amountIdenticalProducts;
        }

        public bool IsProducInWishList(string productName)
        {
            bool flag = false;
            foreach (IWebElement name in GetTableRows())
            {
                if (name.Text.Contains(productName))
                {
                    flag = true;
                }
            }
            return flag;
        }

        public bool IsEmpty()
        {
            bool flag = false;
            if (driver.FindElement(isEmptyWishList).Text.Contains("empty"))
            {
                flag = true;
            }
            return flag;
        }

        public AccountPage ClickContinueButton()
        {
            driver.FindElement(buttonContinue).Click();
            return new AccountPage(driver);
        }

        private void ClickRemoveButton()
        {
            driver.FindElement(removeProductButton).Click();
        }

        public WishListPage AddToShoppingCart(string productName)
        {
            foreach (IWebElement products in GetTableRows())
            {
                if (products.Text.Contains(productName))
                {
                    products.FindElement(addToShoppingCartButton).Click();
                }
            }
            return new WishListPage(driver);
        }

        public WishListPage ClickDeleteProduct(string productName)
        {
            foreach (IWebElement products in GetTableRows())
            {
                if (products.Text.Contains(productName))
                {
                    ClickRemoveButton();
                }
            }
            return new WishListPage(driver);
        }
        public WishListPage ClearAllWishList()
        {
            int steps = GetAmountProducts();
            while (steps > 0)
            {
                Thread.Sleep(1000);
                ClickRemoveButton();
                steps--;
            }
            return new WishListPage(driver);
        }

        public WishListPage CheckAmountIdenticalProducts(int expected, string productName)
        {
            Assert.AreEqual(expected, GetAmountIdenticalProducts(productName));
            return this;
        }

        public WishListPage CheckIsProductInWishList(bool expected, string productName)
        {
            Assert.AreEqual(expected, IsProducInWishList(productName));
            return this;
        }

        public WishListPage CheckAllertAfterAddProductToShopping(string expected)
        {
            StringAssert.Contains(expected, GetAllert());
            return this;
        }

        public WishListPage CheckClearWishList(bool expected)
        {
            Assert.AreEqual(expected, IsEmpty());
            return this;
        }

        public WishListPage CheckOpenWishListPage(string expected)
        {
            Assert.AreEqual(expected, GetPageTitle());
            return this;
        }
    }
}