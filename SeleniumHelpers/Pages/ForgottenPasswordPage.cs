using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class ForgottenPasswordPage : CommonPageWithMenu
    {
        private By continueButton = By.XPath("//input[@value='Continue']");
        private By backButton = By.XPath("//*[@class='pull-left']//*[text()='Back']");
        private By inputEmail = By.Id("input-email");
        private By errorMessage = By.XPath("//div[@class='alert alert-danger']");
        private By forgottenPasswordPageTitle = By.TagName("title");

        public ForgottenPasswordPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToForgetPasswordPage));
        }

        public LoginPage ClickBackButton()
        {
            driver.FindElement(backButton).Click();
            return new LoginPage(driver);
        }

        public LoginPage ClickContinueButton(string input)
        {
            InputEmail(input);
            driver.FindElement(continueButton).Click();
            return new LoginPage(driver);
        }

        public ForgottenPasswordPage ClickContinueButtonIncorrectInput()
        {
            driver.FindElement(continueButton).Click();
            return this;
        }

        public ForgottenPasswordPage InputEmail(string emailInput)
        {
            driver.FindElement(inputEmail).SendKeys(emailInput);
            return this;
        }

        public string GetWarningMessage(By element)
        {
            return driver.FindElement(element).GetAttribute("innerText");
        }

        public void CheckForgottenPasswordPageIncorrectInput()
        {
            string expectedMessage = " Warning: The E-Mail Address was not found in our records, please try again!";
            StringAssert.Contains(expectedMessage, GetWarningMessage(errorMessage));
        }
    }
}
