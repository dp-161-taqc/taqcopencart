using OpenQA.Selenium;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class CommonPageWithAccountMenu : CommonPage
    {
        private readonly By logoutButton = By.XPath("//*[@class='list-group']//*[text()='Logout']");
        private readonly By passwordButton = By.XPath("//*[@class='list-group']//*[text()='Password']");
        
        public CommonPageWithAccountMenu(IWebDriver driver) : base(driver)
        {
        }

        public LogoutPage ClickLogoutPageButton()
        {
            driver.FindElement(logoutButton).Click();
            return new LogoutPage(driver);
        }

        public PasswordEditPage ClickPasswordButton()
        {
            driver.FindElement(passwordButton).Click();
            return new PasswordEditPage(driver);
        }     
    }
}
