﻿using OpenQA.Selenium;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class CommonPageWithMenu : CommonPage
    {
        public CommonPageWithMenu(IWebDriver driver) : base(driver)
        {
        }

        private By loginButton = By.XPath("//*[@class='list-group']//*[text()='Login']");
        private By registerButton = By.XPath("//*[@class='list-group']//*[text()='Register']");
        private By forgottenPasswordButton = By.XPath("//*[@class='list-group']//*[text()='Forgotten Password']");
        private By addressBookButton = By.XPath("//*[@class='list-group']//*[text()='Address Book']");
        private By myAccountButton = By.XPath("//*[@class='list-group']//*[text()='My Account']");
        private By returnsButton = By.XPath("//*[@id='column-right']/div/a[10]");        


        public LoginPage ClickLoginButton()
        {
            driver.FindElement(loginButton).Click();
            return new LoginPage(driver);
        }

        public RegistrationPage ClickRegisterButton()
        {
            driver.FindElement(registerButton).Click();
            return new RegistrationPage(driver);
        }

        public ForgottenPasswordPage ClickForgottenPasswordButton()
        {
            driver.FindElement(forgottenPasswordButton).Click();
            return new ForgottenPasswordPage(driver);
        }

        public LoginPage ClickAddressBookButton()
        {
            driver.FindElement(addressBookButton).Click();
            return new LoginPage(driver);
        }

        public LoginPage ClickMyAccountButton()
        {
            driver.FindElement(myAccountButton).Click();
            return new LoginPage(driver);
        }

        public LoginPage ClickReturnsInMenuForUnloggedUser()
        {
            driver.FindElement(returnsButton).Click();
            return new LoginPage(driver);
        }

        public ReturnsAccountPage ClickReturnsInMenuForLoggedUser()
        {
            driver.FindElement(returnsButton).Click();
            return new ReturnsAccountPage(driver);
        }
    }
}
