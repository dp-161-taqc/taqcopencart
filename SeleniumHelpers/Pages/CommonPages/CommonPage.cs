﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;
using System.Threading;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class CommonPage : BasePage
    {
        private By searchTextBox = By.CssSelector("#search input");
        private By searchButton = By.CssSelector("#search button");
        private By HeadLogo = By.CssSelector("#logo > a > img");
        private By CategoryLink(string tagText) { return By.PartialLinkText(tagText); }
                
        private By dropdownToggle = By.CssSelector("#top-links > ul > li.dropdown > a");
        private By logIn = By.XPath("//ul[@class='dropdown-menu dropdown-menu-right']//a[contains(text(),'Login')]");
        private By Logout = By.XPath("//a[contains(text(),'Logout')]");
        private By shoppingCart = By.CssSelector("a[title='Shopping Cart']");
        private By wishList = By.CssSelector("a[id='wishlist-total']");
        private By cartTotal = By.CssSelector("#cart-total");
        private By cartTotalClearButton = By.XPath("//button[@class='btn btn-danger btn-xs']");
        private string macElement = "//*[@id=\"content\"]/div[2]/div[1]/div/div[3]/button[1]";
        private By checkOut = By.XPath("//*[@id=\"top-links\"]/ul/li[5]/a");

        
        private By telephoneIcon = By.CssSelector(".fa-phone");
        private By referenceReturnsInFooter = By.XPath("//*[@class='list-unstyled']//*[text()='Returns']");
        private By referenceContactUsInFooter = By.XPath("//*[@class='list-unstyled']//*[text()='About Us']");

        private string сontactUsСategoryInFooter = "Contact Us";
        private By termsAndConditionsXPath = By.XPath("/html/body/footer/div/div/div[1]/ul/li[4]/a");
        private By loginPageTitle = By.TagName("title");
        private By orderHistory = By.XPath("/html/body/footer/div/div/div[4]/ul/li[2]/a");
        private By element = By.XPath("//*[@id=\"content\"]/div[2]/div[1]/div/div[1]/a/img");
        private By addToCart = By.XPath("//*[@id=\"button - cart\"]");


        public CommonPage(IWebDriver driver) : base(driver)
        {
        }

        public CommonPage HoverCategory(string category)
        {
            Actions actions = new Actions(driver);
            actions.Build();
            actions.MoveToElement(driver.FindElement(CategoryLink(category)));
            actions.Perform();
            return new CommonPage(driver);
        }

        public SearchPage ClickCategory(string category)
        {
            driver.FindElement(CategoryLink(category), elementWaitSeconds).Click();
            return new SearchPage(driver);
        }

        public ContactUsPage ClickTelephoneIcon()
        {
            driver.FindElement(telephoneIcon).Click();
            return new ContactUsPage(driver);
        }

        public CommonPage EnterTextInSearchBox(string searchText)
        {
            driver.FindElement(searchTextBox).SendKeys(searchText);
            return new CommonPage(driver);
        }

        public SearchPage ClickSearchButton()
        {
            driver.FindElement(searchButton).Click();
            return new SearchPage(driver);
        }
        public CommonPage ClickDropDownToggle()
        {
            driver.FindElement(dropdownToggle).Click();
            return new CommonPage(driver);
        }
        public LoginPage ClickLogInOfDropDownToggle()
        {
            driver.FindElement(logIn).Click();
            return new LoginPage(driver);
        }

        public LoginPage ClickLogOutOfDropDownToggle()
        {
            driver.FindElement(Logout).Click();
            return new LoginPage(driver);
        }
        public HomePage ClickOnSiteLogo()
        {
            driver.FindElement(HeadLogo).Click();
            return new HomePage(driver);
        }
        public ShoppingCartPage GetShoppingCartPage()
        {
            driver.FindElement(shoppingCart).Click();
            return new ShoppingCartPage(driver);
        }
        public WishListPage GetWishListPage()
        {
            Thread.Sleep(3000);
            driver.FindElement(wishList).Click();
            return new WishListPage(driver);
        }

        public CommonPage AddToCartFromMain()
        {
            driver.FindElement(By.XPath(macElement)).Click();
            return new CommonPage(driver);
        }
        public CommonPage AddToCartFromProductPage()
        {
            driver.FindElement(element).Click();
            driver.FindElement(addToCart).Click();
            return new CommonPage(driver);
        }
        public ShoppingCartPage GoToCheckout()
        {
            driver.FindElement(checkOut).Click();
            return new ShoppingCartPage(driver);
        }
        public CheckOutPage GoToCheckoutSecond()
        {
            driver.FindElement(checkOut).Click();
            return new CheckOutPage(driver);
        }

        public ReturnsFormPage ClickReferenceReturnsInFooter()
        {
            driver.FindElement(referenceReturnsInFooter).Click();
            return new ReturnsFormPage(driver);
        }
        public SearchPage GetChecker()
        {
            return new SearchPage(driver);
        }

        public ContactUsPage ClickCategoryContactUsInFooter()
        {
            driver.FindElement(CategoryLink(сontactUsСategoryInFooter), elementWaitSeconds).Click();
            return new ContactUsPage(driver);
        }

        public AboutUsPage ClickReferenceAboutUsInFooter()
        {
            driver.FindElement(referenceContactUsInFooter).Click();
            return new AboutUsPage(driver);
        }


        public TermsAndConditionPage ClickCategoryTermsAndConditionInFooter()
        {
            driver.FindElement(termsAndConditionsXPath).Click();
            return new TermsAndConditionPage(driver);
        }

        public OrderHistoryPage ClickOrderHistoryInFooter()
        {
            driver.FindElement(orderHistory).Click();
            return new OrderHistoryPage(driver);
        }

        public void NavigatePage(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public string GetPageTitle(By pageTitle)
        {
            return driver.FindElement(pageTitle).GetAttribute("innerText");
        }

        public void CheckCommonPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl  + string.Format(URL.PathToHomePage);
            string expectedTitle = GetPageTitle(loginPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }
    }
}
