﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class CheckOutSuccessPage : CommonPage
    {
        private By contactUs = By.XPath("//*[@id=\"content\"]/p[2]/a");
        private By successXpath = By.XPath("  //*[@id=\"content\"]/h1");
        private By continueXpath = By.XPath("//*[@id=\"content\"]/div/div/a");
        private By orderHistory = By.XPath("//*[@id=\"content\"]/p[2]/a[2]");
        private By accountDownloads = By.XPath("//*[@id=\"content\"]/p[3]/a");
        private By account = By.XPath("//*[@id=\"content\"]/p[2]/a[1]");

        public CheckOutSuccessPage(IWebDriver driver) : base(driver)
        {
        }
        public string GetSuccess()
        {
            Thread.Sleep(2000);
            IWebElement field = driver.FindElement(successXpath);
            string content = field.Text;
            return content;
        }
        public CommonPage Continue()
        {

            Thread.Sleep(2000);
            driver.FindElement(continueXpath).Click();
            return new CommonPage(driver);
        }
        public ContactUsPage ContactUs()
        {
            Thread.Sleep(2000);
            driver.FindElement(contactUs).Click();
            return new ContactUsPage(driver);
        }

        public OrderHistoryPage GotoOrderHistory()
        {
            driver.FindElement(orderHistory).Click();
            return new OrderHistoryPage(driver);
        }

        public AccountDownloadsPage GotoAccountDownloads()
        {
            driver.FindElement(accountDownloads).Click();
            return new AccountDownloadsPage(driver);
        }

        public AccountPage GotoAccount()
        {
            driver.FindElement(account).Click();
            return new AccountPage(driver);
        }

        public CheckOutSuccessPage CheckSuccessfulMessage()
        {
            Thread.Sleep(2000);
            Assert.AreEqual("Your order has been placed!", GetSuccess());
            return this;
        }

    }
}
