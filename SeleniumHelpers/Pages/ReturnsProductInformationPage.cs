﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Pages.CommonPages;

namespace SeleniumHelpers.Pages
{
    public class  ReturnsProductInformationPage:CommonPageWithMenu
    {
        protected string expectedReturnId;

        public ReturnsProductInformationPage(IWebDriver driver) : base(driver)
        {
        }

        public ReturnsProductInformationPage(IWebDriver driver, string returnId) : base(driver)
        {
            expectedReturnId = returnId;
        }

        private By titleReturnInfo = By.XPath("//*[@class='account-return-info']//*[text()='Return Information']");
        private By returnId = By.XPath("//*[@id='content']/table/tbody/tr/td[1]");

        public void CheckProductReturnsInformationPageOpened()
        {
            IWebElement expectedElement = driver.FindElement(titleReturnInfo);
            Assert.NotNull(expectedElement);
        }

        public void CheckReturnIdInfoIsCorrect() 
        {
            string returnIdInfo = driver.FindElement(returnId).GetAttribute("innerText");
            string actualReturnId= returnIdInfo.Split('\n')[0].Split(':')[1].Trim();
            Assert.AreEqual(expectedReturnId,actualReturnId);
        }
    }
}
