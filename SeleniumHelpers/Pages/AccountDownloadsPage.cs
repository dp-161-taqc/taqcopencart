﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Models;
using SeleniumHelpers.Data;
using NUnit.Framework;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class AccountDownloadsPage : CommonPage
    {
        public AccountDownloadsPage(IWebDriver driver) : base(driver)
        {
        }
        private By accountDownloads = By.CssSelector("#content > h2");


        public string AccountDownloadsField()
        {
            Thread.Sleep(2000);
            return driver.FindElement(accountDownloads).Text;
        }
        public void IsAccountDownloads()
        {
            Thread.Sleep(2000);
            StringAssert.Contains("Account Downloads", AccountDownloadsField());
        }
        

    }
}

