﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumHelpers.Utils;
using System.Threading;

namespace SeleniumHelpers.Pages
{
    public class CommonPage : ConfigurationHelper
    {
        private By searchTextBox = By.CssSelector("#search input");
        private By searchButton = By.CssSelector("#search button");
        private By CategoryLink(string tagText) { return By.PartialLinkText(tagText); }
        private By dropdownToggle = By.CssSelector("#top-links > ul > li.dropdown > a");
        private By logIn = By.CssSelector("#top-links > ul > li.dropdown.open > ul > li:nth-child(2) > a");
        private By shoppingCart = By.CssSelector("a[title='Shopping Cart']");
        private By cartTotal = By.CssSelector("#cart-total");
        private By cartTotalClearButton = By.XPath("//button[@class='btn btn-danger btn-xs']");
        private string macElement ="//*[@id=\"content\"]/div[2]/div[1]/div/div[3]/button[1]";
        public CommonPage(IWebDriver driver) : base(driver)
        {
        }

        public CommonPage HoverCategory(string category)
        {
            Actions actions = new Actions(driver);
            actions.Build();
            actions.MoveToElement(driver.FindElement(CategoryLink(category)));
            actions.Perform();
            return new CommonPage(driver);
        }

        public SearchPage ClickCategory(string category)
        {
            driver.FindElement(CategoryLink(category), elementWaitSeconds).Click();
            return new SearchPage(driver);
        }

        public CommonPage EnterTextInSearchBox(string searchText)
        {
            driver.FindElement(searchTextBox).SendKeys(searchText);
            return new CommonPage(driver);
        }

        public SearchPage ClickSearchButton()
        {
            driver.FindElement(searchButton).Click();
            return new SearchPage(driver);
        }
        public CommonPage ClickDropDownToggle()
        {
            driver.FindElement(dropdownToggle).Click();
            return new CommonPage(driver);
        }
        public LoginPage ClickLogInOfDropDownToggle()
        {
            driver.FindElement(logIn).Click();
            return new LoginPage(driver);
        }
        public ShoppingCartPage GetShoppingCartPage()
        {
            driver.FindElement(shoppingCart).Click();
            Thread.Sleep(3000); 
            return new ShoppingCartPage(driver);
        }
        public CommonPage AddToCartFromMain()
        {
            driver.FindElement(By.XPath(macElement)).Click();
            return new CommonPage(driver);
        }
        public CommonPage AddToCartFromProductPage()
        {
            driver.FindElement(By.XPath("//*[@id=\"content\"]/div[2]/div[1]/div/div[1]/a/img")).Click();
            driver.FindElement(By.XPath("//*[@id=\"button - cart\"]")).Click();
            return new CommonPage(driver);
        }
        public ShoppingCartPage GoToCheckout()
        {
            driver.FindElement(By.XPath("//*[@id=\"top-links\"]/ul/li[5]/a")).Click();
            return new ShoppingCartPage(driver);
        }

        public void ClearShoppingCart()
        {
            driver.FindElement(cartTotal).Click();
            Thread.Sleep(2000);
            driver.FindElement(cartTotalClearButton).Click();
        }
    }
}
