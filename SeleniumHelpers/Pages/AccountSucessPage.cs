using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class AccountSucessPage : CommonPageWithAccountMenu
    {
        private readonly By accountSucessPageTitle = By.TagName("title");

        public AccountSucessPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToAccountSucessPage));
        }

        public void CheckAccountSucessPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl + string.Format(URL.PathToAccountSucessPage);
            string expectedTitle = GetPageTitle(accountSucessPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }
    }
}
