using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class SearchPage : CommonPageWithMenu
    {
        public int[] Prices { get; private set; }

        public SearchPage(IWebDriver driver) : base(driver)
        {
        }

        #region List Selectors
        private readonly By buttonsAddToCartList = By.CssSelector("#content div.button-group > button:nth-child(1)");
        private readonly By productFramesList = By.CssSelector(".col-xs-12");
        private readonly By productNamesList = By.CssSelector(".caption h4 a");
        private readonly By productImagesList = By.CssSelector("#content > * a > img");
        private readonly By searchCategoryNamesList = By.XPath("//option['*']");
        private readonly By productsLimiterValuesList = By.XPath("//*[@id=\"input-limit\"]/option['*']");
        private readonly By sortProductsByTypesList = By.XPath("//*[@id=\"input-sort\"]/option['*']");
        private readonly By productPricesList = By.XPath("//*[@class=\"price-tax\"]");
        private readonly By buttonsCompareProductList = By.XPath("//*[@data-original-title='Compare this Product']");
        private readonly By buttonsAddToWishList = By.XPath("//*[@data-original-title='Add to Wish List']");
        private readonly By blockProductCaptionList = By.XPath("//*[@class='caption']");
        
        #endregion
        #region Single Selectors      
        private readonly By extraSearchTextBox = By.CssSelector("#input-search");
        private readonly By extraSearchButton = By.CssSelector("#button-search");
        private readonly By selectorCategory = By.CssSelector("#content * > select");
        private readonly By selectorSortProductsBy = By.CssSelector("#input-sort");
        private readonly By selectorProductsLimiter = By.CssSelector("#input-limit");
        private readonly By firstHeader = By.XPath("//h1[contains(text(),'Search')]");
        private readonly By buttonlistView = By.XPath("//button[@id='list-view']");
        private readonly By buttonGridView = By.XPath("//button[@id='grid-view']");
        private readonly By checkBoxSearchInSubCategory = By.XPath("//input[@name='sub_category']");
        private readonly By checkBoxSearchInProductDescription = By.XPath("//input[@id='description']");
        private readonly By secondHeader = By.XPath("//*[@id=\"content\"]/h2");
        public readonly By productCompareLink = By.XPath("//a[@id='compare-total']");
        #endregion
        #region Checkers
        public SearchPage ChekIfNoSuchElementThenPass(By by)
        {
            try
            {
                driver.FindElement(by);
                Assert.Pass("Element present on page");
            }
            catch (NoSuchElementException)
            {
                Assert.Pass("No such element on page");
            }
            return this;
        }

        public SearchPage CheckIsSearchPageLoaded()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            bool element = wait.Until(condition =>
            {
                try
                {
                    IWebElement elementToBeDisplayed = driver.FindElement(firstHeader);
                    return elementToBeDisplayed.Displayed;
                }
                catch (StaleElementReferenceException)
                {
                    Assert.Fail("The page is not loaded, or loaded with an error!");
                    return false;
                }
                catch (NoSuchElementException)
                {
                    Assert.Fail("The page is not loaded, or loaded not fully!");
                    return false;
                }
            });
            return this;
        }

        public SearchPage CheckIfCompareProductLinkOnPageThenFail()
        {
            try
            {
                driver.FindElement(productCompareLink).Click();
                Assert.Fail("Product compare link was clicked");
            }
            catch (NoSuchElementException)
            {
                Assert.Pass("No such element on current page");
            }

            return this;
        }

        public SearchPage CheckIsAvalibleCheckBoxSearchInSubCategory()
        {
            IWebElement element = driver.FindElement(checkBoxSearchInSubCategory);
            Assert.IsNull(element.GetAttribute("disabled"));
            return this;
        }

        public SearchPage CheckIsInaccessibleCheckBoxSearchInSubCategory()
        {
            IWebElement element = driver.FindElement(checkBoxSearchInSubCategory);
            Assert.IsNotNull(element.GetAttribute("disabled"));
            return this;
        }

        public SearchPage CheckSizeAllElementsInList(int expectedWidth, int expectedHeight, IList<IWebElement> list)
        {
            List<string> wrongSizesList = new List<string>();
            int i = 1;
            foreach (IWebElement element in list)
            {

                Console.WriteLine
                    ($"{i}.  Checked element size was:  {element.GetCssValue("width")}  X  {element.GetCssValue("height")}");
                ++i;
                if (!(element.GetCssValue("width").StartsWith(expectedWidth.ToString()) && 
                    element.GetCssValue("height").StartsWith(expectedHeight.ToString())))
                {
                    wrongSizesList.Add
                        ($"{i-1}. Element has wrong size:  {element.GetCssValue("width")}  X  {element.GetCssValue("height")}");
                }
            }
            if (wrongSizesList.Count > 0)
            {
                Console.WriteLine();

                foreach (string size in wrongSizesList)
                {
                    Console.WriteLine(size);

                }
                Assert.Fail($"Quantity elements with wrong size: {wrongSizesList.Count}");
            }
            else
            {
                Assert.Pass("All element sizes are correct");
            }
            return this;
        } 

        public SearchPage CheckElementSize(int length, int width, IWebElement element)
        {
            Assert.AreEqual(new Size(length, width), element.Size);
            return this;
        }

        public SearchPage CheckAreEqual(object expected, object actual)
        {
            Assert.AreEqual(expected, actual);
            return this;
        }

        public SearchPage CheckCollectionIsOrdered(IEnumerable collection)
        {
            CollectionAssert.IsOrdered(collection);
            return this;
        }

        public SearchPage CheckCollectionIsOrderedDescending(IEnumerable<int> collection)
        {
            IEnumerable result = collection.Reverse();
            CollectionAssert.IsOrdered(result);
            return this;
        }

        public SearchPage CheckCollectionIsOrderedDescending(IEnumerable<string> collection)
        {
            IEnumerable result = collection.Reverse();
            CollectionAssert.IsOrdered(result);
            return this;
        }

        public SearchPage CheckStringEndsWith(string expected, string actual)
        {
            StringAssert.EndsWith(expected, actual);
            return this;
        }

        public SearchPage CheckStringContains(string expected, string actual)
        {
            StringAssert.Contains(expected, actual);
            return this;
        }

        public SearchPage CheckIsElementDisplayed(By by)
        {
            IWebElement element = driver.FindElement(by);
            Assert.AreEqual(true, element.Displayed);
            return this;
        }

        public SearchPage GetFailChecker(string message)
        {
            Assert.Fail(message);
            return this;
        }

        public SearchPage GetPassChecker(string message)
        {
            Assert.Pass(message);
            return this;
        }

        public SearchPage CheckTagsContainsProduct(string expectedFirstName, string expectedSecondName)
        {
            Assert.Multiple(() => 
            {
                Assert.Contains(expectedFirstName, ProductNamesList().Select(el => el.Text).ToList());
                Assert.Contains(expectedSecondName, ProductNamesList().Select(el => el.Text).ToList());
            });
            return this;
        }
        #endregion
        #region Lists of WebElements       
        public IList<IWebElement> ProductFramesList()
        {
            return driver.FindElements(productFramesList);
        }

        public IList<IWebElement> BlockProductCaptionList()
        {
            return driver.FindElements(blockProductCaptionList);
        }

        public IList<IWebElement> ButtonsAddToCartList()
        {
            return driver.FindElements(buttonsAddToCartList);
        }
        
        public IList<IWebElement> ButtonsAddToWishList()
        {
            return driver.FindElements(buttonsAddToWishList);
        }
 
        public IList<IWebElement> ButtonsCompareProductList()
        {
            return driver.FindElements(buttonsCompareProductList);
        }

        public IList<IWebElement> ProductPricesList()
        {
            return driver.FindElements(productPricesList);
        }

        public IList<IWebElement> ProductsLimiterValuesList()
        {
            return driver.FindElements(productsLimiterValuesList);
        }

        public IList<IWebElement> SortProductsByTypesList()
        {
            return driver.FindElements(sortProductsByTypesList);
        }

        public IList<IWebElement> SearchCategoryNamesList()
        {
            return driver.FindElements(searchCategoryNamesList);
        }

        public IList<IWebElement> ProductImagesList()
        {
            return driver.FindElements(productImagesList);
        }

        public IList<IWebElement> ProductNamesList()
        {
            return driver.FindElements(productNamesList);
        }
        #endregion
        #region Local Actions
        public ProductComparePage ClickOnCompareProductLink()
        {
            driver.FindElement(productCompareLink).Click();
            return new ProductComparePage(driver);
        }

        public int GetNumberOfProductsToCompareByLink()
        {
            IWebElement element = driver.FindElement(productCompareLink);
            string numbersOnly = Regex.Replace(element.Text, "[^0-9]", "");
            int.TryParse(numbersOnly, out int parsedIntValue);
            return parsedIntValue + 1;
        }

        public SearchPage AddProductsToCompare(int quantityOfProducts)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;

            for (int i = 0; i < quantityOfProducts; i++)
            {
                while ((long)js.ExecuteScript("return pagePosition = window.pageYOffset") > 0)
                {
                    Thread.Sleep(200);
                }
                ButtonsCompareProductList()[i].Click();
            }
            return this;
        }

        public SearchPage EnterTextInExtraSearchBox(string searchText)
        {
            driver.FindElement(extraSearchTextBox).SendKeys(searchText);
            return this;
        }

        public SearchPage ClickExtraSearchButton()
        {
            driver.FindElement(extraSearchButton).Click();
            return this;
        }

        public List<string> GetProductNamesList()
        {
            List<string> productNamesList = new List<string>();

            foreach (IWebElement productName in ProductNamesList())
            {
                productNamesList.Add(productName.Text);
            }
            return productNamesList;
        }

        public List<int> GetPrices()
        {
            Regex rgx = new Regex("[^0-9]");
            List<string> prices = new List<string>();

            foreach (IWebElement price in ProductPricesList())
            {
                    prices.Add(rgx.Replace(price.Text, ""));
            }
            return prices.Select(int.Parse).ToList();
        }


        public SearchPage TickCheckBoxSearchInProductDescription()
        {
            driver.FindElement(checkBoxSearchInProductDescription).SendKeys(Keys.Space);
            return this;
        }

        public SearchPage TryToTickCheckBoxSubCategory()
        {
            driver.FindElement(checkBoxSearchInSubCategory).SendKeys(Keys.Space);
            return this;
        }

        public SearchPage ClickSelectorSortProductsBy()
        {
            driver.FindElement(selectorSortProductsBy).Click();
            return this;
        }

        public SearchPage ClickButtonListView()
        {
            driver.FindElement(buttonlistView).Click();
            return this;
        }

        public SearchPage ClickButtonGridView()
        {
            driver.FindElement(buttonGridView).Click();
            return this;
        }

        public ProductPage ClickProductInList(string productName)
        {
            ProductNamesList().FirstOrDefault(el => el.Text == productName).Click();
            return new ProductPage(driver);
        }

        public ProductPage ClickProductInImagesListByIndex(int productIndex)
        {
            ProductImagesList().ElementAt(productIndex).Click();
            return new ProductPage(driver);
        }

        public SearchPage SortProductsBy(string sortType)
        {
            SortProductsByTypesList().FirstOrDefault(el => el.Text == sortType).Click();
            return this;
        }

        public SearchPage ChooseSearchCategory(string searchCategory)
        {
            driver.FindElement(selectorCategory).Click();
            SearchCategoryNamesList().FirstOrDefault(el => el.Text == searchCategory).Click();
            return this;
        }

    }
}
#endregion