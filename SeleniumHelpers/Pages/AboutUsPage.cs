﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Pages.CommonPages;

namespace SeleniumHelpers.Pages
{
    public class AboutUsPage : CommonPage
    {
        public AboutUsPage(IWebDriver driver) : base(driver)
        {
        }

        private By titleAboutUs = By.XPath("//*[@class='breadcrumb']//*[text()='About Us']");

        public void CheckAboutUsPageOpened()
        {
            IWebElement AboutUs = driver.FindElement(titleAboutUs);
            Assert.NotNull(AboutUs);
        }
    }
}
