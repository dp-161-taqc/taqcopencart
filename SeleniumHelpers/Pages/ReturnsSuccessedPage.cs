﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Models;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class ReturnsSuccessedPage:CommonPageWithMenu
    {       
        public ReturnsSuccessedPage(IWebDriver driver) : base(driver)
        {
        }
        
        private readonly By successMessageForReturns = By.CssSelector("#content > p:nth-child(2)");
        private readonly By buttonContinue = By.XPath("//*[@id='content']/div/div/a");

        public HomePage ClickButtonContinue()
        {           
            driver.FindElement(buttonContinue).Click();
            return new HomePage(driver);
        }       

        public void CheckSuccessMessageForReturns()
        {
            string actualMessege = driver.FindElement(successMessageForReturns,elementWaitSeconds).GetAttribute("innerText");           
            StringAssert.Contains("Thank you for submitting", actualMessege);
        }
    }
}
