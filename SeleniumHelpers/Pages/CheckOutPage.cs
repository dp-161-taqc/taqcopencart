﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Models;
using NUnit.Framework;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class CheckOutPage : CommonPage
    {
        public CheckOutPage(IWebDriver driver) : base(driver)
        {
        }
        private By checkOutRadio = By.XPath("//input[@value='guest']");
        private By guestCheckOut = By.Id("button-account");
        private By loginXpath = By.XPath("//*[@id=\"input-email\"]");
        private By passwordXpath = By.XPath("//*[@id=\"input-password\"]");
        private By continueBillingXpath = By.XPath("//*[@id=\"button-payment-address\"]");
        private By continueBillingXpathGuest = By.XPath("//*[@id=\"button-guest\"]");
        private By continueShippingXpath = By.Id("button-shipping-address");
        private By checkCkeckBoxXpath = By.XPath("//*[@id=\"collapse-payment-method\"]/div/div[2]/div/input[1]");
        private By continueShippingXpathGuest = By.XPath("//*[@id=\"button-guest\"]");
        private By continueDeliveryXpath = By.Id("button-shipping-method");
        private By continuePaymentXpath = By.XPath("//*[@id=\"button-payment-method\"]");
        private By confirmXpath = By.XPath("//*[@id=\"button-confirm\"]");
        private By pageTitle = By.TagName("title");
        private By termsAndConditionXPath = By.ClassName("agree");
        private By commentField = By.Name("comment");
        private By termsMessage = By.ClassName("typography-intro large-8 medium-10 small-12 center");
        private By noTermsAndConditionCheck = By.CssSelector("#collapse-payment-method > div > div.alert.alert-danger");
        private By newAdressForLoggedUserCheck = By.CssSelector("#collapse-payment-address > div > form > div:nth-child(3) > label");
        private By newDeliveryAdress = By.CssSelector("#collapse-shipping-address > div > form > div:nth-child(3) > label");
        private By newDeliveryAdressGuest = By.XPath("//*[@id=\"collapse - payment - address\"]/div/div[2]/label");
        private By continueDeliveryGuest = By.XPath("//*[@id=\"collapse - shipping - address\"]/div/form/div[10]/div");
        private By mainPage = By.XPath("//*[@id=\"logo\"]/a/img");
        

        //BillingFields
        private By billingFaxXPath = By.Id("input-payment-fax");
        private By billingCompanyXPath = By.Id("input-payment-company");
        private By firstNameField = By.XPath("//*[@id=\"input-payment-firstname\"]");
        private By countryField = By.Id("input-payment-country");
        private By regionField = By.XPath("//*[@id=\"input-payment-zone\"]");
        private By lastNameField = By.XPath("//*[@id=\"input-payment-lastname\"]");
        private By emailField = By.XPath("//*[@id=\"input-payment-email\"]");
        private By telephoneField = By.XPath("//*[@id=\"input-payment-telephone\"]");
        private By firstAdressField = By.XPath("//*[@id=\"input-payment-address-1\"]");
        private By secondAdressField = By.XPath("//*[@id=\"input-payment-address-2\"]");
        private By postcodField = By.XPath("//*[@id=\"input-payment-postcode\"]");
        private By cityField = By.Id("input-payment-city");
        private By firstNameDelivery = By.XPath("//*[@id=\"input-shipping-firstname\"]");
        private By lastNameDelivery = By.XPath("//*[@id=\"input-shipping-lastname\"]");
        private By adressDelivery = By.Id("input-shipping-address-1");
        private By adress2Delivery = By.XPath("//*[@id=\"input - shipping - address - 2\"]");
        private By cityDelivery = By.XPath("//*[@id=\"input-shipping-city\"]");
        private By companyDelivery = By.XPath("//*[@id=\"input-shipping-company\"]");

        public CheckOutPage GuestCheckOutRadio()
        {
            driver.FindElement(checkOutRadio).Click();
            
            
            return new CheckOutPage(driver);
        }
        public CheckOutPage GuestCheckOut()
        {
           
            //Thread.Sleep(2000);
            driver.FindElement(guestCheckOut).Click();
            
              
            return new CheckOutPage(driver);
        }
        

        public CheckOutPage EnterLogin(string email)
        {
            driver.FindElement(loginXpath).SendKeys(email);
            return new CheckOutPage(driver);
        }

        
        public CheckOutPage EnterPassword(string password)
        {
            driver.FindElement(passwordXpath).SendKeys(password + Keys.Enter);
            return new CheckOutPage(driver);
        }

        
        public CheckOutPage ContinueBillingButton()
        {
            driver.FindElement(continueBillingXpath).Click();
            return new CheckOutPage(driver);
        }
        public CheckOutPage ContinueBillingStep2()
        {
            EnterLogin(UserRegister.GetUser("TestLast").Email);
            EnterPassword(UserRegister.GetUser("TestLast").Password);
            ContinueBillingButton();
            return new CheckOutPage(driver);
        }
        public CheckOutPage LoggIn()
        {
            EnterLogin(UserRegister.GetUser("TestLast").Email);
            EnterPassword(UserRegister.GetUser("TestLast").Password);
            return new CheckOutPage(driver);
        }

        public CheckOutPage NewAdressForLoggedUser()
        {
            driver.FindElement(newAdressForLoggedUserCheck).Click();
            return new CheckOutPage(driver);
        }
        public CheckOutPage ContinueBillingStep2Guest()
        {
            driver.FindElement(continueBillingXpathGuest).Click();
            return new CheckOutPage(driver);
        }

        public CheckOutPage newDeliveryAdressStep2Guest()
        {
            driver.FindElement(newDeliveryAdressGuest).Click();
            return new CheckOutPage(driver);
        }

        public CheckOutPage ContinueDeliveryStep3Guest()
        {
            driver.FindElement(continueDeliveryGuest).Click();
            return new CheckOutPage(driver);
        }


        public CheckOutPage EnterFirstName()
        {
            driver.FindElement(firstNameField).SendKeys(UserRegister.GetUser("TestLast").Firstname);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterCompany()
        {
            driver.FindElement(billingCompanyXPath).SendKeys(UserRegister.GetUser("TestLast").Company);
            return new CheckOutPage(driver);
        }

        public CheckOutPage InputCountry()
        {
            SelectElement element = new SelectElement(driver.FindElement(countryField));
            element.SelectByText(UserRegister.GetUser("TestLast").Country);
            return new CheckOutPage(driver);

        }

        public CheckOutPage InputRegion()
        {
            SelectElement element = new SelectElement(driver.FindElement(regionField));
            element.SelectByText(UserRegister.GetUser("TestLast").Region);
            return new CheckOutPage(driver);

        }
        public CheckOutPage EnterLastName()
        {
            driver.FindElement(lastNameField).SendKeys(UserRegister.GetUser("TestLast").Lastname);
            return new CheckOutPage(driver);    
        }

        public CheckOutPage EnterEmail()
        {
            driver.FindElement(emailField).SendKeys(UserRegister.GetUser("TestLast").Email);
            return new CheckOutPage(driver);
        }

        public CheckOutPage EnterPhone()
        {
            driver.FindElement(telephoneField).SendKeys(UserRegister.GetUser("TestLast").Telephone);
            return new CheckOutPage(driver);
        }

        public CheckOutPage EnterAdress1()
        {
            driver.FindElement(firstAdressField).SendKeys(UserRegister.GetUser("TestLast").Address1);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterAdress2()
        {
            driver.FindElement(secondAdressField).SendKeys(UserRegister.GetUser("TestLast").Address2);
            return new CheckOutPage(driver);
        }

        public CheckOutPage EnterPostcode()
        {
            driver.FindElement(postcodField).SendKeys(UserRegister.GetUser("TestLast").Postcode);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterFax()
        {
            driver.FindElement(billingFaxXPath).SendKeys(UserRegister.GetUser("TestLast").Fax);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterCity()
        {
            driver.FindElement(cityField).SendKeys(UserRegister.GetUser("TestLast").City);
            return new CheckOutPage(driver);
        }

        
        public CheckOutPage ContinueShippingStep3()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(continueShippingXpath).Click();
            
            return new CheckOutPage(driver);
        }

        public CheckOutPage NewDeliveryAdressStep3()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(newDeliveryAdress).Click();
            return new CheckOutPage(driver);
        }

        public CheckOutPage EnterFirstNameDelivery()
        {
            driver.FindElement(firstNameDelivery).SendKeys(UserRegister.GetUser("TestLast").Firstname);
            return new CheckOutPage(driver);
        }
        
            public CheckOutPage EnterLastNameDelivery()
        {
            driver.FindElement(lastNameDelivery).SendKeys(UserRegister.GetUser("TestLast").Lastname);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterCompanyDelivery()
        {
            driver.FindElement(companyDelivery).SendKeys(UserRegister.GetUser("TestLast").Company);
            return new CheckOutPage(driver);
        }

        public CheckOutPage EnterAdressDelivery()
        {
            driver.FindElement(adressDelivery).SendKeys(UserRegister.GetUser("TestLast").Address1);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterAdress2Delivery()
        {
            driver.FindElement(adress2Delivery).SendKeys(UserRegister.GetUser("TestLast").Address2);
            return new CheckOutPage(driver);
        }
        public CheckOutPage EnterCityDelivery()
        {
            driver.FindElement(cityDelivery).SendKeys(UserRegister.GetUser("TestLast").City);
            return new CheckOutPage(driver);
        }
        

        public CheckOutPage EnterComment()
        {
            driver.FindElement(commentField).SendKeys("comment");
            return new CheckOutPage(driver);
        }
        public CheckOutPage ContinueShippingStep3Guest()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(continueShippingXpathGuest).Click();
            return new CheckOutPage(driver);
        }
       
        
        public CheckOutPage ContinueDeliveryStep4()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(continueDeliveryXpath).Click();
            return new CheckOutPage(driver);
        }
      

        
        public CheckOutPage CheckCkeckBoxStep5()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(checkCkeckBoxXpath).Click();
            return new CheckOutPage(driver);
        }


        public CheckOutPage ContinuePaymentStep5()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(continuePaymentXpath).Click();
            return new CheckOutPage(driver);
        }
        
        public CheckOutSuccessPage Confirm()
        {
            Thread.Sleep(1000);
            //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.FindElement(confirmXpath).Click();
            Thread.Sleep(1000);
            return new CheckOutSuccessPage(driver);
        }
        public TermsAndConditionPage GoTermsAndConditionStep5()
        {
            Thread.Sleep(1000);
            driver.FindElement(termsAndConditionXPath).Click();
            return  new TermsAndConditionPage(driver);
        }
        public string GetMessage()
        {
            
            IWebElement field = driver.FindElement(termsMessage);
            string content = field.Text;
            return content;
           
        }

        public string noTermsAndConditionMessage()
        {
            
            return driver.FindElement(noTermsAndConditionCheck).Text;
        }

        
            public void NoTermsAndCondition(string expectedMessage)
        {
            
            StringAssert.Contains(expectedMessage, noTermsAndConditionMessage());
        }

        public CommonPage GotoMain()
        {
            driver.FindElement(mainPage).Click();
            return new CommonPage(driver);
        }

       


    }
}