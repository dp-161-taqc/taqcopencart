﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class LogoutPage : CommonPageWithMenu
    {
        private readonly By logoutPageTitle = By.TagName("title");

        public LogoutPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToAccountLogoutPage));
        }

        public void CheckLogoutPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl + string.Format(URL.PathToAccountLogoutPage);
            string expectedTitle = GetPageTitle(logoutPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }
    }
}
