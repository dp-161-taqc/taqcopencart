﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages
{
    public class ContactUsSuccessPage : CommonPageWithMenu
    {
        public ContactUsSuccessPage(IWebDriver driver) : base(driver)
        {
        }

        private By contactUsSuccessMessage = By.CssSelector(".row + h1 +p");
        private By continueButton = By.CssSelector(".btn-primary");
        private By featuredTitle = By.CssSelector("#content > h3");
           
        public ContactUsSuccessPage CheckContactUsSuccessMessage()
        {
            StringAssert
                .Contains("Your enquiry has been successfully", GetContactUsSuccessMessage());
            return this;
        }

        public string GetContactUsSuccessMessage()
        {
            return driver.FindElement(contactUsSuccessMessage, elementWaitSeconds).Text;
        }

        public HomePage ClickContactUsContinueButton()
        {
            driver.FindElement(continueButton).Click();
            return new HomePage(driver);
        }
    }
}
