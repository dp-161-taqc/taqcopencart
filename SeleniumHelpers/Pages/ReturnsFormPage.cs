﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class ReturnsFormPage : CommonPageWithMenu
    { 
        public ReturnsFormPage(IWebDriver driver) : base(driver)
        {
        } 

        private readonly By submitButton = By.XPath(@"//input[@value ='Submit']");
        private readonly By backButton = By.CssSelector("div.pull-left a");
        private readonly By messageForReturnsForm = By.CssSelector("#content > p");


        private readonly By firstName = By.CssSelector("#input-firstname");
        private readonly By firstNameWarningMessage = By.XPath(@"//input[@id ='input-firstname']/following::div");
       
        private readonly By lastName = By.CssSelector("#input-lastname");       
        private readonly By lastNameWarningMessage = By.XPath(@"//input[@id = 'input-lastname']/following::div");

        private readonly By email = By.CssSelector("#input-email");
        private readonly By emailWarningMessage = By.XPath(@"//input[@id = 'input-email']/following::div");

        private readonly By telephone = By.CssSelector("#input-telephone");
        private readonly By telephoneWarningMessage = By.XPath(@"//input[@id ='input-telephone']/following::div");

        private readonly By oderId = By.CssSelector("#input-order-id");
        private readonly By oderIdWarningMessage = By.XPath(@"//input[@id ='input-order-id']/following::div");

        private readonly By productName = By.CssSelector("#input-product");
        private readonly By productNameWarningMessage = By.XPath(@"//input[@id ='input-product']/following::div");

        private readonly By productCode = By.CssSelector("#input-model");
        private readonly By productCodeWarningMessage = By.XPath(@"//input[@id ='input-model']/following::div");

        private readonly By quantity = By.CssSelector("#input-quantity");

        private readonly By reasonForReturns = By.CssSelector(".radio label");        

        private readonly By productIsOpenedNo = By.CssSelector(".radio-inline input[type=radio][checked]");

        private readonly By otherDetails = By.CssSelector("#input-comment");


        public IList<IWebElement> ReasonForReturnsList()
        {
            return driver.FindElements(reasonForReturns);
        }

        public ReturnsFormPage ClickReasonForReturns(string reasonForReturn)
        {          
            foreach (IWebElement reasonCurrent in ReasonForReturnsList())
            {
                if (reasonCurrent.GetAttribute("innerText") == reasonForReturn)
                {                  
                    reasonCurrent.Click();
                }  
            }
            return this;
        }    

        public ReturnsFormPage SetFirstName(string firstName)
        {
            driver.FindElement(this.firstName).Clear();
            driver.FindElement(this.firstName).SendKeys(firstName);
            return this;
        }

        public ReturnsFormPage SetLastName(string lastName)
        {
            driver.FindElement(this.lastName).Clear();
            driver.FindElement(this.lastName).SendKeys(lastName);
            return this;
        }

        public ReturnsFormPage SetDataInFieldEmail(string email)
        {
            driver.FindElement(this.email).Clear();
            driver.FindElement(this.email).SendKeys(email);
            return this;
        }

        public ReturnsFormPage SetDataInFieldProductName(string productName)
        {
            driver.FindElement(this.productName).Clear();
            driver.FindElement(this.productName).SendKeys(productName);
            return this;
        }

        public ReturnsFormPage SetDataInFieldProductCode(string productCode)
        {
            driver.FindElement(this.productCode).Clear();
            driver.FindElement(this.productCode, elementWaitSeconds).SendKeys(productCode);
            return this;
        }

        public ReturnsFormPage SetDataInFieldTelephone(string telephone)
        {
            driver.FindElement(this.telephone).Clear();
            driver.FindElement(this.telephone).SendKeys(telephone);
            return this;
        }

        public ReturnsFormPage SetFieldOderId(string oderId)
        {
            driver.FindElement(this.oderId).Clear();
            driver.FindElement(this.oderId).SendKeys(oderId);
            return this;
        }

        public ReturnsFormPage SetFieldQuantity(string quantity)
        {
            driver.FindElement(this.quantity).Clear();
            driver.FindElement(this.quantity).SendKeys(quantity);
            return this;
        }

        public ReturnsFormPage SetFieldOtherDetails(string otherDetails)
        {
            driver.FindElement(this.otherDetails).Clear();
            driver.FindElement(this.otherDetails).SendKeys(otherDetails);
            return this;
        }

        public string GetDataFromField(By element)
        {
            return driver.FindElement(element).GetAttribute("value");           
            
        }

        public ReturnsFormPage ClickSubmitButton()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
            wait.Until((dr) => dr.FindElement(submitButton));

            driver.FindElement(submitButton).Click();
            return this;
        }

        public ReturnsSuccessedPage ClickSuccessSubmit()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
            wait.Until((dr) => dr.FindElement(submitButton));
           
            driver.FindElement(submitButton).Click();           
            return new ReturnsSuccessedPage(driver);
        }

        public LoginPage ClickBackForUnloggedUser()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
            wait.Until((dr) => dr.FindElement(submitButton));

            driver.FindElement(backButton).Click();
            return new LoginPage(driver);
        }

        public AccountPage ClickBackForLoggedUser()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
            wait.Until((dr) => dr.FindElement(submitButton));

            driver.FindElement(backButton).Click();
            return new AccountPage(driver);
        }

        public string GetWarningMessage(By element)
        {
            return driver.FindElement(element).GetAttribute("innerText");
        }

        public void CheckIncorrectMessageForProductCode()
        {
            string actualMessege = GetWarningMessage(productCodeWarningMessage);
            StringAssert.Contains("Product Model must", actualMessege);
        }

        public void CheckIncorrectMessageForProductName()
        {
            string actualMessege = GetWarningMessage(productNameWarningMessage);
            StringAssert.Contains("Product Name must", actualMessege);
        }

        public void CheckIncorrectMessageForEmail()
        {
            string actualMessege = GetWarningMessage(emailWarningMessage);
            StringAssert.Contains("E-Mail Address does not appear to be valid!", actualMessege);
        }

        public void CheckIncorrectMessageForFieldFirstName()
        {
            string actualMessege = GetWarningMessage(firstNameWarningMessage); 
            StringAssert.Contains("First Name must be", actualMessege);
        }

        public void CheckIncorrectMessageForFieldLastName()
        {
            string actualMessege = GetWarningMessage(lastNameWarningMessage);
            StringAssert.Contains("Last Name must be", actualMessege);
        }

        public void CheckIncorrectMessageForFieldOderId()
        {

            string actualMessege = GetWarningMessage(oderIdWarningMessage); 
            StringAssert.Contains("Order ID required!", actualMessege);
        }

        public void CheckIncorrectMessageForFieldTelephone()
        {
            string actualMessege = GetWarningMessage(telephoneWarningMessage);
            StringAssert.Contains("Telephone must be", actualMessege);
        }

        public ReturnsFormPage FillDataInRequiredFields()
        {
            FieldsInformationForReturnsForm correctData = FieldsInformationForReturnsGenerator.EnterCorrectDataInRequiredFields();
            driver.FindElement(firstName).SendKeys(correctData.FirstName);
            driver.FindElement(lastName).SendKeys(correctData.LastName);
            driver.FindElement(email).SendKeys(correctData.Email);
            driver.FindElement(telephone).SendKeys(correctData.Telephone);
            driver.FindElement(oderId).SendKeys(correctData.OderId);
            driver.FindElement(productName).SendKeys(correctData.ProductName);
            driver.FindElement(productCode).SendKeys(correctData.ProductCode);
            ClickReasonForReturns(correctData.ReasonForReturn);
            driver.FindElement(productIsOpenedNo).Click();
            return this;
        }

        public ReturnsFormPage FillDataInRequiredFieldsForLoggedUser()
        {
            FieldsInformationForReturnsForm correctData = FieldsInformationForReturnsGenerator.EnterCorrectDataInRequiredFields();
            driver.FindElement(oderId).SendKeys(correctData.OderId);
            driver.FindElement(productName).SendKeys(correctData.ProductName);
            driver.FindElement(productCode).SendKeys(correctData.ProductCode);
            ClickReasonForReturns(correctData.ReasonForReturn);
            driver.FindElement(productIsOpenedNo).Click();
            return this;
        }

        public void CheckUrlForReturnsForm()
        {            
            string actualMessege = driver.FindElement(messageForReturnsForm).GetAttribute("innerText");
            StringAssert.Contains("Please complete the form below", actualMessege);
        }

        public void CheckDataInFieldEmailAfterLogging()
        {
            string expectedReturnedUserEmail = Users.UserForReturnsForm().Email;
            string actualReturnedUserEmail = GetDataFromField(email);
            Assert.AreEqual(expectedReturnedUserEmail, actualReturnedUserEmail);
        }

        public void CheckDataInFieldFirstNameAfterLogging()
        {
            string expectedReturnedUserFirstName = Users.UserForReturnsForm().Firstname;
            string actualReturnedUserFirstName = GetDataFromField(firstName);
            Assert.AreEqual(expectedReturnedUserFirstName, actualReturnedUserFirstName);
        }        

        public void CheckDataInFieldLastNameAfterLogging()
        {
            string expectedReturnedUserLastName = Users.UserForReturnsForm().Lastname;
            string actualReturnedUserLastName = GetDataFromField(lastName);
            Assert.AreEqual(expectedReturnedUserLastName, actualReturnedUserLastName);
        }

        public void CheckDataInFieldTelephoneAfterLogging()
        {
            string expectedReturnedUserTelephone = Users.UserForReturnsForm().Telephone;
            string actualReturnedUserTelephone = GetDataFromField(telephone);
            Assert.AreEqual(expectedReturnedUserTelephone, actualReturnedUserTelephone);
        }
    }
}
