﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class ShoppingCartPage : CommonPage
    {
        public ShoppingCartPage(IWebDriver driver)
            : base(driver)
        {

        }
        private By pageTitle = By.TagName("title");
        private By isEmptyShoppingCart = By.XPath("//div[@id='content']//p[contains(text(),'Your shopping cart is empty!')]");
        private By tableWithProducts = By.XPath("//div[@class='table-responsive']//tbody");
        private By tableWithPrices = By.XPath("//div[@class='col-sm-4 col-sm-offset-8']//tbody");
        private By removeButtonInTable = By.CssSelector("span > button.btn.btn-danger");
        private By updateButtonInTable = By.CssSelector("span > button.btn.btn-primary");
        private By changeQuntityProductBoxInTable = By.CssSelector("div > input.form-control");
        private By unitPriceInTable = By.ClassName("text-right");
        private By rewardPoints = By.XPath("//*[@id='accordion']/div[2]/div[1]/h4/a");
        private By rewardPointsField = By.CssSelector("#input-reward");
        private By rewardPointsSubmitButton = By.CssSelector("#button-reward");
        private By couponCode = By.XPath("//a[contains(text(),'Use Coupon Code')]");
        private By couponCodeField = By.CssSelector("#input-coupon");
        private By couponCodeSubmitButton = By.CssSelector("#button-coupon");
        private By giftCertificate = By.XPath("//a[contains(text(),'Use Gift Certificate')]");
        private By giftCertificateField = By.CssSelector("#input-voucher");
        private By giftCertificateSubmitButton = By.CssSelector("#button-voucher");
        private By removeProductButton = By.XPath("//button[@class='btn btn-danger']");
        private By totalPrice = By.CssSelector("#content > div.row > div > table > tbody > tr:nth-child(3) > td:nth-child(2)");
        private By unitPrice = By.CssSelector("#content > form > div > table > tbody > tr > td:nth-child(5)");
        private string CheckOutXpath = "//*[@id=\"top-links\"]/ul/li[5]/a";
        private By enterTextInQuantityProductBox = By.CssSelector("#content > form > div > table > tbody > tr > td:nth-child(4) > div > input");
        private By emptyCartField = By.XPath("//*[@id=\"content\"]/p");
        private By totalPriceField = By.XPath("//*[@id=\"content\"]/div[2]/div/table/tbody/tr[2]/td[2]");
        private By updateCartField = By.CssSelector("#content > form > div > table > tbody > tr > td:nth-child(4) > div > input");


        private List<IWebElement> GetProductsTableRows()
        {
            IWebElement table = driver.FindElement(tableWithProducts);
            return table.FindElements(By.TagName("tr")).ToList();
        }

        private void ClickRemoveButton()
        {
            driver.FindElement(removeProductButton).Click();
        }

        public string GetUnitPrices(string productName)
        {
            string unitPrice = "";
            foreach (IWebElement product in GetProductsTableRows())
            {
                if (product.Text.Contains(productName))
                {
                    unitPrice = product.FindElement(unitPriceInTable).Text.TrimStart('$');
                }
            }
            return unitPrice;
        }

        public string GetTotalPrice()
        {
            return driver.FindElement(totalPrice).Text.TrimStart('$');
        }

        public int GetAmountDifferentProducts()
        {
            return GetProductsTableRows().Count();
        }

        public string GetPageTitle()
        {
            return driver.FindElement(pageTitle).GetAttribute("innerText");
        }
        public string GetUnitPrice()
       {
         return driver.FindElement(unitPrice).Text.TrimStart('$');
       }

        public string GetQuntityOneProduct(string productName)
        {
            string quantityOneProduct = "";
            foreach (IWebElement product in GetProductsTableRows())
            {
                if (product.Text.Contains(productName))
                {
                    quantityOneProduct = product.FindElement(changeQuntityProductBoxInTable).GetAttribute("value");
                }
            }
            return quantityOneProduct;
        }

        public bool IsProducInShoppingCart(string productName)
        {
            bool flag = false;
            Thread.Sleep(1000);
            foreach (IWebElement name in GetProductsTableRows())
            {
                if (name.Text.Contains(productName))
                {
                    flag = true;
                }
            }
            return flag;
        }

        public bool IsEmpty()
        {
            bool flag = false;
            if (driver.FindElement(isEmptyShoppingCart).Text.Contains("empty"))
            {
                flag = true;
            }
            return flag;
        }


        public bool IsThereField()
        {
            bool flag = true;
            try
            {
                IWebElement element = driver.FindElement(rewardPoints);
            }
            catch (NoSuchElementException)
            {
                flag = false;
            }
            return flag;
        }

        public ShoppingCartPage ClickRemoveProduct(string productName)
        {
            foreach (IWebElement product in GetProductsTableRows())
            {
                if (product.Text.Contains(productName))
                {
                    product.FindElement(removeButtonInTable).Click();
                }
            }
            return new ShoppingCartPage(driver);
        }

        public ShoppingCartPage ClickUpdateProduct(string productName)
        {
            foreach (IWebElement product in GetProductsTableRows())
            {
                if (product.Text.Contains(productName))
                {
                    product.FindElement(updateButtonInTable).Click();
                }
            }
            return new ShoppingCartPage(driver);
        }

        public ShoppingCartPage InputQuantityProduct(string productName, string quntityProducts)
        {
            foreach (IWebElement product in GetProductsTableRows())
            {
                if (product.Text.Contains(productName))
                {
                   product.FindElement(changeQuntityProductBoxInTable).Clear();
                   product.FindElement(changeQuntityProductBoxInTable).SendKeys(quntityProducts);
                }
            }
            return new ShoppingCartPage(driver);
        }

        public ShoppingCartPage EnterTextInQuantityProductBox(string quantityProductUnits)
        {
            IWebElement checkBox = driver.FindElement(enterTextInQuantityProductBox);
            checkBox.Clear();
            checkBox.SendKeys(quantityProductUnits + Keys.Enter);
            return new ShoppingCartPage(driver);
        }     
       
        public ShoppingCartPage ApplyCouponCode(string couponСodeValue)
        {
            driver.FindElement(couponCode).Click();
            driver.FindElement(couponCodeField).SendKeys(couponСodeValue);
            driver.FindElement(couponCodeSubmitButton).Click();
            return new ShoppingCartPage(driver);
        }

        public ShoppingCartPage ApplyRewardPoints(string amountRewardPoints)
        {
            driver.FindElement(rewardPoints).Click();
            driver.FindElement(rewardPointsField).SendKeys(amountRewardPoints);
            driver.FindElement(rewardPointsSubmitButton).Click();
            return new ShoppingCartPage(driver);
        }
        public ShoppingCartPage ApplyGifCertificate(string giftCertificateValue)
        {
            driver.FindElement(giftCertificate).Click();
            driver.FindElement(giftCertificateField).SendKeys(giftCertificateValue);
            driver.FindElement(giftCertificateSubmitButton).Click();
            return new ShoppingCartPage(driver);
        }

        public ShoppingCartPage ClearShoppingCart()
        {
            int steps = GetAmountDifferentProducts();
            while (steps > 0)
            {
                Thread.Sleep(1000);
                ClickRemoveButton();
                steps--;
            }
            return new ShoppingCartPage(driver);
        }

        public string GetEmptyCart()
        {
            IWebElement field = driver.FindElement(emptyCartField);
            string content = field.Text;
            return content;
        }
        public string GetFullPrice()
        {
            IWebElement field = driver.FindElement(totalPriceField);
            string fullprice = field.Text;
            return fullprice;
        }

        public new CheckOutPage GoToCheckout()
        {
            driver.FindElement(By.XPath(CheckOutXpath)).Click();

            return new CheckOutPage(driver);
        }
        public ShoppingCartPage UpdateCart(string quantity)
        {
            driver.FindElement(updateCartField).SendKeys(Keys.Backspace + quantity + Keys.Enter);
            return new ShoppingCartPage(driver);
        }

        public ShoppingCartPage CheckIsProductInShoppingCart(bool expectedIsProductInShoppingCart, string productName)
        {
            Assert.AreEqual(expectedIsProductInShoppingCart, IsProducInShoppingCart(productName));
            return this;
        }

        public ShoppingCartPage CheckTotalPrice(string expectedTotalPrice)
        {
            Assert.AreEqual(expectedTotalPrice, GetTotalPrice());
            return this;
        }

        public ShoppingCartPage CheckUnitlPrice(string expectedUnitPrice, string productName)
        {
            Assert.AreEqual(expectedUnitPrice, GetUnitPrices(productName));
            return this;
        }

        public ShoppingCartPage CheckIsThereFieldRewardPoints(bool expectedFieldNotExist)
        {
            Assert.AreEqual(expectedFieldNotExist, IsThereField());
            return this;
        }

        public ShoppingCartPage CheckGetQuntityOneProduct(string expectedQuntityOneProduct, string productName)
        {
            Assert.AreEqual(expectedQuntityOneProduct, GetQuntityOneProduct(productName));
            return this;
        }
        public ShoppingCartPage CheckGetQuntityProductTableRows(int expectedQuntityTableRows)
        {
            Assert.AreEqual(expectedQuntityTableRows, GetAmountDifferentProducts());
            return this;
        }

        public ShoppingCartPage CheckFullPrice(string expectedPrice)
        {
            Assert.AreEqual(expectedPrice, GetFullPrice());
            return this;
        }
        public ShoppingCartPage CheckEmptyCart()
        {
            Assert.AreEqual("Your shopping cart is empty!", GetEmptyCart());
            return this;
        }


    }
}

