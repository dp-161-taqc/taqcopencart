using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class AccountPage : CommonPageWithAccountMenu
    {
        private By accountPageTitle = By.TagName("title");
        private By modifyWishList = By.XPath("//a[contains(text(),'Modify your wish list')]");


        public AccountPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToAccountPage));
        }

        public WishListPage ClickModifyWishList()
        {
            driver.FindElement(modifyWishList).Click();
            return new WishListPage(driver);
        }

        public void CheckAccountPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl + string.Format(URL.PathToAccountPage);
            string expectedTitle = GetPageTitle(accountPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }

        public AccountPage CheckClickOnWishListPageContinueButton(string expected)
        {
            Assert.AreEqual(expected, GetPageTitle(accountPageTitle));
            return this;
        }
    }
}
