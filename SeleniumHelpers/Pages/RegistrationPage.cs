﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class RegistrationPage : CommonPageWithMenu
    {
        private By inputFirstname = By.Id("input-firstname");
        private By inputLastname = By.Id("input-lastname");
        private By inputEmail = By.Id("input-email");
        private By inputTelephone = By.Id("input-telephone");
        private By inputFax = By.Id("input-fax");
        private By inputCompany = By.Id("input-company");
        private By inputAddress1 = By.Id("input-address-1");
        private By inputAddress2 = By.Id("input-address-2");
        private By inputCity = By.Id("input-city");
        private By inputPostcode = By.Id("input-postcode");
        private By inputCountry = By.Id("input-country");
        private By inputRegion = By.Id("input-zone");
        private By inputPassword = By.Id("input-password");
        private By inputPasswordConfirm = By.Id("input-confirm");
        private By inputCheckBoxConfirm = By.Name("agree");
        private By registerPageTitle = By.TagName("title");
        private By loginPageButton = By.XPath("//*[@class='row']//*[text()='login page']");
        private By continueButton = By.XPath("//input[@value='Continue']");
        private By firstNameWarningMessage = By.XPath(@"//input[@id ='input-firstname']/following::div");
        private By WarningMessage = By.XPath("//div[@class='text-danger']");
        private By privatePolicyWarningMessage = By.XPath("//div[@class='alert alert-danger']");

        public RegistrationPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToRegisterPage));
        }

        public LoginPage ClickLoginPageButton()
        {
            driver.FindElement(loginPageButton).Click();
            return new LoginPage(driver);
        }

        public RegistrationPage InputFirstname(string input)
        {
            driver.FindElement(inputFirstname).SendKeys(input);
            return this;
        }

        public RegistrationPage InputLastname(string input)
        {
            driver.FindElement(inputLastname).SendKeys(input);
            return this;
        }

        public RegistrationPage InputEmail(string input)
        {
            driver.FindElement(inputEmail).SendKeys(input);
            return this;
        }

        public RegistrationPage InputTelephone(string input)
        {
            driver.FindElement(inputTelephone).SendKeys(input);
            return this;
        }

        public RegistrationPage InputFax(string input)
        {
            driver.FindElement(inputFax).SendKeys(input);
            return this;
        }

        public RegistrationPage InputCompany(string input)
        {
            driver.FindElement(inputCompany).SendKeys(input);
            return this;
        }

        public RegistrationPage InputAddress1(string input)
        {
            driver.FindElement(inputAddress1).SendKeys(input);
            return this;
        }

        public RegistrationPage InputAddress2(string input)
        {
            driver.FindElement(inputAddress2).SendKeys(input);
            return this;
        }

        public RegistrationPage InputCity(string input)
        {
            driver.FindElement(inputCity).SendKeys(input);
            return this;
        }

        public RegistrationPage InputPostcode(string input)
        {
            driver.FindElement(inputPostcode).SendKeys(input);
            return this;
        }

        public RegistrationPage InputCountry(string input)
        {
            SelectElement element = new SelectElement(driver.FindElement(inputCountry));
            element.SelectByText(input);
            return this;
        }

        public RegistrationPage InputRegion(string input)
        {
            SelectElement element = new SelectElement(driver.FindElement(inputRegion));
            element.SelectByText(input);
            return this;
        }

        public RegistrationPage InputPassword(string input)
        {
            driver.FindElement(inputPassword).SendKeys(input);
            return this;
        }

        public RegistrationPage InputPasswordConfirm(string input)
        {
            driver.FindElement(inputPasswordConfirm).SendKeys(input);
            return this;
        }

        public RegistrationPage ClickCheckBoxConfirm()
        {
            driver.FindElement(inputCheckBoxConfirm).Click();
            return this;
        }

        public RegistrationPage ClickContinueButtonIncorrectInput()
        {
            driver.FindElement(continueButton).Click();
            return this;
        }

        public AccountSucessPage ClickContinueButton(UserModel user)
        {
            InputFirstname(user.Firstname)
            .InputLastname(user.Lastname)
            .InputEmail(user.Email)
            .InputTelephone(user.Telephone)
            .InputFax(user.Fax)
            .InputCompany(user.Company)
            .InputAddress1(user.Address1)
            .InputAddress2(user.Address2)
            .InputCity(user.City)
            .InputPostcode(user.Postcode)
            .InputCountry(user.Country)
            .InputRegion(user.Region)
            .InputPassword(user.Password)
            .InputPasswordConfirm(user.Password)
            .ClickCheckBoxConfirm()
            .driver.FindElement(continueButton).Click();
            return new AccountSucessPage(driver);
        }

        public string GetWarningMessage(By element)
        {
            return driver.FindElement(element).GetAttribute("innerText");
        }

        public void CheckRegisterPageFirstNameIncorrectInput()
        {
            string expectedMessage = "First Name must be between 1 and 32 characters!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageLastNameIncorrectInput()
        {
            string expectedMessage = "Last Name must be between 1 and 32 characters!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageEmailIncorrectInput()
        {
            string expectedMessage = "E-Mail Address does not appear to be valid!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageTelephoneIncorrectInput()
        {
            string expectedMessage = "Telephone must be between 3 and 32 characters!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageAddress1IncorrectInput()
        {
            string expectedMessage = "Address 1 must be between 3 and 128 characters!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageCityIncorrectInput()
        {
            string expectedMessage = "City must be between 2 and 128 characters!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageCountryIncorrectInput()
        {
            string expectedMessage = "Please select a country!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPageRegionIncorrectInput()
        {
            string expectedMessage = "Please select a region / state!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPagePasswordIncorrectInput()
        {
            string expectedMessage = "Password must be between 4 and 20 characters!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPagePasswordConfirmIncorrectInput()
        {
            string expectedMessage = "Password confirmation does not match password!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(WarningMessage));
        }

        public void CheckRegisterPagePrivatePolicyIncorrectInput()
        {
            string expectedMessage = " Warning: You must agree to the Privacy Policy!"; ;
            StringAssert.Contains(expectedMessage, GetWarningMessage(privatePolicyWarningMessage));
        }

        public void CheckRegisterPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl + string.Format(URL.PathToRegisterPage);
            string expectedTitle = GetPageTitle(registerPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }
    }
}
