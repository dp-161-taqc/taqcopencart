﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class LoginPage : CommonPageWithMenu
    {
        private By inputEmail = By.Id("input-email");
        private By inputPassword = By.Id("input-password");
        private By loginPageTitle = By.TagName("title");
        private By loginButton = By.XPath("//input[@value='Login']");
        private By errorMessage = By.XPath("//div[@class='alert alert-danger']");
        private By continueButton = By.XPath("//*[@class='well']//*[text()='Continue']");
        private By forgetPasswordLinkButton = By.XPath("//*[@class='form-group']//*[text()='Forgotten Password']");

        public LoginPage(IWebDriver driver) : base(driver)
        {
            NavigatePage(ConfigurationHelper.BaseUrl + string.Format(URL.PathToLoginPage));
        }

        public RegistrationPage ClickContinueButton()
        {
            driver.FindElement(continueButton).Click();
            return new RegistrationPage(driver);
        }

        public ForgottenPasswordPage ClickForgottenPasswordLinkButton()
        {
            driver.FindElement(forgetPasswordLinkButton).Click();
            return new ForgottenPasswordPage(driver);
        }

        public LoginPage InputEmail(string emailInput)
        {
            driver.FindElement(inputEmail).SendKeys(emailInput);
            return this;
        }

        public LoginPage InputPassword(string passwordInput)
        {
            driver.FindElement(inputPassword).SendKeys(passwordInput);
            return this;
        }

        public LoginPage ClickLoginButtonCenterMenuWithIncorrectInput()
        {
            driver.FindElement(loginButton).Click();
            return this;
        }

        public AccountPage ClickLoginButtonCenterMenu(UserModel user)
        {
            InputEmail(user.Email);
            InputPassword(user.Password);
            driver.FindElement(loginButton).Click();
            return new AccountPage(driver);
        }

        public string GetWarningMessage(By element)
        {
            return driver.FindElement(element).GetAttribute("innerText");
        }

        public void CheckLoginPageIncorrectInput()
        {
            string expectedMessage = " Warning: No match for E-Mail Address and/or Password.";
            StringAssert.Contains(expectedMessage, GetWarningMessage(errorMessage));
        }

        public void CheckLoginPage()
        {
            string expectedURL = ConfigurationHelper.BaseUrl + string.Format(URL.PathToLoginPage);
            string expectedTitle = GetPageTitle(loginPageTitle);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(expectedURL, driver.Url);
                Assert.AreEqual(expectedTitle, driver.Title);
            });
        }
    }
}
