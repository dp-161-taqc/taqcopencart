﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Models;
using NUnit.Framework;

namespace SeleniumHelpers.Pages.CommonPages
{
    public class TermsAndConditionPage : CommonPage
    {
        public TermsAndConditionPage(IWebDriver driver) : base(driver)
        {
        }

     

        private By termsAndConditionMassage = By.CssSelector("#modal-agree > div > div > div.modal-header > h4");
        private By termsAndConditionMassageOnPage = By.XPath(" //*[@id=\"content\"]/h1");

        public string GetTermsAndCondition()
        {
            Thread.Sleep(2000);
            IWebElement field = driver.FindElement(termsAndConditionMassage);
            string content = field.Text;
            return content;
        }
        public string GetTermsAndConditionPage()
        {
            Thread.Sleep(2000);
            IWebElement field = driver.FindElement(termsAndConditionMassageOnPage);
            string content = field.Text;
            return content;
        }
        public TermsAndConditionPage CheckTermsandConditionPage()
        {
            Assert.AreEqual("Terms & Conditions", GetTermsAndConditionPage());
            return this;
        }
        public TermsAndConditionPage CheckTermsandCondition()
        {
            Assert.AreEqual("Terms & Conditions", GetTermsAndCondition());
            return this;
        }
    }

}