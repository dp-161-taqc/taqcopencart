﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Pages
{
    public class HomePage : CommonPageWithMenu
    {
        public HomePage(IWebDriver driver) : base(driver)
        {
        }

        private By slideShow = By.CssSelector("#slideshow0");
        private By productList = By.CssSelector("#content > div.row");
        private By addToWishListButton = By.CssSelector(".button-group>button[data-original-title='Add to Wish List']");
        private By addToShoppingCart = By.CssSelector(".button-group>button>span");

        public void CheckHomePageIsOpened()
        {
            IWebElement expectedElement = driver.FindElement(slideShow, elementWaitSeconds);                     
            Assert.NotNull(expectedElement);
        }

        private List<IWebElement> GetProductList()
        {
            IWebElement productsList = driver.FindElement(productList);
            return productsList.FindElements(By.TagName("div")).ToList();
        }
        public HomePage ClickAddToWishList(string productName)
        {
            foreach (IWebElement product in GetProductList())
            {
                if (product.Text.Contains(productName))
                {
                    product.FindElement(addToWishListButton).Click();
                    break;
                }
            }
            return new HomePage(driver);
        }
        public HomePage ClickAddToShoppingCart(string productName)
        {
            foreach (IWebElement product in GetProductList())
            {
                if (product.Text.Contains(productName))
                {
                    product.FindElement(addToShoppingCart).Click();
                    break;
                }
            }
            return new HomePage(driver);
        }
    }
}
