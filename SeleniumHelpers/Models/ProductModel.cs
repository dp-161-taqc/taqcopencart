﻿using System.Collections.Generic;

namespace SeleniumHelpers.Models
{
    public class ProductModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string OldPrice { get; set; }
        public string FullDescriptionTab { get; set; }
        public string FullDescription { get; set; }
        public string SmallProductImageSrc { get; set; }
        public string LargeProductImageSrc { get; set; }
        public string Brand { get; set; }
        public string ProductCode { get; set; }
        public string Availability { get; set; }
        public List<ProductModel> RelatedProducts { get; set; }
        public List<ProductModel> TagProducts { get; set; }
        public string Tag { get; set; }
        public string SearchTagUrl { get; set; }
    }
}
