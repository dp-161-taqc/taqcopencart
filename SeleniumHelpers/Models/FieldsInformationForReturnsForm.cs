﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumHelpers.Models
{
    public class FieldsInformationForReturnsForm
    {      
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string OderId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Quantity { get; set; }
        public string ReasonForReturn { get; set; }
        public string ProductIsOpened { get; set; }       
    }
}
