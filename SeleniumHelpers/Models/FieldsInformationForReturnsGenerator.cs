﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumHelpers.Models
{
    public class FieldsInformationForReturnsGenerator
    {
        public static FieldsInformationForReturnsForm EnterCorrectDataInRequiredFields()
        {
            return new FieldsInformationForReturnsForm()
            {
                FirstName = "Anna",
                LastName = "Khodak",
                Email = "anna@qqq.com",
                Telephone = "123456",
                OderId = "123",
                ProductName = "DVD",
                ProductCode = "codeDVD",
                ReasonForReturn = "Order Error",
            }; 
        }      
    }
}
