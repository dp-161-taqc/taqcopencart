using SeleniumHelpers.Data;

namespace SeleniumHelpers.Models
{
    public class UserRegister
    {
        public static UserModel GetUser(string userLastName)
        {
            switch (userLastName)
            {
                case "TestLast": return Users.User1();
                case "TestLast2": return Users.User2();
                case "TestLast3": return Users.User3();
                case "TestLast4": return Users.User4();
                case "CustomerOne": return Users.User5();
                case "CustomerTwo": return Users.User6();               
            }
            return null;
        }       
    }
}
