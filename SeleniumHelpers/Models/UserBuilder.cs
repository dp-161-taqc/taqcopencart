﻿
namespace SeleniumHelpers.Models
{
    public class UserBuilder
    {
        public string firstName;
        public string lastName;
        public string email;
        public string telephone;
        public string fax;
        public string company;
        public string address1;
        public string address2;
        public string city;
        public string postcode;
        public string country;
        public string region;
        public string password;


        public UserBuilder FirstName(string firstName)
        {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder LastName(string lastName)
        {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder Email(string email)
        {
            this.email = email;
            return this;
        }

        public UserBuilder Telephone(string telephone)
        {
            this.telephone = telephone;
            return this;
        }
    
        public UserBuilder Fax(string fax)
        {
            this.fax = fax;
            return this;
        }

        public UserBuilder Company(string company)
        {
            this.company = company;
            return this;
        }

        public UserBuilder Address1(string address1)
        {
            this.address1 = address1;
            return this;
        }

        public UserBuilder Address2(string address2)
        {
            this.address2 = address2;
            return this;
        }

        public UserBuilder City(string city)
        {
            this.city = city;
            return this;
        }

        public UserBuilder Postcode(string postcode)
        {
            this.postcode = postcode;
            return this;
        }

        public UserBuilder Country(string country)
        {
            this.country = country;
            return this;
        }

        public UserBuilder Region(string region)
        {
            this.region = region;
            return this;
        }

        public UserBuilder Password(string password)
        {
            this.password = password;
            return this;
        }

        public UserModel Build()
        {
            return new UserModel(this);
        }
    }
}
