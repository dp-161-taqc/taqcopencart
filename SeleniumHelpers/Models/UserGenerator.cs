﻿using System;

namespace SeleniumHelpers.Models
{
    public class UserGenerator
    {
        public static UserModel GenerateUser()
        {
            int randomNumber = new Random().Next(100000);

            return new UserBuilder()
                .FirstName("Test" + randomNumber)
                .LastName("TestLast" + randomNumber)
                .Email("testlast" + randomNumber + "@gmail.com")
                .Telephone("+380970" + randomNumber)
                .Fax("+380970" + randomNumber)
                .Company("SoftServe")
                .Address1("Liberty ave, " + randomNumber)
                .Address2("Liberty ave, " + randomNumber)
                .City("Chicago")
                .Postcode(randomNumber.ToString())
                .Country("United States")
                .Region("Illinois")
                .Password("qwerty")
                .Build();
        }
    }
}
