﻿using SeleniumHelpers.Utils;
using System.Collections.Generic;

namespace SeleniumHelpers.Models
{
    public class ProductGenerator
    {
        public static ProductModel CreateCaseForIphone()
        {
            return new ProductModel()
            {
                Id = 65,
                Price = "$5.00",
                OldPrice = "$5.47",
                Name = "Case for iPhone 7/8",
                FullDescriptionTab = "Description",
                FullDescription = "Smart case Smart case Avatti B & Z PC does not allow the appearance of scratches and scuffs on the body of the device, is resistant to the effects of fats, oils, ozone. The case has a design designed for use with the above model, tightly fixed on the device and securely held on it. The accessory design also takes into account all the slots for the speakers, connectors and ports, and the side keys of the device. Light and thin cover-case does not increase the size of the device and emphasizes its thin style. The accessory is pleasant tactile and visually, causes admiration for bright colors and a well-thought-out design.",
                SmallProductImageSrc = ConfigurationHelper.BaseUrl + "image/cache/catalog/iphone_7_pc_case__lousi_bidon_black-228x228.jpg",
                LargeProductImageSrc = ConfigurationHelper.BaseUrl + "image/cache/catalog/iphone_7_pc_case__lousi_bidon_black-500x500.jpg",
                Brand = "Apple",
                Availability = "Availability: In Stock",
                ProductCode = "Product Code: case for iPhone 7/8",
                RelatedProducts = new List<ProductModel>() { CreateAppleWatch() },
                TagProducts = new List<ProductModel>() { CreateSmartCoverIpad() },
                Tag = "caseForiPhone",
                SearchTagUrl = "caseForiPhone"
            };
        }

        public static ProductModel CreateAppleWatch()
        {
            return new ProductModel()
            {
                Id = 66,
                Name = "Apple Watch Magnetic Charging Dock"
            };
        }

        public static ProductModel CreateSmartCoverIpad()
        {
            return new ProductModel()
            {
                Id = 67,
                Name = "Apple Smart Cover iPad NEW 2017 RED"
            };
        }
    }
}