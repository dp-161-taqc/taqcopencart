﻿namespace SeleniumHelpers.Models
{
    public class UserModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Password { get; set; }

        public UserModel(UserBuilder userBuilder)
        {
            Firstname = userBuilder.firstName;
            Lastname = userBuilder.lastName;
            Email = userBuilder.email;
            Telephone = userBuilder.telephone;
            Fax = userBuilder.fax;
            Company = userBuilder.company;
            Address1 = userBuilder.address1;
            Address2 = userBuilder.address2;
            City = userBuilder.city;
            Postcode = userBuilder.postcode;
            Country = userBuilder.country;
            Region = userBuilder.region;
            Password = userBuilder.password;
        }        
    }
}
