﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHelpers.Pages;
using System;

namespace SeleniumHelpers.Utils
{
    public class CommonActions : BasePage
    {
        public CommonActions(IWebDriver driver) : base(driver)
        {
        }

        public void OpenURL(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public void WaitImplicitly(int seconds)
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }

        public void WaitExplicitlyWithCondition(IWebDriver driver, string condition)
        {
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(d => d + "." + condition);
        }

        public string GetLastChars(int numberOfChars, string inputString)
        {
            return inputString.Substring(inputString.Length - numberOfChars);
        }
    }
}
