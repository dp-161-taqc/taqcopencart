using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.XPath;

namespace SeleniumHelpers.Utils
{
    public class XmlParser
    {
        public static XmlParser Parser { get; set; } = new XmlParser();

        public List<string> ParseXml(string fileName, string tag, params string[] innerTags)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            StreamReader textStreamReader = new StreamReader
                (assembly.GetManifestResourceStream("SeleniumHelpers.Data.Xml." + fileName));
            XmlDocument doc = new XmlDocument();
            doc.Load(textStreamReader);

            List<string> result = new List<string>();
            for (int i = 0; i < innerTags.Length; i++)
            {
                try
                {
                    if (innerTags[i].Contains("*") && innerTags.Length == 1)
                    {
                        XmlNodeList nodeList = doc.SelectNodes("//" + tag + "/" + innerTags[i]);
                        foreach (XmlNode item in nodeList)
                        {
                            result.Add(item.InnerText);
                        }
                    }
                    else
                    {
                        XmlNode node = doc.SelectSingleNode("//" + tag + "/" + innerTags[i]);
                        result.Add(node.InnerText);
                        // Console.WriteLine($"XML value of index {i} was:   " + node.InnerText);
                    }
                }
                catch(NullReferenceException)
                {
                    Console.WriteLine($"XML value of index {i} was:   NULL");
                }
                catch (XPathException)
                {
                    Console.WriteLine($"XML value of index {i} has:   INCORRECT FORMAT");
                }
            }
            if (result.Count < 1)
            {
                Console.WriteLine("Unfortunately not one item was found..........  :(");
                return result;
            }
            return result;
        }
    }
}