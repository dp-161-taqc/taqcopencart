﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace SeleniumHelpers.Utils
{
    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, double timeoutInSeconds)
        {
            var wait = new WebDriverWait(new SystemClock(), driver, TimeSpan.FromSeconds(timeoutInSeconds), TimeSpan.FromSeconds(1));
            var findElement = wait.Until(drv => drv.FindElement(by));

            return findElement;
        }

        public static ReadOnlyCollection<IWebElement> FindElements(this IWebDriver driver, By by, double timeoutInSeconds)
        {
            var wait = new WebDriverWait(new SystemClock(), driver, TimeSpan.FromSeconds(timeoutInSeconds), TimeSpan.FromSeconds(1));
            var webElements = wait.Until(drv => drv.FindElements(by));

            return webElements;
        }

        public static IWebDriver StartWebDriver()
        {
            string browser = ConfigurationHelper.GetBrowser();
            IWebDriver driver = null;

            switch (browser)
            {
                case "Chrome":
                    driver = StartChromeDriver();
                    break;
                case "IE":
                    driver = StartIEDriver();
                    break;
                default:
                    driver = StartChromeDriver();
                    break;
            }
            if (driver != null)
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
                driver.Manage().Window.Maximize();
            }

            return driver;
        }

        private static IWebDriver StartChromeDriver()
        {
            var driver = new ChromeDriver();
            return driver;
        }

        private static IWebDriver StartIEDriver()
        {
            InternetExplorerOptions ieOptions = new InternetExplorerOptions
            {
                IgnoreZoomLevel = true,
                PageLoadStrategy = PageLoadStrategy.Normal,
                EnableNativeEvents = true,
                EnablePersistentHover = true,
                IntroduceInstabilityByIgnoringProtectedModeSettings = true
            };
            //ieOptions.EnsureCleanSession = true;
            InternetExplorerDriver driver = new InternetExplorerDriver(ieOptions)
            {
                Url = ConfigurationHelper.BaseUrl
            };
            driver.ExecuteScript("javascript:document.getElementById('overridelink').click()");
            return driver;
        }
    }
}
