﻿using System.Configuration;

namespace SeleniumHelpers.Utils
{
    public class ConfigurationHelper
    {
        public static readonly string BaseUrl = SetBaseUrl();

        public static string GetBrowser()
        {
            string value = GetConfigParameter("DriverToUse");
            if (string.IsNullOrEmpty(value))
            {
                return "Chrome";
            }
            return value;
        }

        static string SetBaseUrl()
        {
            string value = GetConfigParameter("BaseURL");
            if (string.IsNullOrEmpty(value))
            {
                return "http://newstore25062019.is-best.net/";
            }

            return value;
        }

        public static string GetConfigParameter(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }
    }
}
