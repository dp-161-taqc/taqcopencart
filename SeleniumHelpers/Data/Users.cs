﻿using SeleniumHelpers.Models;
using SeleniumHelpers.Utils;
using System.Collections.Generic;
using System.Linq;

namespace SeleniumHelpers.Data
{
    public class Users
    {
        public static UserModel User1()
        {
            return GetUserFromXml("Users.xml", "user1");
        }

        public static UserModel User2()
        {
            return GetUserFromXml("Users.xml", "user2");
        }

        public static UserModel User3()
        {
            return GetUserFromXml("Users.xml", "user3");
        }

        public static UserModel User4()
        {
            return GetUserFromXml("Users.xml", "user4");
        }

        public static UserModel User5()
        {
            return GetUserFromXml("Users.xml", "user5");
        }

        public static UserModel User6()
        {
            return GetUserFromXml("Users.xml", "user6");
        }

        public static UserModel UserForReturnsForm()
        {
            return GetUserFromXml("Users.xml", "UserForReturnsForm");
        }

        public static UserModel UserForProductAndContactUsForm()
        {
            return GetUserFromXml("Users.xml", "UserForProductAndContactUsForm");           
        }

        private static UserModel GetUserFromXml(string file, string user)
        {
            List<string> list = XmlParser.Parser.ParseXml(file, user, "*");

            return new UserBuilder()
                .FirstName(list.ElementAt(0))
                .LastName(list.ElementAt(1))
                .Email(list.ElementAt(2))
                .Telephone(list.ElementAt(3))
                .Fax(list.ElementAt(4))
                .Company(list.ElementAt(5))
                .Address1(list.ElementAt(6))
                .Address2(list.ElementAt(7))
                .City(list.ElementAt(8))
                .Postcode(list.ElementAt(9))
                .Country(list.ElementAt(10))
                .Region(list.ElementAt(11))
                .Password(list.ElementAt(12))
                .Build();
        }       
    }
}
