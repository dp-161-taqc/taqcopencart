using SeleniumHelpers.Utils;

namespace SeleniumHelpers.Data
{
    public class URL
    {
        public static readonly string PathToLoginPage = GetPathFromUrlXmlFile("loginPage");
        public static readonly string PathToAccountPage = GetPathFromUrlXmlFile("accountPage");
        public static readonly string PathToAccountSucessPage = GetPathFromUrlXmlFile("accountSucessPage");
        public static readonly string PathToAccountLogoutPage = GetPathFromUrlXmlFile("accountLogoutPage");
        public static readonly string PathToRegisterPage = GetPathFromUrlXmlFile("registerPage");
        public static readonly string PathToForgetPasswordPage = GetPathFromUrlXmlFile("forgetPasswordPage");
        public static readonly string PathToPasswordEditPage = GetPathFromUrlXmlFile("passwordEditPage");
        public static readonly string PathToReturnsForm = GetPathFromUrlXmlFile("returnFormPage");
        public static readonly string PathToReturnsSuccessedPage = GetPathFromUrlXmlFile("returnSucessPage");
        public static readonly string PathToHomePage = GetPathFromUrlXmlFile("homePage");
        public static readonly string PathToSearchPage = GetPathFromUrlXmlFile("searchPage");
        public static readonly string PathToProductPage = GetPathFromUrlXmlFile("productPage");
        public static readonly string PathToContactUsPage = GetPathFromUrlXmlFile("contactUsPage");
        
        private static string GetPathFromUrlXmlFile(string page)
        {
            return XmlParser.Parser.ParseXml("URL.xml", "paths", page)[0];
        }
    }

    public class CopyOfURL
    {
        public static readonly string PathToLoginPage = GetPathFromUrlXmlFile("loginPage");
        public static readonly string PathToAccountPage = GetPathFromUrlXmlFile("accountPage");
        public static readonly string PathToAccountSucessPage = GetPathFromUrlXmlFile("accountSucessPage");
        public static readonly string PathToAccountLogoutPage = GetPathFromUrlXmlFile("accountLogoutPage");
        public static readonly string PathToRegisterPage = GetPathFromUrlXmlFile("registerPage");
        public static readonly string PathToForgetPasswordPage = GetPathFromUrlXmlFile("forgetPasswordPage");
        public static readonly string PathToPasswordEditPage = GetPathFromUrlXmlFile("passwordEditPage");
        public static readonly string PathToReturnsForm = GetPathFromUrlXmlFile("returnFormPage");
        public static readonly string PathToReturnsSuccessedPage = GetPathFromUrlXmlFile("returnSucessPage");
        public static readonly string PathToHomePage = GetPathFromUrlXmlFile("homePage");
        public static readonly string PathToSearchPage = GetPathFromUrlXmlFile("searchPage");

        private static string GetPathFromUrlXmlFile(string page)
        {
            return XmlParser.Parser.ParseXml("CopyOfURL.xml", "paths", page)[0];
        }
    }
}
