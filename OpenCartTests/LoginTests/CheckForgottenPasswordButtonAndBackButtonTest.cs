﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckForgottenPasswordButtonAndBackButtonTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckForgottenPasswordButton()
        {
            commonPageWithMenu
                .ClickForgottenPasswordButton()
                .ClickBackButton()
                .CheckLoginPage();
        }
    }
}
