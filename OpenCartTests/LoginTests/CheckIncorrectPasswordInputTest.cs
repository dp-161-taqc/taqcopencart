﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckIncorrectPasswordInputTest : StartPageSetupBaseTest
    {
        [TestCase("TestLast", "fps5kf4k")]
        [TestCase("TestLast2", "  ")]
        [TestCase("TestLast4", "")]
        public void CheckIncorrectPasswordInput(string userLastName, string password)
        {
            commonPageWithMenu
           .ClickLoginButton()
           .InputEmail(userLastName)
           .InputPassword(password)
           .ClickLoginButtonCenterMenuWithIncorrectInput()
           .CheckLoginPageIncorrectInput();
        }
    }
}
