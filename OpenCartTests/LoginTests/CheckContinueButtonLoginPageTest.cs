﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckContinueButtonLoginPageTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckContinueButtonFromLoginPageToRegisterPage()
        {
            commonPageWithMenu
                .ClickLoginButton()
                .ClickContinueButton()
                .CheckRegisterPage();
        }
    }
}
