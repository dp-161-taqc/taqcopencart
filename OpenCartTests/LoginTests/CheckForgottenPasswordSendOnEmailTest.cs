﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckForgottenPasswordSendOnEmailTest : StartPageSetupBaseTest
    {
        [TestCase("TestLast2")]
        public void CheckForgottenPasswordSendOnEmail(string userLastName)
        {
            commonPageWithMenu
                .ClickForgottenPasswordButton()
                .ClickContinueButton(
                UserRegister.GetUser(userLastName).Email)
                .CheckLoginPage();
        }
    }
}
