﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckConfirmUserLoginTest : StartPageSetupBaseTest
    {
        [TestCase("TestLast3")]
        public void CheckLoginConfirm(string userLastName)
        {
            commonPageWithMenu
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                .CheckAccountPage();
        }
    }
}
