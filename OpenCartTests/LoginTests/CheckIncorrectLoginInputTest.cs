﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    class CheckIncorrectLoginInputTest : StartPageSetupBaseTest
    {
        [TestCase("Test", "qwertry")]
        [TestCase("", "qwertry")]
        [TestCase("   ", "12345")]
        public void CheckIncorrectLoginInput(string userLastName, string password)
        {
            commonPageWithMenu
           .ClickLoginButton()
           .InputEmail(userLastName)
           .InputPassword(password)
           .ClickLoginButtonCenterMenuWithIncorrectInput()
           .CheckLoginPageIncorrectInput();
        }
    }
}
