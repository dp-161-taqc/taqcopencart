﻿using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class StartPageSetupBaseTest : BaseTest
    {
        protected CommonPageWithMenu commonPageWithMenu;

        [SetUp]
        public void StartUrlSetup()
        {
            commonPageWithMenu = new CommonPageWithMenu(driver);
            driver.Navigate().GoToUrl(BaseUrl);
        }
    }
}
