﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckIncorrectLoginAndPasswordInputTest : StartPageSetupBaseTest
    {
        [TestCase("Test", "")]
        [TestCase("", "$$")]
        [TestCase("   ", " ")]
        public void CheckIncorrectLoginAndPasswordInput(string userLastName, string password)
        {
            commonPageWithMenu
           .ClickLoginButton()
           .InputEmail(userLastName)
           .InputPassword(password)
           .ClickLoginButtonCenterMenuWithIncorrectInput()
           .CheckLoginPageIncorrectInput();
        }
    }
}
