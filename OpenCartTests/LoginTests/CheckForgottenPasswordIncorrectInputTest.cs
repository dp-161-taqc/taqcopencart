﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckForgottenPasswordIncorrectInputTest : StartPageSetupBaseTest
    {
        [TestCase("")]
        [TestCase("Abc..123@example.com")]
        [TestCase("email.@example.com")]
        public void CheckForgottenPasswordIncorrectInput(string email)
        {
            commonPageWithMenu
                .ClickForgottenPasswordButton()
                .InputEmail(email)
                .ClickContinueButtonIncorrectInput()
                .CheckForgottenPasswordPageIncorrectInput();
        }
    }
}
