﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckMyAccountButtonBasePageTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckButtonMyAccountOnBasePage()
        {
            commonPageWithMenu
                .ClickMyAccountButton()
                .CheckLoginPage();
        }
    }
}
