﻿using NUnit.Framework;

namespace OpenCartTests.LoginTests
{
    [TestFixture]
    public class CheckAddressBookButtonBasePageTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckButtonAddressBookOnBasePage()
        {
            commonPageWithMenu
                .ClickAddressBookButton()
                .CheckLoginPage();
        }
    }
}
