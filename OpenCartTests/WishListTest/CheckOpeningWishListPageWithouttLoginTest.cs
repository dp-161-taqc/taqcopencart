﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckOpeningWishListPageWithoutLoginTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("Account Login")]
        public void CheckOpeningWishListPageWithoutLogin(string expectedPage)
        {
            commonPage
                    .GetWishListPage()
                    .CheckOpenWishListPage(expectedPage);
        }
    }
}
