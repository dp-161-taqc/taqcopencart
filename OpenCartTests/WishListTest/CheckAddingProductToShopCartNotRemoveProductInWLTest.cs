﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckAddingProductToShopCartNotRemoveProductInWLTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 8 (Product) Red", true, "CustomerOne")]
        public void CheckAddingProductToShopCartNotRemoveProductInWL(string productName,
                                                                       bool expectedIsProductInWishList,
                                                                     string userLastName)
        {
           commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToWishListButton()
                    .GetWishListPage()
                    .AddToShoppingCart(productName)
                    .CheckIsProductInWishList(expectedIsProductInWishList, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetWishListPage()
                .ClearAllWishList()
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}



