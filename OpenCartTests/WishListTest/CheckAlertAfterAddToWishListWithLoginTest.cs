﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckAlertAfterAddToWishListWithLoginTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", "Success", "CustomerOne")]
        public void CheckAlertAfterAddToWishListWithtLogin(string productName,
                                                           string exspectedAllert,
                                                           string userLastName)
        {
            commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToWishListButton()
                    .CheckAllertAddToWishList(exspectedAllert);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetWishListPage()
                .ClearAllWishList();
        }
    }
}
