﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckAddingProductFromWLToShopCartTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", true, "CustomerOne")]
        public void CheckAddingProductFromWLToShopCart(string productName,
                                                         bool expectedIsProductInShoppingCart, 
                                                       string userLastName)
        {
            commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                   .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToWishListButton()
                    .GetWishListPage()
                    .AddToShoppingCart(productName)
                    .GetShoppingCartPage()
                    .CheckIsProductInShoppingCart(expectedIsProductInShoppingCart, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart()
                .GetWishListPage()
                .ClearAllWishList();
            
        }
    }
}
