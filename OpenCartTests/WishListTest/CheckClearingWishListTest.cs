﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckClearingWishListTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", true, "CustomerOne")]
        public void CheckClearingWishList(string productName,
                                          bool expectedConditionWishList, 
                                          string userLastName)
        {
            commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToWishListButton()
                    .GetWishListPage()
                    .ClearAllWishList()
                    .CheckClearWishList(expectedConditionWishList);
        }
    }
}
