﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckOpeningWishListPageWithtLoginTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "My Wish List")]
        public void CheckOpeningWishListPageWithtLogin(string userLastName, string expectedPage)
        {
           commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .GetWishListPage()
                    .CheckOpenWishListPage(expectedPage);
        }
    }
}

