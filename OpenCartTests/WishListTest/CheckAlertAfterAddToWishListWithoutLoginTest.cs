﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    public class CheckAlertAfterAddToWishListWithoutLoginTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", "You must")]
        public void CheckAlertAfterAddToWishListWithoutLogin(string productName, string expectedAllert)
        {
            commonPage
                  .EnterTextInSearchBox(productName)
                  .ClickSearchButton()
                  .ClickProductInList(productName)
                  .ClickOnAddToWishListButton()
                  .CheckAllertAddToWishList(expectedAllert);
        }
    }
}
