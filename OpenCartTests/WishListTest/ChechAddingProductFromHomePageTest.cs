﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class ChechAddingProductFromHomePageTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("iPad mini", true, "CustomerOne")]
        public void ChechAddingProductFromHomePage(string productName,
                                                     bool expectedIsProductInWishList,
                                                   string userLastName)
        {
            commonPage
                     .ClickDropDownToggle()
                     .ClickLogInOfDropDownToggle()
                     .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                     .ClickOnSiteLogo()
                     .ClickAddToWishList(productName)
                     .GetWishListPage()
                     .CheckIsProductInWishList(expectedIsProductInWishList, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetWishListPage()
                .ClearAllWishList();
        }
    }
}


