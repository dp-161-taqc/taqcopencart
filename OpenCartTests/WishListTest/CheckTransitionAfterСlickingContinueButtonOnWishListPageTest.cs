﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckTransitionAfterСlickingContinueButtonOnWishListPageTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "My Account")]
        public void CheckTransitionAfterСlickingContinueButtonOnWishListPage(string userLastName, 
                                                                             string expextedPage)
        {
           commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .GetWishListPage()
                    .ClickContinueButton()
                    .CheckClickOnWishListPageContinueButton(expextedPage);
        }
    }
}
