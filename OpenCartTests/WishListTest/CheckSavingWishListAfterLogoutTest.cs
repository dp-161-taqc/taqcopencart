﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckSavingWishListAfterLogoutTest : BaseShoppingCartAndWishListPageTest
    {
            [TestCase("IPhone 7 32GB Black", true, "CustomerOne")]
            public void CheckSavingWishListAfterLogout(string productName, 
                                                       bool expectedIsProductInWishList, 
                                                       string userLastName)
            {
            commonPage
                      .ClickDropDownToggle()
                      .ClickLogInOfDropDownToggle()
                      .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                      .EnterTextInSearchBox(productName)
                      .ClickSearchButton()
                      .ClickProductInList(productName)
                      .ClickOnAddToWishListButton()
                      .GetWishListPage()
                      .ClickLogoutPageButton()
                      .ClickDropDownToggle()
                      .ClickLogInOfDropDownToggle()
                      .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                      .ClickModifyWishList()
                      .CheckIsProductInWishList(expectedIsProductInWishList, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetWishListPage()
                .ClearAllWishList();
        }
    }
}

