﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckAddingOnlyOneIdenticalProductTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", 1, "CustomerOne")]
        public void CheckAddingOnlyOneIdenticalProduct(string productName, 
                                                       int exspecteAmountIdenticalProducts, 
                                                       string userLastName)
        {
            commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToWishListButton()
                    .ClickOnAddToWishListButton()
                    .GetWishListPage()
                    .CheckAmountIdenticalProducts(exspecteAmountIdenticalProducts, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetWishListPage()
                .ClearAllWishList();
        }
    }
}
