﻿using NUnit.Framework;
using OpenCartTests.SpecialOffersTest;
using SeleniumHelpers.Models;

namespace OpenCartTests.WishListTest
{
    [TestFixture]
    class CheckAlertAfterAddProductToShopCartTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", "Success", "CustomerOne")]
        public void CheckAlertAfterAddProductToShopCart(string productName, 
                                                        string expectedAllert, 
                                                        string userLastName)
        {
            commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToWishListButton()
                    .GetWishListPage()
                    .AddToShoppingCart(productName)
                    .CheckAllertAfterAddProductToShopping(expectedAllert);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetWishListPage()
                .ClearAllWishList()
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
