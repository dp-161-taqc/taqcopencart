﻿using NUnit.Framework;
using OpenCartTests.Logs;
using OpenQA.Selenium;
using SeleniumHelpers.Utils;

namespace OpenCartTests
{
    public class BaseTest
    {
        protected IWebDriver driver;
        protected string BaseUrl { get; set; } = ConfigurationHelper.BaseUrl;
        static protected XmlParser Parser { get; set; } = new XmlParser();

        [SetUp]
        public void BeforeTest()
        {
            driver = WebDriverExtensions.StartWebDriver();
        }

        [TearDown]
        public void AfterTest()
        {
           Log4App.Log();
           driver.Quit();
        }
    }
}
