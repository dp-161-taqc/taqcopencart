﻿using NLog;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenCartTests.Logs
{
    public class Log4App
    {
        private static Logger logger = LogManager.GetLogger("Logger");

        public static void Log()
        {
            var className = TestContext.CurrentContext.Test.ClassName;
            var testName = TestContext.CurrentContext.Test.Name;
            var result = TestContext.CurrentContext.Result.Outcome;

            logger.Debug(className);
            logger.Debug(testName);
            logger.Debug(result.Status);

            var methodName = TestContext.CurrentContext.Test.MethodName;
            var arguments = TestContext.CurrentContext.Test.Arguments;
            var dateTime = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
            var message = TestContext.CurrentContext.Result.Message;

            GenerateReport(dateTime, result, className, methodName, arguments, String.IsNullOrEmpty(message) ? "-" : message);
        }

        public static void GenerateReport(String timeWhenLaunchTest, ResultState result, string className, string methodName, object[] argumentsArray, string message)
        {
            string pathReportFile = AppDomain.CurrentDomain.BaseDirectory + @"\..\..\Logs\log.html";
            message = message.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\n", "</br>");

            List<String> lines = File.ReadAllLines(pathReportFile).ToList();
            File.WriteAllLines(pathReportFile, lines.GetRange(0, lines.Count - 4).ToArray());

            FileStream fs = new FileStream(pathReportFile, FileMode.Append, FileAccess.Write);
            StreamWriter streamWriter = new StreamWriter(fs);

            string arguments = string.Empty;
            if (argumentsArray.Length > 0)
            {
                for (int i = 0; i < argumentsArray.Length; i++)
                {
                    arguments += (i == argumentsArray.Length - 1) ? String.Format("\"{0}\"", argumentsArray[i]) : String.Format("\"{0}\", ", argumentsArray[i]);
                }
            }

            if (methodName.Length > 40) methodName = (methodName.Substring(0, 40) + "</br>" + methodName.Substring(40, methodName.Length - 40));

            if (result == ResultState.Success) // passed test record
            {
                SucessTestWriteToHtml(streamWriter, timeWhenLaunchTest, className, methodName, arguments, message);
            }
            if (!(result == ResultState.Success)) // failed test record
            {
                FailedTestWriteToHtml(streamWriter, timeWhenLaunchTest, className, methodName, arguments, message);
            }

            WriteEndOfHtmlFile(streamWriter);
        }

        private static void SucessTestWriteToHtml(StreamWriter streamWriter, string timeWhenLaunchTest, string className, string methodName, string arguments, string message)
        {
            streamWriter.WriteLine(@"<tr style='color: green;'>" + "\n" +
                                   @"<td>" + timeWhenLaunchTest + @"</td>" + "\n" +
                                   @"<td>Passed</td>" + "\n");
            CommonReportInfo(streamWriter, className, methodName, arguments, message);
        }

        private static void FailedTestWriteToHtml(StreamWriter streamWriter, string timeWhenLaunchTest, string className, string methodName, string arguments, string message)
        {
            streamWriter.WriteLine(@"<tr style='color: red;'>" + "\n" +
                                   @"<td>" + timeWhenLaunchTest + @"</td>" + "\n" +
                                   @"<td>Failed</td>" + "\n");
            CommonReportInfo(streamWriter, className, methodName, arguments, message);
        }

        private static void CommonReportInfo(StreamWriter streamWriter, string className, string methodName, string arguments, string message)
        {
            streamWriter.WriteLine(
                    @"<td>" + className + "</td>" + "\n" +
                    @"<td>" + methodName + "</td>" + "\n" +
                    @"<td>" + arguments + "</td>" + "\n" +
                    @"<td>" + message + @"</td>" + "\n" +
                    @"</tr>");
        }

        private static void WriteEndOfHtmlFile(StreamWriter streamWriter)
        {
            streamWriter.WriteLine(
                @"</table>" + "\n" +
                @"</body>" + "\n" +
                @"" + "\n" +
                @"</html>");
            streamWriter.Close();
        }
    }
}
