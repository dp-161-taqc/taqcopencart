﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectFirstNameInputTest : StartPageSetupBaseTest
    {
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("first name should be less then thirty three characters")]
        public void CheckIncorrectFirstNameInput(string firstname)
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(firstname)
                .InputLastname(UserGenerator.GenerateUser().Lastname)
                .InputEmail(UserGenerator.GenerateUser().Email)
                .InputTelephone(UserGenerator.GenerateUser().Telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputRegion(UserGenerator.GenerateUser().Region)
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(UserGenerator.GenerateUser().Password)
                .ClickCheckBoxConfirm()
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPageFirstNameIncorrectInput();
        }
    }
}
