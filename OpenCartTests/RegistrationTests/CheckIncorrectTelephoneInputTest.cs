﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectTelephoneInputTest : StartPageSetupBaseTest
    {
        [TestCase("")]
        [TestCase("up to 32 letters, numbers and special characters)")]
        [TestCase("   ")]
        public void CheckIncorrectTelephoneInput(string telephone)
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(UserGenerator.GenerateUser().Firstname)
                .InputLastname(UserGenerator.GenerateUser().Lastname)
                .InputEmail(UserGenerator.GenerateUser().Email)
                .InputTelephone(telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputRegion(UserGenerator.GenerateUser().Region)
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(UserGenerator.GenerateUser().Password)
                .ClickCheckBoxConfirm()
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPageTelephoneIncorrectInput();
        }
    }
}
