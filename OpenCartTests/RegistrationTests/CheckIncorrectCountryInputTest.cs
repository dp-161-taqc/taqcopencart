﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectCountryInputTest : StartPageSetupBaseTest
    {
        [TestCase("--- Please Select ---")]
        public void CheckIncorrectCountryInput(string country)
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(UserGenerator.GenerateUser().Firstname)
                .InputLastname(UserGenerator.GenerateUser().Lastname)
                .InputEmail(UserGenerator.GenerateUser().Email)
                .InputTelephone(UserGenerator.GenerateUser().Telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputCountry(country)
                .InputRegion("--- None ---")
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(UserGenerator.GenerateUser().Password)
                .ClickCheckBoxConfirm()
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPageCountryIncorrectInput();
        }
    }
}
