﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectEmailInputTest : StartPageSetupBaseTest
    {
        [TestCase("")]
        [TestCase("Abc..123@example.com")]
        [TestCase("email.@example.com")]
        public void CheckIncorrectEmailInput(string email)
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(UserGenerator.GenerateUser().Firstname)
                .InputLastname(UserGenerator.GenerateUser().Lastname)
                .InputEmail(email)
                .InputTelephone(UserGenerator.GenerateUser().Telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputRegion(UserGenerator.GenerateUser().Region)
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(UserGenerator.GenerateUser().Password)
                .ClickCheckBoxConfirm()
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPageEmailIncorrectInput();
        }
    }
}
