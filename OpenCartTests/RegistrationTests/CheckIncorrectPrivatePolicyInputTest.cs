﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectPrivatePolicyInputTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckIncorrectPrivatePolicyInput()
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(UserGenerator.GenerateUser().Firstname)
                .InputLastname(UserGenerator.GenerateUser().Lastname)
                .InputEmail(UserGenerator.GenerateUser().Email)
                .InputTelephone(UserGenerator.GenerateUser().Telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputRegion(UserGenerator.GenerateUser().Region)
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(UserGenerator.GenerateUser().Password)
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPagePrivatePolicyIncorrectInput();
        }
    }
}
