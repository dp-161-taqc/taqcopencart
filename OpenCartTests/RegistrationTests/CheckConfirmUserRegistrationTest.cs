﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckConfirmUserRegistrationTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckRegistrationConfirm()
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .ClickContinueButton(UserGenerator.GenerateUser())
                .CheckAccountSucessPage();
        }
    }
}
