﻿using NUnit.Framework;
using OpenCartTests.LoginTests;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckLoginPageLinkButtonTest : StartPageSetupBaseTest
    {
        [Test]
        public void CheckLoginPageLinkButton()
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .ClickLoginPageButton()
                .CheckLoginPage();
        }
    }
}
