﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectLastNameInputTest : StartPageSetupBaseTest
    {
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("last name should be less then thirty three characters")]
        public void CheckIncorrectLastNameInput(string lastName)
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(UserGenerator.GenerateUser().Firstname)
                .InputLastname(lastName)
                .InputEmail(UserGenerator.GenerateUser().Email)
                .InputTelephone(UserGenerator.GenerateUser().Telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputRegion(UserGenerator.GenerateUser().Region)
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(UserGenerator.GenerateUser().Password)
                .ClickCheckBoxConfirm()
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPageLastNameIncorrectInput();
        }
    }
}
