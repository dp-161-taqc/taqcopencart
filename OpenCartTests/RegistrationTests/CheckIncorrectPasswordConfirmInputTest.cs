﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.RegistrationTests
{
    [TestFixture]
    public class CheckIncorrectPasswordConfirmInputTest : StartPageSetupBaseTest
    {
        [TestCase("")]
        [TestCase("abc")]
        [TestCase("afkldfklfgdfgdsfdklsfdklsf")]
        public void CheckIncorrectPasswordConfirmInput(string passwordConfirm)
        {
            commonPageWithMenu
                .ClickRegisterButton()
                .InputFirstname(UserGenerator.GenerateUser().Firstname)
                .InputLastname(UserGenerator.GenerateUser().Lastname)
                .InputEmail(UserGenerator.GenerateUser().Email)
                .InputTelephone(UserGenerator.GenerateUser().Telephone)
                .InputAddress1(UserGenerator.GenerateUser().Address1)
                .InputCity(UserGenerator.GenerateUser().City)
                .InputRegion(UserGenerator.GenerateUser().Region)
                .InputPassword(UserGenerator.GenerateUser().Password)
                .InputPasswordConfirm(passwordConfirm)
                .ClickCheckBoxConfirm()
                .ClickContinueButtonIncorrectInput()
                .CheckRegisterPagePasswordConfirmIncorrectInput();
        }
    }
}
