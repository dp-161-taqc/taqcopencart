﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckReviewWithIncorrectReviewTest : BaseProductTest
    {
        [TestCase("Good Name", "", 5)]
        [TestCase("Good Name", "Bad review!", 5)]
        public void CheckReviewWithIncorrectReview(string name, string review, int rating)
        {
            productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .OpenReviewTab()
                .FillName(name)
                .FillReview(review)
                .SetRatingRadioButton(rating)
                .ClickContinueButton()
                .CheckWarningAboutReview();           
        }
    }
}
