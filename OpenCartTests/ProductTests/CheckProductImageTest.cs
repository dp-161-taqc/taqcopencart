﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckProductImageTest : BaseProductTest
    {
        [Test]
        public void CheckProductImage()
        {
            productPage
                 .OpenProductPageById(caseForIphoneModel.Id)
                 .CheckSmallProductImage(caseForIphoneModel.SmallProductImageSrc)
                 .ClickOnImage()
                 .CheckLargeProductImage(caseForIphoneModel.LargeProductImageSrc)
                 .ClickOnCloseButton()
                 .CheckSmallProductImage(caseForIphoneModel.SmallProductImageSrc); 
        }
    }       
}
