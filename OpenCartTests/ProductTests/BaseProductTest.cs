﻿using NUnit.Framework;
using SeleniumHelpers.Models;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.ProductTests
{
    public class BaseProductTest : BaseTest
    {
        protected ProductModel caseForIphoneModel;
        protected ProductPage productPage;

        [SetUp]
        public void BeforeProductTest()
        {
            productPage = new ProductPage(driver);
            caseForIphoneModel = ProductGenerator.CreateCaseForIphone();
        }
    }
}
