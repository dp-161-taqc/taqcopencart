﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckOpenProductFromSearchTest : BaseProductTest
    {
        [Test]
        public void CheckOpenProductFromSearch()
        {
            productPage
                .GotoBaseUrl()
                .EnterTextInSearchBox(caseForIphoneModel.Name)
                .ClickSearchButton()
                .ClickProductInList(caseForIphoneModel.Name)
                .CheckProductName(caseForIphoneModel.Name);
        }
    }
}
