﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckOpenProductFromCategoryListTest : BaseProductTest
    {
        [TestCase("Accessories", "Cases")]
        public void CheckOpenProductFromCategoryList(string category, string subcategory)
        {
            productPage
                .GotoBaseUrl()
                .HoverCategory(category)
                .ClickCategory(subcategory)
                .ClickProductInList(caseForIphoneModel.Name)
                .CheckProductName(caseForIphoneModel.Name);
        }
    }
}
