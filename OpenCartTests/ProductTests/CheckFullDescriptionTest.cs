﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    public class CheckFullDescriptionTest : BaseProductTest 
    {
        [Test]
        public void CheckFullDescription()
        {    
            productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .CheckFullDescriptionTabAndText(caseForIphoneModel.FullDescriptionTab, caseForIphoneModel.FullDescription);            
        }
    }
}
