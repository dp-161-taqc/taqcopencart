﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckReviewWithCorrectDataTest : BaseProductTest
    {
        [TestCase("Good Name", "Good review! Good review! Good review!", 5)]         
        public void CheckReviewWithCorrectData(string name, string review, int rating)
        {
            productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .OpenReviewTab()
                .FillName(name)
                .FillReview(review)
                .SetRatingRadioButton(rating)
                .ClickContinueButton()
                .CheckGoodWarning();
        }
    }
}
