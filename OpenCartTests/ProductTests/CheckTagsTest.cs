﻿using NUnit.Framework;
using System.Linq;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckTagsTest : BaseProductTest
    {
        [Test]
        public void CheckTags()
        {
            productPage
                 .OpenProductPageById(caseForIphoneModel.Id)
                 .ClickOnTag(caseForIphoneModel.Tag)
                 .CheckTagsContainsProduct(caseForIphoneModel.Name, caseForIphoneModel.TagProducts.First().Name);           
        }
    }    
}
