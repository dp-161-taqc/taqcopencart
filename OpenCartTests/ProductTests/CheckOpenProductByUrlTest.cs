﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckOpenProductByUrlTest : BaseProductTest
    {
        [Test]
        public void CheckOpenProductByUrl()
        {
            productPage
                  .OpenProductPageById(caseForIphoneModel.Id)
                  .CheckProductName(caseForIphoneModel.Name);           
        }
    }
}
