﻿using NUnit.Framework;
using System.Linq;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckRelatedProductsTest : BaseProductTest
    {
        [Test]
        public void CheckRelatedProducts()
        {
           productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .ClickRelatedProductInList(caseForIphoneModel.RelatedProducts.First().Name)               
                .CheckProductName(caseForIphoneModel.RelatedProducts.FirstOrDefault().Name);      
        }
    }
}
