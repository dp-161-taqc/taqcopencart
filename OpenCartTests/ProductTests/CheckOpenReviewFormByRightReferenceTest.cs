﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckOpenReviewFormByRightReferenceTest : BaseProductTest
    {
        [Test]
        public void CheckOpenReviewFormByRightReference()
        {
            productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .ClickReviewRightReference()                
                .CheckReviewsLabelAndText();
        }
    }
}
