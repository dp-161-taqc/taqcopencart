﻿using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckNameForProductReviewFormForLoggedUserTest : BaseProductTest
    {
        [TestCase("Accessories", "Cases")]
        public void CheckNameForProductReviewFormForLoggedUser(string category, string subcategory)
        {
            productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .ClickDropDownToggle()
                .ClickLogInOfDropDownToggle()
                .ClickLoginButtonCenterMenu(Users.UserForProductAndContactUsForm())
                .HoverCategory(category)
                .ClickCategory(subcategory)
                .ClickProductInList(caseForIphoneModel.Name)
                .ClickReviewRightReference()
                .CheckNameForProductReviewFormForLoggedUser
                    (Users.UserForProductAndContactUsForm().Firstname + "\u00a0" +
                           Users.UserForProductAndContactUsForm().Lastname);   
        }
    }
}
