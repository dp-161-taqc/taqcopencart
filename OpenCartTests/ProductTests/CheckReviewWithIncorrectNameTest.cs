﻿using NUnit.Framework;
using System;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckReviewWithIncorrectNameTest : BaseProductTest
    {
        [TestCase("", "Good review! Good! Good !", 5)]
        [TestCase("    ", "Good review! Good! Good !", 4)]
        [TestCase("Bd", "Good review! Good! Good !", 5)]
        [TestCase("Very long first name and last name", "Good review! Good! Good !", 5)]
        public void CheckReviewWithIncorrectName(string name, string review, int rating)
        {
            productPage
                  .OpenProductPageById(caseForIphoneModel.Id)
                  .OpenReviewTab()
                  .FillName(name)
                  .FillReview(review)
                  .SetRatingRadioButton(rating)
                  .ClickContinueButton()
                  .CheckWarningAboutName();            
        }
    }
}
