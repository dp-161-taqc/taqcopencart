﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    public class CheckProductPriceTest : BaseProductTest
    {
        [Test]
        public void CheckPrice()
        { 
            productPage
                 .OpenProductPageById(caseForIphoneModel.Id)
                 .CheckActualPriceAndOldPrice(caseForIphoneModel.Price, caseForIphoneModel.OldPrice);
        }
    }
}
