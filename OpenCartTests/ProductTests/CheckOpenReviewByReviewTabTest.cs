﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckOpenReviewByReviewTabTest : BaseProductTest
    {
        [Test]
        public void CheckOpenReviewByReviewTab()
        {            
            productPage
                  .OpenProductPageById(caseForIphoneModel.Id)
                  .OpenReviewTab()
                  .CheckReviewsLabelAndText();
        }
    }
}
