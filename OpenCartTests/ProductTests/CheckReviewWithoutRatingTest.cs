﻿using NUnit.Framework;

namespace OpenCartTests.ProductTests
{
    [TestFixture]
    class CheckReviewWithoutRatingTest : BaseProductTest
    {
        [TestCase("12", "Bad")]
        [TestCase("", "Good review! Good review! Good review!")]
        [TestCase("Good Name", "")]
        [TestCase("Very long first name and last name", "Good review! Good! Good !")]
        [Test]
        public void CheckReviewWithoutRating(string name, string review)
        {
            productPage
                .OpenProductPageById(caseForIphoneModel.Id)
                .OpenReviewTab()
                .FillName(name)
                .FillReview(review)               
                .ClickContinueButton()
                .CheckWarningAboutRating();           
        }
    }
}
