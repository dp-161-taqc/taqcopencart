﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectProductCodeTest : BaseReturnProductsTest
    {       
        [TestCase("")]
        [TestCase("1")] 
        public void CheckIncorrectDataInFieldProductCode(string productCode)
        {
            returnsFormPage
                .SetDataInFieldProductCode(productCode)
                .ClickSubmitButton()
                .CheckIncorrectMessageForProductCode();
        }
    }
}
