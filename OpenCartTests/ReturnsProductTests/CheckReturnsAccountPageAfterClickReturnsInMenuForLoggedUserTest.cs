﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckReturnsAccountPageAfterClickReturnsInMenuForLoggedUserTest : BaseReturnProductsTest
    {
        [Test]
        public void CheckReturnsAccountPageAfterClickReturnsInMenuForLoggedUser()
        {
            returnsFormPage
               .ClickLoginButton()
               .ClickLoginButtonCenterMenu(Users.UserForReturnsForm())
               .ClickReferenceReturnsInFooter()
               .ClickReturnsInMenuForLoggedUser()
               .CheckReturnsAccountPage();
        }
    }
}
