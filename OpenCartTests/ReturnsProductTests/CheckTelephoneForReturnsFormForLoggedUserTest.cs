﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckTelephoneForReturnsFormForLoggedUserTest : BaseReturnProductsTest
    {
        [Test]
        public void CheckFieldTelephoneForReturnsFormForLoggedUser()
        {
            returnsFormPage
                   .ClickLoginButton()
                   .ClickLoginButtonCenterMenu(Users.UserForReturnsForm())
                   .ClickReferenceReturnsInFooter()
                   .CheckDataInFieldTelephoneAfterLogging();
        }

    }
}
