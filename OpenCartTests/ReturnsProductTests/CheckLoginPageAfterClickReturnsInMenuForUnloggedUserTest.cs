﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckLoginPageAfterClickReturnsInMenuForUnloggedUserTest : BaseReturnProductsTest
    {
        [Test]
        public void CheckLoginPageAfterClickReturnsInMenuForUnloggedUser()
        {
            returnsFormPage
                .ClickReturnsInMenuForUnloggedUser()
                .CheckLoginPage();
        }
    }
}
