﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectEmailTest:BaseReturnProductsTest
    {
        [TestCase("")]
        [TestCase("aa.com")]
        [TestCase("aa@com")]
        [TestCase("aa@.11")]
        [TestCase("aa")]
        [TestCase("@ .")]
        public void CheckIncorrectDataInFieldEmail(string email)
        {
            returnsFormPage
                .SetDataInFieldEmail(email)
                .ClickSubmitButton()
                .CheckIncorrectMessageForEmail();
        }
    }
}
