﻿
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckUrlForReturnsFormTest : BaseReturnProductsTest
    {        
       [Test]
        public void CheckUrlForReturnsForm()
        {
            returnsFormPage
                .CheckUrlForReturnsForm();           
        }
    }
}
