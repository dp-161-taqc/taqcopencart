﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckSubmittingOtherDetailsFieldTests:BaseReturnProductsTest
    {
        [TestCase("")]
        [TestCase("(letters, numbers10 and special characters)")]        
        public void CheckingSubmitWithAnyDataForQuantityField(string otherDetails)
        {
            returnsFormPage
              .FillDataInRequiredFields()
              .SetFieldOtherDetails(otherDetails)
              .ClickSuccessSubmit()
              .CheckSuccessMessageForReturns();
        }

    }
}
