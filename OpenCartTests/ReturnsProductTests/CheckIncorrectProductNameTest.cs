﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectProductNameTest:BaseReturnProductsTest
    {    
     
        [TestCase("")]
        [TestCase("1")] 
        public void CheckIncorrectDataInFieldProductName(string productName)
        {           
            returnsFormPage               
                .SetDataInFieldProductName(productName)
                .ClickSubmitButton()
                .CheckIncorrectMessageForProductName();       
                 
        }
    }
}
