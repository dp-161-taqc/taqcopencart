﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckProductReturnsInformationIsCorrectTest:BaseReturnProductsTest
    {
        [Test]
        public void CheckProductReturnsInformationIsCorrect()
        {
            returnsFormPage
              .ClickLoginButton()
              .ClickLoginButtonCenterMenu(Users.UserForReturnsForm())
              .ClickReferenceReturnsInFooter()
              .FillDataInRequiredFieldsForLoggedUser()
              .ClickSuccessSubmit()
              .ClickReturnsInMenuForLoggedUser()
              .ClickViewButton()
              .CheckReturnIdInfoIsCorrect();
        }
    }
}
