﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectDataForOrderIdTest:BaseReturnProductsTest
    {
        [Test]
        public void CheckIncorrectDataForOrderId()
        {
            returnsFormPage
                .SetFieldOderId("")
                .ClickSubmitButton()
                .CheckIncorrectMessageForFieldOderId();
        }
    }
}
