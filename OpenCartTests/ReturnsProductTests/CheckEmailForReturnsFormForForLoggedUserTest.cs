﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckFieldEmailForReturnsFormForLoggedUserTest:BaseReturnProductsTest
    {
        [Test]
        public void CheckFieldEmailForReturnsFormForLoggedUser()
        {
            returnsFormPage
                   .ClickLoginButton()
                   .ClickLoginButtonCenterMenu(Users.UserForReturnsForm())
                   .ClickReferenceReturnsInFooter()
                   .CheckDataInFieldEmailAfterLogging();
        }
    }
}
