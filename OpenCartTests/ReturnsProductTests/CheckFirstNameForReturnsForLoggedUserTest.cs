﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckFirstNameForReturnsForForLoggedUserTest : BaseReturnProductsTest
    {
        [Test]
        public void CheckFieldFirstNameForReturnsForLoggedUser()
        {  
            returnsFormPage
                   .ClickLoginButton()
                   .ClickLoginButtonCenterMenu(Users.UserForReturnsForm())
                   .ClickReferenceReturnsInFooter()
                   .CheckDataInFieldFirstNameAfterLogging(); 
        }
    }
}
