﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectLastNameTest:BaseReturnProductsTest
    {
        [TestCase("up to 32 letters, numbers and special characters)")]
        [TestCase("")]
        public void CheckIncorrectDataInFieldLastName(string lastName)
        {
            returnsFormPage           
                .SetLastName(lastName)
                .ClickSubmitButton()
                .CheckIncorrectMessageForFieldLastName();               
        }
    }
}
