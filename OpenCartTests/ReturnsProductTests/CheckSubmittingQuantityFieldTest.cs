﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckSubmittingQuantityFieldTest:BaseReturnProductsTest
    {
        [TestCase("")]
        [TestCase("letters, numbers and special characters)")]
        [TestCase("0")]
        public void CheckingSubmitWithAnyDataForQuantityField(string quantityItem)
        {
              returnsFormPage
                .FillDataInRequiredFields()               
                .SetFieldQuantity(quantityItem)
                .ClickSuccessSubmit()               
                .CheckSuccessMessageForReturns(); 
        }
    }
}
