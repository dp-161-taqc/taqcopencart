﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectFirstNameTest : BaseReturnProductsTest
    {
        [TestCase("up to 32 letters, numbers and special characters)")]
        [TestCase("")]     
        public void CheckIncorrectDataInFieldFirstName(string firstName)
        {            
            returnsFormPage
                .SetFirstName(firstName)
                .ClickSubmitButton()
                .CheckIncorrectMessageForFieldFirstName();
        }
    }
}
