﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Models;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckSubmittingWithCorrectDataInRequiredFieldsTest:BaseReturnProductsTest
    {
        [Test]
        public void CheckSubmittingWithCorrectDataInRequiredFields()
        {
             returnsFormPage
                .FillDataInRequiredFields()
                .ClickSuccessSubmit()                
                .CheckSuccessMessageForReturns();
        }
    }
}
