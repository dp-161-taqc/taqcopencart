﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckAboutUsPageOpenedTest:BaseReturnProductsTest
    {
        [Test]
        public void CheckAboutUsPageOpened()
        {
            returnsFormPage
                .ClickReferenceAboutUsInFooter()
                .CheckAboutUsPageOpened();
        }
    }
}
