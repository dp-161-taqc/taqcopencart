﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckLoginPageOpenedAfterClickBackForUnloggedUserTest : BaseReturnProductsTest
    {
        [Test]
        public void CheckLoginPageAfterClickBackForUnloggedUser()
        {
            returnsFormPage
                 .ClickBackForUnloggedUser()
                 .CheckLoginPage(); 
        }
    }
}
