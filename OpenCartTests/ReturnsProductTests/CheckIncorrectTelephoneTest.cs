﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckIncorrectTelephoneTest:BaseReturnProductsTest
    {
        [TestCase("")]
        [TestCase("up to 32 letters, numbers and special characters)")]
        [TestCase("12")]
        public void CheckIncorrectDataInFieldTelephone(string telephone)
        {
            returnsFormPage               
                .SetDataInFieldTelephone(telephone)
                .ClickSubmitButton()
                .CheckIncorrectMessageForFieldTelephone();
        }
    }
}
