﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckFieldOtherDetailsTest:BaseReturnProductsTest
    {
        [TestCase("")]
        [TestCase("letters, numbers and special characters)")]       
        public void CheckFieldOtherDetails(string otherDetails)
        {
            returnsFormPage
              .FillDataInRequiredFields()
              .SetFieldOtherDetails(otherDetails)
              .ClickSuccessSubmit()
              .CheckSuccessMessageForReturns();
        }
    }
}
