﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Pages;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckHomePageOpenedTest:BaseTest
    {
        protected ReturnsSuccessedPage returnsSuccessedPage;
        protected string urlSuccessedPage;        

        [SetUp]
        public void BeforeProductTest()
        {
            returnsSuccessedPage = new ReturnsSuccessedPage(driver);
            urlSuccessedPage = BaseUrl + string.Format(URL.PathToReturnsSuccessedPage);
            driver.Navigate().GoToUrl(urlSuccessedPage);
        }

        [Test]
        public void CheckHomePageOpenedAfterClickContinue()
        {
            returnsSuccessedPage
                .ClickButtonContinue()
                .CheckHomePageIsOpened();       
        }
    }
}
