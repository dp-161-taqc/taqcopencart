﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Data;

namespace OpenCartTests.ReturnsProductTests
{
    [TestFixture]
    class CheckAccountPageOpenedAfterClickBackForForLoggedUserTest : BaseReturnProductsTest
    {
        [Test]
        public void CheckAccountPageOpenedAfterClickBackForLoggedUser()
        {
            returnsFormPage
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(Users.UserForReturnsForm())
                .ClickReferenceReturnsInFooter()
                .ClickBackForLoggedUser()
                .CheckAccountPage();
        }
    }
}
