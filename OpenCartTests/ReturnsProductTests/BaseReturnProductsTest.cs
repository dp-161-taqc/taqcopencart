﻿
using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Models;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.ReturnsProductTests
{
    class BaseReturnProductsTest:BaseTest
    {       
        protected ReturnsFormPage returnsFormPage;       

        protected string urlReturnsFormPage;     

        [SetUp]
        public void BeforeProductTest()
        {
            returnsFormPage = new ReturnsFormPage(driver);
          
            urlReturnsFormPage = BaseUrl + URL.PathToReturnsForm;         
            
            driver.Navigate().GoToUrl(urlReturnsFormPage);
        }
    }
}
