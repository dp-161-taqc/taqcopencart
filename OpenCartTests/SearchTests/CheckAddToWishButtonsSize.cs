﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #29
    public class CheckAddToWishButtonsSize : BaseSearchTest
    {
        [TestCase("a", 52, 40)]
        public void CheckAddToWishButtonsSizeTest
                 (string userSearchQuery, int expectedWidth, int expectedHeight)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckSizeAllElementsInList(expectedWidth, expectedHeight, searchPage.ButtonsAddToWishList());
        }
    }
}