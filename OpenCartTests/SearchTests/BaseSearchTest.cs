﻿using NUnit.Framework;
using SeleniumHelpers.Data;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Utils;

namespace OpenCartTests.SearchTests
{
    public class BaseSearchTest : BaseTest
    {
        protected ProductPage productPage;
        protected CommonActions action;
        protected SearchPage searchPage;
        protected ProductComparePage productComparePage;  

        [SetUp]
        public void BeforeSearchtTest()
        {
            searchPage = new SearchPage(driver);
            productPage = new ProductPage(driver);
            productComparePage = new ProductComparePage(driver);
            action = new CommonActions(driver);

            action.WaitImplicitly(5);
            action.OpenURL(BaseUrl + URL.PathToSearchPage);
            searchPage.CheckIsSearchPageLoaded();
        }

    }
}
