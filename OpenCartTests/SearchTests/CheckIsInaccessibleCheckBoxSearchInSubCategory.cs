﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 17
    public class CheckIsInaccessibleCheckBoxSearchInSubCategory : BaseSearchTest
    {
        [TestCase("All Categories", "iphone")]
        public void CheckIsInaccessibleCheckBoxSearchInSubCategoryTest
            (string searchCategory, string searchQuery)
        {
            searchPage
                .EnterTextInSearchBox(searchQuery)
                .ClickSearchButton()
                .ChooseSearchCategory(searchCategory)
                .CheckIsInaccessibleCheckBoxSearchInSubCategory();
        }
    }
}
