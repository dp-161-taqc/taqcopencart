﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] //#5
    public class CheckSearchPageThatUrlEndsWith3LastCharsOfSearchQuery : BaseSearchTest
    {
        [TestCase("search")]
        [TestCase("QUERY")]
        [TestCase("457789843436")]
        public void CheckSearchPageThatUrlEndsWith3LastCharsOfSearchQueryTest(string userSearchQuery)
        {
            string expectedLastCharsOfUrl = action.GetLastChars(3, userSearchQuery);
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckStringEndsWith(expectedLastCharsOfUrl, driver.Url);
        }
    }
}
