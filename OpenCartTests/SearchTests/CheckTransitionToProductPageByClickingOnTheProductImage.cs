﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #1
    public class CheckTransitionToProductPageByClickingOnTheProductImage : BaseSearchTest
    {
        [TestCase("watch", 0)]
        public void CheckTransitionToTheProductPageByClickingOnTheProductImageTest
            (string userSearchQuery, int indexOfElement)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .ClickProductInImagesListByIndex(indexOfElement)
                .CheckIsProductPageLoaded();
        }
    }
}
