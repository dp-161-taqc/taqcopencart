﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 13
    public class CheckProductSmallImageSizeOnSearchPage : BaseSearchTest
    {
        [TestCase("iphone", 228, 228)]
        public void CheckProductSmallImageSizeOnSearchPageTest
           (string userSearchQuery, int expectedWidth, int expectedHeight)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckSizeAllElementsInList(expectedWidth, expectedHeight, searchPage.ProductImagesList());
        }
    }
}