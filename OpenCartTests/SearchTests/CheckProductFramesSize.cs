﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #26
    public class CheckProductFramesSize : BaseSearchTest
    {
        [TestCase("i", 292, 491)]
        public void CheckProductFramesSizeTest
                 (string userSearchQuery, int expectedWidth, int expectedHeight)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckSizeAllElementsInList(expectedWidth, expectedHeight, searchPage.ProductFramesList());
        }
    }
}
