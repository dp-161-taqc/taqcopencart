﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #27
    public class CheckCompareButtonsSize : BaseSearchTest
    {
        [TestCase("a", 52, 40)]
        public void CheckCompareButtonsSizeTest
                 (string userSearchQuery, int expectedWidth, int expectedHeight)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckSizeAllElementsInList(expectedWidth, expectedHeight, searchPage.ButtonsCompareProductList());
        }
    }
}