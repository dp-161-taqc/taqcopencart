﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 25
    public class CheckThatSearchByDescriptionIsNotFindProductsWhenCheckboxDisabled : BaseSearchTest
    {
        [TestCase("can", 0)]
        [TestCase("get", 0)]
        public void CheckThatSearchByDescriptionIsNotFindProductsWhenCheckboxDisabledTest
            (string userSearchQuery, int expectedQuantityOfProducts)
        {
            searchPage
                .EnterTextInExtraSearchBox(userSearchQuery)
                .ClickExtraSearchButton()
                .CheckAreEqual(expectedQuantityOfProducts, searchPage.ProductNamesList().Count);
        }
    }
}
