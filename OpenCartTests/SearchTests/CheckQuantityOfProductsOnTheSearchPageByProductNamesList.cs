﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #10
    public class CheckQuantityOfProductsOnTheSearchPageByProductNamesList : BaseSearchTest
    {
        [TestCase("ipad", 6)]
        [TestCase("watch", 2)]
        public void CheckQuantityOfProductsOnTheSearchPageByProductNamesListTest
            (string userSearchQuery, int expectedNumberOfProducts)
        {
            action.WaitExplicitlyWithCondition(driver, "Title == Search Page");
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckAreEqual(expectedNumberOfProducts, searchPage.ProductNamesList().Count);
        }
    }
}
