﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #7
    public class CheckProductSortingByPriceIncrease : BaseSearchTest
    {
        [TestCase("ipad", "Price (Low > High)")]
        [TestCase("iphone", "Price (Low > High)")]
        [TestCase("watch", "Price (Low > High)")]
        public void CheckProductSortingByPriceIncreaseTest(string userSearchQuery, string sortMethod)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .ClickSelectorSortProductsBy()
                .SortProductsBy(sortMethod)
                .CheckCollectionIsOrdered(searchPage.GetPrices());
        }
    }
}