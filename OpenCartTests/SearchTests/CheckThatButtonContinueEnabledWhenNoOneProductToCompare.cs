﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 23
    public class CheckThatButtonContinueEnabledWhenNoOneProductToCompare : BaseSearchTest
    {
        [TestCase("ipad")]
        public void CheckThatButtonContinueEnabledWhenNoOneProductToCompareTest(string userSearchQuery)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .ClickOnCompareProductLink()
                .CheckIsProductComparePageLoaded()
                .CheckIfButtonContinueClickableThenPass();
        }
    }
}
