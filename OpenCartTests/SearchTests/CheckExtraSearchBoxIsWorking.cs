﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 12
    public class CheckExtraSearchBoxIsWorkingCorrect : BaseSearchTest
    {
        [TestCase("watch")]
        public void CheckExtraSearchBoxIsWorkingTest(string expextedPartOfTitle)
        {
            searchPage
                .EnterTextInExtraSearchBox(expextedPartOfTitle)
                .ClickExtraSearchButton()
                .CheckStringContains(expextedPartOfTitle, driver.Title);
        }
    }
}
