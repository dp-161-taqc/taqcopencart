﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #28
    public class CheckAddToCartButtonsSize : BaseSearchTest
    {
        [TestCase("a", 156, 40)]
        public void CheckAddToCartButtonsSizeTest
                 (string userSearchQuery, int expectedWidth, int expectedHeight)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckSizeAllElementsInList(expectedWidth, expectedHeight, searchPage.ButtonsAddToCartList());
        }
    }
}