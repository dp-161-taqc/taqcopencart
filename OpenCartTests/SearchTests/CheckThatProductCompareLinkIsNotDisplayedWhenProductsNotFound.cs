﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 15
    public class CheckThatProductCompareLinkIsNotDisplayedWhenProductsNotFound : BaseSearchTest
    {
        [TestCase("")]
        [TestCase("7700707007")]
        public void CheckThatProductCompareLinkIsNotDisplayedWhenProductsNotFoundTest(string invalidInput)
        {
            searchPage
                .EnterTextInSearchBox(invalidInput)
                .ClickSearchButton()
                .GetChecker()
                .ChekIfNoSuchElementThenPass(searchPage.productCompareLink);
        }
    }
}
