﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #8
    public class CheckProductSortingByNameReduce : BaseSearchTest
    {
        [TestCase("iphone", "Name (Z - A)")]
        [TestCase("mac", "Name (Z - A)")]
        [TestCase("ipad", "Name (Z - A)")]
        [TestCase("watch", "Name (Z - A)")]
        public void CheckProductSortingByNameReduceTest(string userSearchQuery, string sortMethod)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .ClickSelectorSortProductsBy()
                .SortProductsBy(sortMethod)
                .CheckCollectionIsOrderedDescending(searchPage.GetProductNamesList());
        }
    }
}
