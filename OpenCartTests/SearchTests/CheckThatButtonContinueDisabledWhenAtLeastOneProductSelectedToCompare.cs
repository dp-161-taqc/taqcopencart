﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 22
    public class CheckThatButtonContinueDisabledWhenAtLeastOneProductSelectedToCompare : BaseSearchTest
    {
        [TestCase("ipad", 4)]
        public void CheckThatButtonContinueDisabledWhenAtLeastOneProductSelectedToCompareTest
            (string userSearchQuery, int quantityOfProducts)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .AddProductsToCompare(quantityOfProducts)
                .ClickOnCompareProductLink()
                .CheckIsProductComparePageLoaded()
                .CheckIfButtonContinueClickableThenFail();
        }
    }   
}
