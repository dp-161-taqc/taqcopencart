﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #6
    public class CheckProductSortingByPriceReduce : BaseSearchTest
    {
        [TestCase("ipad", "Price (High > Low)")]
        [TestCase("iphone", "Price (High > Low)")]
        [TestCase("watch", "Price (High > Low)")]
        public void CheckProductSortingByPriceReduceTest(string userSearchQuery, string sortMethod)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .ClickSelectorSortProductsBy()
                .SortProductsBy(sortMethod)
                .CheckCollectionIsOrderedDescending(searchPage.GetPrices());
        }
    }
}