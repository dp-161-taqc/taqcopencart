﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 20
    public class CheckQuantityOfProductsOnComparePage : BaseSearchTest
    {
        [TestCase("iphone", 3)]
        public void CheckQuantityOfProductsOnComparePageTest(string userSearchQuery, int expectedQuantityOfProducts)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .AddProductsToCompare(expectedQuantityOfProducts)
                .ClickOnCompareProductLink()
                .CheckIsProductComparePageLoaded()
                .GetChecker()
                .CheckAreEqual
                     (expectedQuantityOfProducts, productComparePage.ProductsToCompareImagesList().Count);
        }
    }
}
