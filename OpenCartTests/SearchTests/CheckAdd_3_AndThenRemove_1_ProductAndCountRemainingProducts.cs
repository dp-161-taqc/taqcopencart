﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 21
    public class CheckAdd_3_AndThenRemove_1_ProductAndCountRemainingProducts : BaseSearchTest
    {
        [TestCase("iphone", 3, 0, 2)]
        public void CheckAdd_3_AndThenRemove_1_ProductAndCountRemainingProductsTest
            (string userSearchQuery, int quantityOfProducts, int indexOfProduct, int expectedQuantityOfProducts)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .AddProductsToCompare(quantityOfProducts)
                .ClickOnCompareProductLink()
                .CheckIsProductComparePageLoaded()
                .RemoveСomparableProductByIndex(indexOfProduct)       
                .GetChecker()
                .CheckAreEqual(expectedQuantityOfProducts, productComparePage.ProductsToCompareImagesList().Count);
        }
    }
}