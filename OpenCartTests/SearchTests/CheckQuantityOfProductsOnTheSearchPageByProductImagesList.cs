﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 11
    public class CheckQuantityOfProductsOnTheSearchPageByProductImagesList : BaseSearchTest
    {
        [TestCase("mac", 2)]
        [TestCase("ipad", 6)]
        [TestCase("watch", 2)]
        public void CheckQuantityOfProductsOnTheSearchPageByProductImagesListTest
            (string userSearchQuery, int expectedNumberOfProduct)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckAreEqual(expectedNumberOfProduct, searchPage.ProductImagesList().Count);
        }
    }
}
