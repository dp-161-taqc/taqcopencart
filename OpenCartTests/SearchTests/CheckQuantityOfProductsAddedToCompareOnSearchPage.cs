﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 18
    public class CheckQuantityOfProductsAddedToCompareOnSearchPage : BaseSearchTest
    {
        [TestCase("iphone", 3)]
        public void CheckQuantityOfProductsAddedToCompareOnSearchPageTest
            (string userSearchQuery, int expectedQuantityOfProducts)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .AddProductsToCompare(expectedQuantityOfProducts)
                .CheckAreEqual(expectedQuantityOfProducts, searchPage.GetNumberOfProductsToCompareByLink());
        }
    }
}
