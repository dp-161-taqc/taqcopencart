﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #30
    public class CheckBlocksProductCaptionSize : BaseSearchTest
    {
        [TestCase("ipad", 260, 180)]
        public void CheckBlocksProductCaptionSizeTest
                 (string userSearchQuery, int expectedWidth, int expectedHeight)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .CheckSizeAllElementsInList(expectedWidth, expectedHeight, searchPage.BlockProductCaptionList());
        }
    }
}