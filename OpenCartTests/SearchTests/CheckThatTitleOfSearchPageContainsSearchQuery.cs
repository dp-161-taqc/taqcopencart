﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{ 
    [TestFixture] // #3
    public class CheckThatTitleOfSearchPageContainsSearchQuery : BaseSearchTest
    {
        [TestCase("2432437980")]
        [TestCase("some search query...")]
        [TestCase("$^*$$#^*%_#%*&")]
        public void CheckThatTitleOfSearchPageContainsSearchQueryTest(string expectedPartOfTitle)
        {
            searchPage
                .EnterTextInSearchBox(expectedPartOfTitle)
                .ClickSearchButton()
                .CheckStringContains(expectedPartOfTitle, driver.Title);
        }
    }
}
