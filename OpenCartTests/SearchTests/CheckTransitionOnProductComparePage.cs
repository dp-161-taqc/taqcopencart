﻿using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 19
    public class CheckTransitionOnProductComparePage : BaseSearchTest
    {
        [TestCase("iphone", 3)]
        public void CheckTransitionOnProductComparePageTest
            (string userSearchQuery, int quantityOfProducts)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .AddProductsToCompare(quantityOfProducts)
                .ClickOnCompareProductLink()
                .CheckIsProductComparePageLoaded();
        }
    }
}
