﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 16
    public class CheckIsAvalibleCheckBoxSearchInSubCategory : BaseSearchTest
    {
        [TestCase("IPhone", "iphone")]
        [TestCase("Accessories", "iphone")]
        public void CheckIsAvalibleCheckBoxSearchInSubCategoryTest
            (string searchCategory, string searchQuery)
        {
            searchPage
                .EnterTextInSearchBox(searchQuery)
                .ClickSearchButton()
                .ChooseSearchCategory(searchCategory)
                .CheckIsAvalibleCheckBoxSearchInSubCategory();
        }
    }
}
