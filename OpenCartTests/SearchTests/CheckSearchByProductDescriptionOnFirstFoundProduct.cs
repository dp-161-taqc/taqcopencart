﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 24
    public class CheckSearchByProductDescriptionOnFirstFoundProduct : BaseSearchTest
    {
        [TestCase("i", 0)]
        [TestCase("can", 0)]
        [TestCase("get", 0)]
        [TestCase("it", 0)]
        public void CheckSearchByProductDescriptionOnFirstFoundProductTest
            (string expectedSearchText, int indexOfProduct)
        {
            searchPage
                .TickCheckBoxSearchInProductDescription()
                .EnterTextInExtraSearchBox(expectedSearchText)
                .ClickExtraSearchButton()
                .ClickProductInImagesListByIndex(indexOfProduct)
                .CheckIsProductPageLoaded()
                .GetChecker()
                .CheckStringContains(expectedSearchText, productPage.GetFullDescriptionText());
        }
    }
}
