﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #9
    public class CheckProductSortingByNameIncrease : BaseSearchTest
    {
        [TestCase("iphone", "Name (A - Z)")]
        [TestCase("mac", "Name (A - Z)")]
        [TestCase("ipad", "Name (A - Z)")]
        [TestCase("watch", "Name (A - Z)")]
        public void CheckProductSortingByNameIncreaseTest(string userSearchQuery, string sortMethod)
        {
            searchPage
                .EnterTextInSearchBox(userSearchQuery)
                .ClickSearchButton()
                .ClickSelectorSortProductsBy()
                .SortProductsBy(sortMethod)
                .CheckCollectionIsOrdered(searchPage.GetProductNamesList());
        }
    }
}