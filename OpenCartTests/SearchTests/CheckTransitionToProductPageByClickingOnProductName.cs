﻿using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using System.Linq;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #2
    public class CheckTransitionToProductPageByClickingOnProductName : BaseSearchTest
    {
        [TestCase(0)]
        public void CheckTransitionToProductPageByClickingOnProductNameTest(int indexOfElement)
        {
            searchPage
                .EnterTextInSearchBox("iphone")
                .ClickSearchButton()
                .ClickProductInList(searchPage.ProductNamesList().ElementAt(indexOfElement).Text)
                .CheckIsProductPageLoaded();
        }
    }
}
