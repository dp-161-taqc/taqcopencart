﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // # 14
    public class CheckThatProductCompareLinkDisplayedWhenProductsFound : BaseSearchTest
    {
        [TestCase("ipad")]
        [TestCase("watch")]
        public void CheckThatProductCompareLinkDisplayedWhenProductsFoundTest(string validInput)
        {
            searchPage
                .EnterTextInSearchBox(validInput)
                .ClickSearchButton()
                .CheckIsElementDisplayed(searchPage.productCompareLink);
        }
    }
}
