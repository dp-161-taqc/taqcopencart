﻿using NUnit.Framework;

namespace OpenCartTests.SearchTests
{
    [TestFixture] // #4
    public class CheckThatTitleEndsWithUserSearchQuery : BaseSearchTest
    {
        [TestCase("some search quary")]
        [TestCase("2432437980")]
        public void CheckThatTitleEndsWithUserSearchQueryTest(string expectedPartOfTitle)
        {
            searchPage
                .EnterTextInSearchBox(expectedPartOfTitle)
                .ClickSearchButton()
                .CheckStringEndsWith(expectedPartOfTitle, driver.Title);
        }
    }
}
