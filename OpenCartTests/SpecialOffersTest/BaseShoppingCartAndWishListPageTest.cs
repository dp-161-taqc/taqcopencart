﻿using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.SpecialOffersTest
{
    public class BaseShoppingCartAndWishListPageTest:BaseTest
    {
        protected CommonPage commonPage;
        [SetUp]
        public void BeforeTestCases()
        {
            commonPage = new CommonPage(driver);
            OpenURL(BaseUrl);
        }

        protected void OpenURL(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

    }
}
