﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckShopCartChangeQuantityOneProductTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 8 (Product) Red", "5", "5")]
        public void CheckShopCartChangeQuantityOneProduct(string userLastName, 
                                                          string productName, 
                                                          string quantityProduct,
                                                          string expectedQuntityTheSameProduct)
        {
            commonPage
                 .ClickDropDownToggle()
                 .ClickLogInOfDropDownToggle()
                 .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                 .EnterTextInSearchBox(productName)
                 .ClickSearchButton()
                 .ClickProductInList(productName)
                 .ClickOnAddToShoppingCartButton()
                 .GetShoppingCartPage()
                 .InputQuantityProduct(productName, quantityProduct)
                 .ClickUpdateProduct(productName)
                 .CheckGetQuntityOneProduct(expectedQuntityTheSameProduct, productName);

        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                 .GetShoppingCartPage()
                 .ClearShoppingCart();
        }
    }
}
