﻿using NUnit.Framework;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckUseOfGiftCertificateForDiscountTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 8 (Product) Red", "10002", "700.00")]
        public void CheckUseOfGiftCertificateForDiscount(string productName,
                                                         string validGiftCertificate, 
                                                         string expectedTotalPrice)
        {
           commonPage
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .ClickOnAddToShoppingCartButton()
                    .GetShoppingCartPage()
                    .ApplyGifCertificate(validGiftCertificate)
                    .CheckTotalPrice(expectedTotalPrice);
        }
    }
}
