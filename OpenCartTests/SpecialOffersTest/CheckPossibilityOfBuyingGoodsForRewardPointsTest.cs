﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckPossibilityOfBuyingGoodsForRewardPointsTest: BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 7 32GB Jet Black", "1", "500", "0.00")]
        public void CheckPossibilityOfBuyingGoodsForRewardPoints(string userLastName, 
                                                                 string productName, 
                                                                 string quantityProducts,
                                                                 string rewardPoints, 
                                                                 string expectedTotalPrice)
        {
            commonPage
                     .ClickDropDownToggle()
                     .ClickLogInOfDropDownToggle()
                     .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                     .EnterTextInSearchBox(productName)
                     .ClickSearchButton()
                     .ClickProductInList(productName)
                     .ClickOnAddToShoppingCartButton()
                     .GetShoppingCartPage()
                     .EnterTextInQuantityProductBox(quantityProducts)
                     .ApplyRewardPoints(rewardPoints)
                     .CheckTotalPrice(expectedTotalPrice);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
