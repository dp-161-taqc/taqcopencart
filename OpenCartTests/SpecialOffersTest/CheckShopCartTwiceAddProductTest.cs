﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckShopCartTwiceAddProductTest: BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 8 (Product) Red", "2")]
        public void CheckShopCartTwiceAddProduct(string userLastName, 
                                                 string productName,
                                                 string expectedQuntityOneProduct)
        {
            commonPage
                 .ClickDropDownToggle()
                 .ClickLogInOfDropDownToggle()
                 .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                 .EnterTextInSearchBox(productName)
                 .ClickSearchButton()
                 .ClickProductInList(productName)
                 .ClickOnAddToShoppingCartButton()
                 .ClickOnAddToShoppingCartButton()
                 .GetShoppingCartPage()
                 .CheckGetQuntityOneProduct(expectedQuntityOneProduct, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
