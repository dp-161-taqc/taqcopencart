﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckDiscountsForGroupOfWholesaleCustomersTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", 5, "CustomerTwo")]
        [TestCase("iPad New 2017 Pro 12.9 512GB", 112, "CustomerTwo")]
        [TestCase("iPad New 2018 Wi-Fi 32Gb Silver", 27, "CustomerTwo")]
        public void CheckDiscountsForGroupOfWholesaleCustomers(string productName, 
                                                               int expectedDiscount, 
                                                               string userLastName)
        {
            commonPage
                  .ClickDropDownToggle()
                  .ClickLogInOfDropDownToggle()
                  .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                  .EnterTextInSearchBox(productName)
                  .ClickSearchButton()
                  .ClickProductInList(productName)
                  .CheckGetDiscount(expectedDiscount);
        }
    }
}
