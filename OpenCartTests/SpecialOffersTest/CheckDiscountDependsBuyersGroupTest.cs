﻿using NUnit.Framework;
using SeleniumHelpers.Models;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckDiscountDependsBuyersGroupTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 8 (Product) Red", "5", "780.00")]
        [TestCase("CustomerTwo", "IPhone 8 (Product) Red", "5", "750.00")]
        public void CheckDiscountDependsBuyersGroup(string userLastName, 
                                                    string productName,
                                                    string quantityProducts,
                                                    string expectedUnitPrice)
        {
            commonPage
                      .ClickDropDownToggle()
                      .ClickLogInOfDropDownToggle()
                      .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                      .EnterTextInSearchBox(productName)
                      .ClickSearchButton()
                      .ClickProductInList(productName)
                      .ClickOnAddToShoppingCartButton()
                      .GetShoppingCartPage()
                      .InputQuantityProduct(productName, quantityProducts)
                      .ClickUpdateProduct(productName)
                      .CheckUnitlPrice(expectedUnitPrice, productName);
        }
        [TearDown]
        public void TearDown()
        {
            new ShoppingCartPage(driver).ClearShoppingCart();
        }
    }
}
