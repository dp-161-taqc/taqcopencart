﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckShopCartAddTwoDiffProductsAddTwoTableRowsTest:BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 8 (Product) Red", "IPhone 7 32GB Black", 2)]
        public void CheckShopCartAddTwoDiffProductsAddTwoTableRows(string userLastName, 
                                                                   string productNameOne,
                                                                   string productNameTwo,
                                                                      int expectedQuntityTableRows)
        {
            commonPage
                 .ClickDropDownToggle()
                 .ClickLogInOfDropDownToggle()
                 .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                 .EnterTextInSearchBox(productNameOne)
                 .ClickSearchButton()
                 .ClickProductInList(productNameOne)
                 .ClickOnAddToShoppingCartButton()
                 .GetShoppingCartPage()
                 .EnterTextInSearchBox(productNameTwo)
                 .ClickSearchButton()
                 .ClickProductInList(productNameTwo)
                 .ClickOnAddToShoppingCartButton()
                 .GetShoppingCartPage()
                 .CheckGetQuntityProductTableRows(expectedQuntityTableRows);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
