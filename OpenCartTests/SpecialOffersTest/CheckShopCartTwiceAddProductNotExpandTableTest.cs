﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckShopCartTwiceAddProductNotExpandTableTest:BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 8 (Product) Red", 1)]
        public void CheckShopCartTwiceAddProductNotExpandTable(string userLastName,
                                                               string productName,
                                                                  int expectedQuntityTableRows)
        {
            commonPage
                 .ClickDropDownToggle()
                 .ClickLogInOfDropDownToggle()
                 .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                 .EnterTextInSearchBox(productName)
                 .ClickSearchButton()
                 .ClickProductInList(productName)
                 .ClickOnAddToShoppingCartButton()
                 .ClickOnAddToShoppingCartButton()
                 .GetShoppingCartPage()
                 .CheckGetQuntityProductTableRows(expectedQuntityTableRows);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
