﻿using NUnit.Framework;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]//5
    public class CheckDefaultDiscountTest : BaseShoppingCartAndWishListPageTest
    {

        [TestCase("IPhone 8 (Product) Red", "10", "770.00")]
        public void CheckDefaultDiscount(string productName, 
                                         string quantityProducts, 
                                         string expectedUnitPrice)
        {
            commonPage
                     .EnterTextInSearchBox(productName)
                     .ClickSearchButton()
                     .ClickProductInList(productName)
                     .ClickOnAddToShoppingCartButton()
                     .GetShoppingCartPage()
                     .EnterTextInQuantityProductBox(quantityProducts)
                     .CheckUnitlPrice(expectedUnitPrice, productName);
        }
    }
}
