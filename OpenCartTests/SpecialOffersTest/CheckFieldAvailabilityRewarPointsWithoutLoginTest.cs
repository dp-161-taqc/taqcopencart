﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;


namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckFieldAvailabilityRewarPointsWithoutLoginTest: BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 8 (Product) Red", true)]
        public void CheckFieldAvailabilityRewarPointsWithoutLogin(string productName, bool expectedFieldNotExist)
        {
            commonPage
                  .EnterTextInSearchBox(productName)
                  .ClickSearchButton()
                  .ClickProductInList(productName)
                  .ClickOnAddToShoppingCartButton()
                  .GetShoppingCartPage()
                  .CheckIsThereFieldRewardPoints(expectedFieldNotExist);
        }
    }
}
