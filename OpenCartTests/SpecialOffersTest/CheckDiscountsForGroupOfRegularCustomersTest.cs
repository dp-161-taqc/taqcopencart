﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]//3
    class CheckDiscountsForGroupOfRegularCustomersTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", 20, "CustomerOne")]
        [TestCase("iPad New 2017 Pro 12.9 512GB", 63, "CustomerOne")]
        [TestCase("iPad New 2018 Wi-Fi 32Gb Silver", 22, "CustomerOne")]
        public void CheckDiscountsForGroupOfRegularCustomers(string productName, 
                                                             int expectedDiscount, 
                                                             string userLastName)
        {
            commonPage
                    .ClickDropDownToggle()
                    .ClickLogInOfDropDownToggle()
                    .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                    .EnterTextInSearchBox(productName)
                    .ClickSearchButton()
                    .ClickProductInList(productName)
                    .CheckGetDiscount(expectedDiscount);
        }
    }
}
