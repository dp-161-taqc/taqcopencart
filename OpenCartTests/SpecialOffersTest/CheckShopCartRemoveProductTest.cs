﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckShopCartRemoveProductTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("CustomerOne", "IPhone 8 (Product) Red", "IPhone 7 32GB Black", false)]
        public void CheckShopCartRemoveProduct(string userLastName, 
                                               string productNameOne,
                                               string productNameTwo,
                                               bool expectedIsProductInShoppingCart)
        {
             commonPage
                  .ClickDropDownToggle()
                  .ClickLogInOfDropDownToggle()
                  .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                  .EnterTextInSearchBox(productNameOne)
                  .ClickSearchButton()
                  .ClickProductInList(productNameOne)
                  .ClickOnAddToShoppingCartButton()
                  .GetShoppingCartPage()
                  .EnterTextInSearchBox(productNameTwo)
                  .ClickSearchButton()
                  .ClickProductInList(productNameTwo)
                  .ClickOnAddToShoppingCartButton()
                  .GetShoppingCartPage()
                  .ClickRemoveProduct(productNameOne)
                  .CheckIsProductInShoppingCart(expectedIsProductInShoppingCart, productNameOne);
        }

        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
