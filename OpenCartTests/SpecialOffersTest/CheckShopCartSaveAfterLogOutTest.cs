﻿using NUnit.Framework;
using SeleniumHelpers.Models;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    class CheckShopCartSaveAfterLogOutTest:BaseShoppingCartAndWishListPageTest
    {

        [TestCase("IPhone 7 32GB Black", true, "CustomerOne")]
        public void CheckShopCartSaveAfterLogOut(string productName, 
                                                 bool expectedIsProductInShoppingCart,
                                                 string userLastName)
        {
            commonPage
                      .ClickDropDownToggle()
                      .ClickLogInOfDropDownToggle()
                      .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                      .EnterTextInSearchBox(productName)
                      .ClickSearchButton()
                      .ClickProductInList(productName)
                      .ClickOnAddToShoppingCartButton()
                      .GetShoppingCartPage()
                      .ClickDropDownToggle()
                      .ClickLogOutOfDropDownToggle()
                      .ClickDropDownToggle()
                      .ClickLogInOfDropDownToggle()
                      .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                      .GetShoppingCartPage()
                      .CheckIsProductInShoppingCart(expectedIsProductInShoppingCart, productName);
        }
        [TearDown]
        public void TearDown()
        {
            commonPage
                .GetShoppingCartPage()
                .ClearShoppingCart();
        }
    }
}
