﻿using NUnit.Framework;

namespace OpenCartTests.SpecialOffersTest
{
    [TestFixture]
    public class CheckUseOfDiscountCouponTest : BaseShoppingCartAndWishListPageTest
    {
        [TestCase("IPhone 7 32GB Black", "111112", "243.00")]
        public void CheckUseOfDiscountCoupon(string productName, 
                                             string validCouponCode, 
                                             string expectedTotalPrice)
        {
            commonPage
                     .EnterTextInSearchBox(productName)
                     .ClickSearchButton()
                     .ClickProductInList(productName)
                     .ClickOnAddToShoppingCartButton()
                     .GetShoppingCartPage()
                     .ApplyCouponCode(validCouponCode)
                     .CheckTotalPrice(expectedTotalPrice);
        }
        
    }
}
