﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.AccountTests
{
    [TestFixture]
    public class CheckConfirmPasswordEditTest : StartPageSetupBaseTest
    {
        [TestCase("TestLast", "qwerty33")]
        public void CheckConfirmPasswordEditUser(string userLastName, string password)
        {
            commonPageWithMenu
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                .ClickPasswordButton()
                .InputPassword(password)
                .InputPasswordConfirm(password)
                .ClickContinueButton()
                .CheckAccountPage();
        }

        [TearDown]
        public void ChangeOnPreviousPassword()
        {
            new AccountPage(driver)
                .ClickPasswordButton()
                .InputPassword("qwerty")
                .InputPasswordConfirm("qwerty")
                .ClickContinueButton();
        }
    }
}
