﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.AccountTests
{
    [TestFixture]
    public class CheckLogoutButtonAccountPageTest : StartPageSetupBaseTest
    {
        [TestCase("TestLast")]
        public void CheckLogoutButtonAndCheckContiuneButtonTest(string userLastName)
        {
            commonPageWithMenu
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                .ClickLogoutPageButton()
                .CheckLogoutPage();
        }
    }
}
