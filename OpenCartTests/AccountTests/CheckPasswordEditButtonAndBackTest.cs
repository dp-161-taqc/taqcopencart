﻿using NUnit.Framework;
using OpenCartTests.LoginTests;
using SeleniumHelpers.Models;

namespace OpenCartTests.AccountTests
{
    [TestFixture]
    public class CheckPasswordEditButtonAndBackTest : StartPageSetupBaseTest
    {
        [TestCase("TestLast2")]
        public void CheckClickPasswordButtonAndAfterBackButton(string userLastName)
        {
            commonPageWithMenu
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(UserRegister.GetUser(userLastName))
                .ClickPasswordButton()
                .ClickBackButton()
                .CheckAccountPage();
        }
    }
}
