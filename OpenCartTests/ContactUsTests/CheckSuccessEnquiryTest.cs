﻿using NUnit.Framework;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckSuccessEnquiryTest : BaseContactUsTest
    {
        [TestCase("Good name", "good@mail.com", "Good enquiry")]
        public void CheckSuccessEnquiry(string name, string mail, string enquiry)
        {
            contactUsPage
                .OpenContactUsPageByUrl()
                .SetName(name)
                .SetMail(mail)
                .SetEnquiry(enquiry)
                .ClickSubmitButton()
                .CheckContactUsSuccessMessage()
                .ClickContactUsContinueButton()
                .CheckHomePageIsOpened();
        }
    }
}
