﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckOpenContactUsPageFromHeaderTest : BaseContactUsTest
    {
        [Test]
        public void CheckOpenContactUsPageFromHeader()
        {
            contactUsPage
                .GoToHomePage()
                .ClickTelephoneIcon()
                .CheckContactUsPageIsOpen();
        }
    }
}
