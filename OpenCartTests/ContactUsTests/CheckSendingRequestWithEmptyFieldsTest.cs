﻿using NUnit.Framework;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckSendingRequestWithEmptyFieldsTest : BaseContactUsTest
    {
        [TestCase("", "", "")]
        public void CheckSendingRequestWithEmptyFields(string name, string mail, string enquiry)
        {
            contactUsPage
                .OpenContactUsPageByUrl()
                .SetName(name)
                .SetMail(mail)
                .SetEnquiry(enquiry)
                .ClickSubmitButtonWithBadData()
                .CheckContactUsBadNameMessage();   
        }
    }
}
