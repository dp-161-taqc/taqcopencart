﻿using NUnit.Framework;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckOpenContactUsPageFromFooterTest : BaseContactUsTest
    {
        [Test]
        public void CheckOpenContactUsPageFromFooter()
        {
            contactUsPage
                .GoToHomePage()
                .ClickCategoryContactUsInFooter()               
                .CheckContactUsPageIsOpen();            
        }
    }
}
