﻿using NUnit.Framework;
using SeleniumHelpers.Pages;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class BaseContactUsTest : BaseTest
    {       
        protected ContactUsPage contactUsPage;       

        [SetUp]
        public void BeforeContactUsTest()
        {
            contactUsPage = new ContactUsPage(driver);           
        }
    }
}
