﻿using NUnit.Framework;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckSendingRequestWithBadNameTest : BaseContactUsTest
    {
        [TestCase("", "good@mail.com", "Good enquiry")]
        [TestCase("I", "good@mail.com", "Good enquiry")]
        [TestCase("123456789123456789123456789123456", "good@mail.com", "Good enquiry")]
        public void CheckSendingRequestWithBadName(string name, string mail, string enquiry)
        {
            contactUsPage
                .OpenContactUsPageByUrl()
                .SetName(name)
                .SetMail(mail)
                .SetEnquiry(enquiry)
                .ClickSubmitButtonWithBadData()
                .CheckContactUsBadNameMessage();            
        }
    }
}
