﻿using NUnit.Framework;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckSendingRequestWithBadEnquiryTest : BaseContactUsTest
    {
        [TestCase("Good name", "good@mail.com", "")]
        [TestCase("Good name", "good@mail.com", "Less 10")]
        public void CheckSendingRequestWithBadEnquiry(string name, string mail, string enquiry)
        {
            contactUsPage
                .OpenContactUsPageByUrl()
                .SetName(name)
                .SetMail(mail)
                .SetEnquiry(enquiry)
                .ClickSubmitButtonWithBadData()
                .CheckContactUsBadEnquiryMessage();            
        }
    }
}
