﻿using NUnit.Framework;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckSendingRequestWithBadMailTest : BaseContactUsTest
    {
        [TestCase("Good name", "", "Good enquiry")]
        [TestCase("Good name", "bad", "Good enquiry")]
        [TestCase("Good name", "1@", "Good enquiry")]
        [TestCase("Good name", "1@2", "Good enquiry")]
        [TestCase("Good name", "1@2.", "Good enquiry")]
        public void CheckSendingRequestWithBadMail(string name, string mail, string enquiry)
        {
            contactUsPage
                .OpenContactUsPageByUrl()
                .SetName(name)
                .SetMail(mail)
                .SetEnquiry(enquiry)
                .ClickSubmitButtonWithBadData()
                .CheckContactUsBadMailMessage();            
        }
    }
}
