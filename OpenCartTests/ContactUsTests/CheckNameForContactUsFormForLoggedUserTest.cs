﻿using NUnit.Framework;
using SeleniumHelpers.Data;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckNameForContactUsFormForLoggedUserTest : BaseContactUsTest
    {
        [Test]
        public void CheckNameForContactUsFormForLoggedUser()
        {
            contactUsPage
                .GoToHomePage()
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(Users.UserForProductAndContactUsForm())
                .ClickTelephoneIcon()
                .CheckNameForContactUsFormForLoggedUser(Users.UserForProductAndContactUsForm().Firstname);           
        }
    }
}
