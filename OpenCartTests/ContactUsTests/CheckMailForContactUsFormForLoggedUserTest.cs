﻿using NUnit.Framework;
using SeleniumHelpers.Data;

namespace OpenCartTests.ContactUsTests
{
    [TestFixture]
    class CheckMailForContactUsFormForLoggedUserTest : BaseContactUsTest
    {
        [Test]
        public void CheckMailForContactUsFormForLoggedUser()
        {
            contactUsPage
                .GoToHomePage()
                .ClickLoginButton()
                .ClickLoginButtonCenterMenu(Users.UserForProductAndContactUsForm())
                .ClickTelephoneIcon()
                .CheckMailForContactUsFormForLoggedUser(Users.UserForProductAndContactUsForm().Email);
        }
    }
}
