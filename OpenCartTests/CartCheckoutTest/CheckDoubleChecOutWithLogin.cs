﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Models;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckDoubleChecOutWithLogin : BaseCheckoutTest
    {
        [Test]
        public void CheckDoubleChecOutWithLoginTest()
        {
             homePage
                     .ClickAddToShoppingCart("MacBook Pro")
                     .GetShoppingCartPage()
                     .GoToCheckout()
                     .ContinueBillingStep2()
                     .GotoMain()
                     .AddToCartFromMain()
                     .GoToCheckoutSecond()
                     .ContinueBillingButton()
                     .ContinueShippingStep3()
                     .ContinueDeliveryStep4()
                     .CheckCkeckBoxStep5()
                     .ContinuePaymentStep5()
                     .Confirm()
                     .CheckSuccessfulMessage();
        }
    }
}