﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Models;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckAccountPageAfterCheckout : BaseCheckoutTest
    {
        [Test]
        public void CheckAccountPageAfterCheckoutTest()
        {

            homePage
                     .EnterTextInSearchBox("MacBook Pro")
                     .ClickSearchButton()
                     .ClickProductInList("MacBook Pro")
                     .ClickOnAddToShoppingCartButton()
                     .GetShoppingCartPage()
                     .GoToCheckout()
                     .ContinueBillingStep2()
                     .ContinueShippingStep3()
                     .ContinueDeliveryStep4()
                     .CheckCkeckBoxStep5()
                     .ContinuePaymentStep5()
                     .Confirm()
                     .GotoAccount()
                     .CheckAccountPage();
        }
    }
}