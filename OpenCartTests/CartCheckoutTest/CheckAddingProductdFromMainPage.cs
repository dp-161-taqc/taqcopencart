﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages;

namespace OpenCartTests.CartCheckout
{
   
    [TestFixture]
    class AddingProductFromMain : BaseCheckoutTest
    {
        [TestCase("$2,100.00")]
        public void AddingFromMainAndCheckCart(string expectedPrice)
        {
             homePage
                     .ClickAddToShoppingCart("MacBook Pro")
                     .GetShoppingCartPage()
                     .CheckFullPrice(expectedPrice);
        }
        
    }
}
