﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class Update : BaseCheckoutTest
    {
        [TestCase("MacBook Pro", "$4,000.00", "2")]
        public void UpdateCart(string productName, string expectedPrice, string quantityProduct)
        {
           homePage
                .EnterTextInSearchBox(productName)
                .ClickSearchButton()
                .ClickProductInList(productName)
                .ClickOnAddToShoppingCartButton()
                .GetShoppingCartPage()
                .UpdateCart(quantityProduct)
                .CheckFullPrice(expectedPrice);

        }
    }
}

