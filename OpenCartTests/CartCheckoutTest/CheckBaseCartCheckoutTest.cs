﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Pages;

namespace OpenCartTests.CartCheckout
{
    class BaseCheckoutTest : BaseTest
    {
        protected CommonPage commonPage;

        [SetUp]
        public void BeforeProductTest()
        {
            commonPage = new CommonPage(driver);
            OpenURL("https://free74238.cpsite.ru");
        }

        protected void OpenURL(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

    }
}
