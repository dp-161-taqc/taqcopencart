﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class EnterInvalidQuantityOfProduct : BaseCheckoutTest
    {
        [TestCase("MacBook Pro", "hkhkhjk")]
        [TestCase("MacBook Pro", "0")]
        [TestCase("MacBook Pro", "%$#^")]
        public void EnterInvalidQuantityOfProductTest(string productName, string invalidValue)
        {
             homePage
                .EnterTextInSearchBox(productName)
                .ClickSearchButton()
                .ClickProductInList(productName)
                .EnterValueShoppingCart(invalidValue)
                .ClickOnAddToShoppingCartButton()
                .GetShoppingCartPage()
                .CheckEmptyCart();
        }
    }
}
