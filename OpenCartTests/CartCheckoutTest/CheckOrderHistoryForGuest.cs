﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Models;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckOrderHistoryForGuest : BaseCheckoutTest
    {
        [Test]
        public void CheckOrderHistoryForGuestTest()
        {
            homePage
                .ClickOrderHistoryInFooter()
                .CheckLoginPage();
        }
    }
}