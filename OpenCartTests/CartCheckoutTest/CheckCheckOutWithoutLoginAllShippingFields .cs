﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckOutWithoutLoginAllFieldsShipping : BaseCheckoutTest
    {
        [Test]
        public void CheckOutWithoutLoginShippingObligatiryFieldsTest()
        {
               homePage
                     .ClickAddToShoppingCart("MacBook Pro")
                     .GetShoppingCartPage()
                     .GoToCheckout()
                     .GuestCheckOutRadio()
                     .GuestCheckOut()
                     .EnterFirstName()
                     .EnterLastName()
                     .EnterEmail()
                     .EnterPhone()
                     .EnterFax()
                     .EnterCompany()
                     .EnterPostcode()
                     .EnterAdress1()
                     .EnterAdress2()
                     .InputCountry()
                     .InputRegion()
                     .EnterCity()
                     .ContinueBillingStep2Guest()
                     .ContinueDeliveryStep4()
                     .CheckCkeckBoxStep5()
                     .ContinuePaymentStep5()
                     .Confirm()
                     .CheckSuccessfulMessage();
        }
    }
}