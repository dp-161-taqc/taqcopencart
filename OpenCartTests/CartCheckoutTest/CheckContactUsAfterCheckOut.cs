﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class ContactUsAfterCheckOut : BaseCheckoutTest
    {
        [Test]
        public void ContactUsAfterCheckOutTest()
        {
            homePage
                    .ClickAddToShoppingCart("MacBook Pro")
                    .GetShoppingCartPage()
                    .GoToCheckout()
                    .GuestCheckOutRadio()
                    .GuestCheckOut()
                    .EnterFirstName()
                    .EnterLastName()
                    .EnterPhone()
                    .EnterPostcode()
                    .EnterEmail()
                    .EnterAdress1()
                    .InputCountry()
                    .InputRegion()
                    .EnterCity()
                    .ContinueBillingStep2Guest()
                    .ContinueDeliveryStep4()
                    .CheckCkeckBoxStep5()
                    .ContinuePaymentStep5()
                    .Confirm()
                    .ContactUs()
                    .CheckContactUsPageIsOpen();
        }
    }
}