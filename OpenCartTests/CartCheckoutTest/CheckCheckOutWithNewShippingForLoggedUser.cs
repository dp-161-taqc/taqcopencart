﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Models;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckOutWithNewShippingForLoggedUser : BaseCheckoutTest
    {
        [TestCase("MacBook Pro")]
        public void CheckOutWithNewShippingForLoggedUserTest(string productName)
        {
            
            homePage
                     .ClickAddToShoppingCart(productName)
                     .GetShoppingCartPage()
                     .GoToCheckout()
                     .LoggIn()
                     .ContinueBillingButton()
                     .NewDeliveryAdressStep3()
                     .EnterFirstNameDelivery()
                     .EnterLastNameDelivery()
                     .EnterCompanyDelivery()
                     .EnterAdressDelivery()
                     .EnterCityDelivery()
                     .ContinueShippingStep3()
                     .ContinueDeliveryStep4()
                     .CheckCkeckBoxStep5()
                     .ContinuePaymentStep5()
                     .Confirm()
                     .CheckSuccessfulMessage();
        }

    }
}