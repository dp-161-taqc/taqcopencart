﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class AddingProductFromSearch : BaseCheckoutTest
    {
        [TestCase("MacBook Pro", "$2,000.00")]
        public void AddingFromSearchAndCheckCart(string productName, string expectedPrice)
        {
            homePage
            .EnterTextInSearchBox(productName)
            .ClickSearchButton()
            .ClickProductInList(productName)
            .ClickOnAddToShoppingCartButton()
            .GetShoppingCartPage()
            .CheckFullPrice(expectedPrice);
        }
    }
}
