﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class TermsAndCondition : BaseCheckoutTest
    {
        [Test]
        public void TermsAndConditionTest()
        {

      homePage
                .ClickCategoryTermsAndConditionInFooter()
                .CheckTermsandConditionPage();
           
        }
    }
}