﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Pages;

namespace OpenCartTests.CartCheckout
{
    class BaseCheckoutTest : BaseTest
    {
        protected HomePage homePage;
        

        [SetUp]
        public void BeforeProductTest()
        {
            homePage = new HomePage(driver);
            
            OpenURL("http://newstore25062019.is-best.net/");
        }

        protected void OpenURL(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

    }
}
