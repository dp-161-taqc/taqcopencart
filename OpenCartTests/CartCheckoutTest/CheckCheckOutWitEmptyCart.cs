﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckOutWithEmptyCart : BaseCheckoutTest
    {
        [Test]
        public void CheckOutWithEmptyCartTest()
        {
           homePage
                .GoToCheckout()
                .CheckEmptyCart();
        }
    }
}