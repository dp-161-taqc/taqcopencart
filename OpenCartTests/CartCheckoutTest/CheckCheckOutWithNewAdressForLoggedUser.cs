﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;
using SeleniumHelpers.Models;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class CheckOutWithNewAdressForLoggedUser : BaseCheckoutTest
    {
        [Test]
        public void CheckOutWithNewAdressForLoggedUserTest()
        {
            homePage
                     .ClickAddToShoppingCart("MacBook Pro")
                     .GetShoppingCartPage()
                     .GoToCheckout()
                     .LoggIn()
                     .NewAdressForLoggedUser()
                     .EnterFirstName()
                     .EnterLastName()
                     .EnterCompany()
                     .EnterAdress1()
                     .EnterAdress2()
                     .InputCountry()
                     .InputRegion()
                     .EnterCity()
                     .ContinueBillingButton()
                     .ContinueShippingStep3()
                     .ContinueDeliveryStep4()
                     .CheckCkeckBoxStep5()
                     .ContinuePaymentStep5()
                     .Confirm()
                     .CheckSuccessfulMessage();
        }
    }
}