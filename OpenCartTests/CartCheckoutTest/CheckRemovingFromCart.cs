﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages.CommonPages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class Removing : BaseCheckoutTest
    {
        [TestCase ("MacBook Pro", "0")]
        public void RemovingFromCartTest(string productName, string quantity)
        {
            homePage
                .EnterTextInSearchBox(productName)
                .ClickSearchButton()
                .ClickProductInList(productName)
                .ClickOnAddToShoppingCartButton()
                .GetShoppingCartPage()
                .UpdateCart(quantity)
                .CheckEmptyCart();
        }
    }
}