﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SeleniumHelpers.Pages;

namespace OpenCartTests.CartCheckout
{

    [TestFixture]
    class AddingSeveralSameProduct : BaseCheckoutTest
    {
        [TestCase("MacBook Pro", "$20,000.00", "10")]
        public void AddingSeveralSameProductTest(string productName, string expectedPrice, string quantityProduct)
        {

                homePage
                .EnterTextInSearchBox(productName)
                .ClickSearchButton()
                .ClickProductInList(productName)
                .EnterValueShoppingCart(quantityProduct)
                .ClickOnAddToShoppingCartButton()
                .GetShoppingCartPage()
                .CheckFullPrice(expectedPrice);
        }
    }
}
